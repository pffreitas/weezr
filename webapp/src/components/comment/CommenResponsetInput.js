import React from 'react';
import CommentAPI from '../../api/CommentAPI';
import { TextInput } from '../base';

export const CommentResponseInput = ({ parentId, onConfirm, onCancel }) => {

  const addComment = (text) => {
    CommentAPI.addCommentResponse(parentId, text)
      .then(res => onConfirm(res.data))
      .catch(() => {
      });
  };

  return (
    <TextInput onConfirm={addComment} onCancel={onCancel} confirmButtonText="Reply" placeholder="Add a response..."/>
  );
};
