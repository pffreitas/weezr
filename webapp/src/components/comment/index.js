import { CommentInput } from './CommentInput';
import { Comment } from './Comment';

export {
  CommentInput,
  Comment,
};
