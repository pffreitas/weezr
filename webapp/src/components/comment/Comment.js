import React, { useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { CommentResponseInput } from './CommenResponsetInput';
import CommentAPI from '../../api/CommentAPI';
import { usePaginatedFetch } from '../fetcher/useFetch';
import { Loading } from '../loading/Loading';
import Color from '../color';
import { style } from 'react-toastify';

const Container = styled.div`
  display: grid;
  grid-template-columns: 36px 1fr; 
  grid-gap: 10px;
`;

const UserAvatar = styled.img`
  border-radius: 50%;
`;

const CommentContent = styled.div`
`;

const CommentTitle = styled.h6`
  font-size: 1rem;
  font-weight: bold;
  margin-top: 0;
  padding: 0;
  color: ${Color.BlueGrey700};

  & small { 
    color: ${Color.Grey500};
  }
`;

const CommentBody = styled.p`
  white-space: pre;
`;

const ResponseContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const ResponseActions = styled.div`
  & a {
    margin-right: 15px;
    padding: 0;
  }
`;

const ResponsesList = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 10px;
  margin: 15px 0;
`;

const LoadMoreResponsesContainer = styled.div`
  & a {
    margin-right: 15px;
    padding: 0;
  }
`;


export const Comment = ({ comment, responseEnabled }) => {

  const [replyOpened, setReplyOpened] = useState(false);
  const [responseListOpened, setResponseListOpened] = useState(false);

  const [responses, loadingResponses, loadMore, setResponses] = usePaginatedFetch([], 2,
    (limit, offset) => {
      if (responseListOpened) {
        return CommentAPI.loadCommentResponses(comment.ID, limit, offset);
      }
    },
    [responseListOpened],
  );

  return (
    <Container>
      <UserAvatar width={responseEnabled ? "36px" : "28px"} height="auto" src={comment.User.picture} />
      <CommentContent>
        <CommentTitle>{comment.User.name} <small>{moment(comment.CreatedAt).fromNow()}</small></CommentTitle>
        <CommentBody>{comment.Text}</CommentBody>

        {responseEnabled && (
          <ResponseContainer>
            <ResponseActions>
              <a className="sv-button link link-default" onClick={() => setResponseListOpened(!responseListOpened)}>
                Load {comment.ResponseCount} Responses
              </a>

              <a className="sv-button link link-default" onClick={() => setReplyOpened(true)}>Reply</a>
            </ResponseActions>

            {replyOpened && (
              <CommentResponseInput parentId={comment.ID}
                onConfirm={(response) => {
                  setResponses([response, ...responses]);
                  setReplyOpened(false);
                }}
                onCancel={() => setReplyOpened(false)}
                placeholder="Add a response..." />
            )}

            {responses.length > 0 && (
              <ResponsesList>
                {responses.map(c => <Comment key={`comment-${c.ID}`} comment={c} responseEnabled={false} />)}
                <LoadMoreResponsesContainer>
                  <Loading loading={loadingResponses} />
                  <a className="sv-button link link-info" onClick={() => loadMore()}>Load more responses</a>
                </LoadMoreResponsesContainer>
              </ResponsesList>
            )}

          </ResponseContainer>
        )}
      </CommentContent>
    </Container>
  );
};
