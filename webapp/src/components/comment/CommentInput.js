import React from 'react';
import CommentAPI from '../../api/CommentAPI';
import { TextInput } from '../base';

export const CommentInput = ({ episodeId, onConfirm }) => {

  const addComment = (text) => {
    CommentAPI.addComment(episodeId, text)
      .then(res => onConfirm(res.data))
      .catch(() => {
      });
  };

  return (
    <TextInput onConfirm={addComment} confirmButtonText="Comment" placeholder="Add a comment..."/>
  );
};

