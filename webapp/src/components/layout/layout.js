import styled from 'styled-components';

export const Body = styled.div`
  margin: 72px 15px 0px 15px;
  
  @media (min-width: 1024px) {
    margin: 72px;
  }
`;


