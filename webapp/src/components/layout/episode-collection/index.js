import styled from 'styled-components';
import InfiniteScroll from 'react-infinite-scroller';
import { Title4 } from '../../typography'

const EpisodeCollectionContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  
  @media (min-width: 768px){
    grid-template-columns: 240px 1fr;
    grid-template-rows: 60px 260px;
    grid-column-gap: 40px;
   
    max-width: 740px;
    margin-left: calc(50% - 740px / 2);
  }
`;

const ImageContainer = styled.div`
  padding-bottom: 20px;
  max-width: 240px;
  margin-left: calc(50% - 120px);
  
  @media (min-width: 768px){
    grid-row: 1 /  2;
    grid-column: 1;
  }
`;

const MosaicImage = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  
  img {
    width: 120px;
    height: auto;
  }

  img:nth-child(1) {
    border-radius: 4px 0 0 0;
  }
  
  img:nth-child(2) {
    border-radius: 0 4px 0 0;
  }
  
  img:nth-child(3) {
    border-radius: 0 0 0 4px;
  }
  
  img:nth-child(4) {
    border-radius: 0 0 4px 0; 
  }
`;

const Image = styled.img`
  border-radius: 4px;
  width: 240px;
  height: 240px;
`;


const FollowingContainer = styled.div`
  display: flex;
`;

const FollowingCount = styled.span`
  line-height: 36px;
  display: inline-block;
  flex: 1;
`;


const TitleContainer = styled.div`
  grid-row: 2;
  
  display: grid;
  grid-template-columns: 1fr 48px;
  
  @media (min-width: 768px){
    grid-row: 1;
    grid-column: 2;
  }
`;

const Title = styled(Title4)``;

const Description = styled.p`
  grid-row: 3;
`;

const EpisodeContainer = styled.div`
  padding-top: 25px;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 25px;

  @media (min-width: 768px){
    grid-row: 2 / 4;
    grid-column: 2;
  }
`;

const EpisodeScroll = styled(InfiniteScroll)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 25px;
`;

export {
  EpisodeCollectionContainer,
  ImageContainer,
  MosaicImage,
  Image,
  FollowingContainer,
  FollowingCount,
  TitleContainer,
  Title,
  Description,
  EpisodeContainer,
  EpisodeScroll,

};
