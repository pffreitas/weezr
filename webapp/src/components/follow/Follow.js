import classnames from 'classnames';
import React from 'react';
import ListenAPI from '../../api/ListenAPI';
import ShowStore from '../../stores/ShowStore';
import { toast } from 'react-toastify';

export const FollowButton = ({ slug, following, onFollow, onUnfollow }) => {

  const followShow = showSlug => {
    if (following) {
      ListenAPI.unfollowShow(showSlug)
        .then(() => {
          onUnfollow();
          ShowStore.fetchFollowing();
        })
        .catch(err => {
          console.log(err);
          toast.error('Failed to like episode', {
            position: toast.POSITION.TOP_CENTER,
          });
        });
    } else {
      ListenAPI.followShow(showSlug)
        .then(() => {
          onFollow();
          ShowStore.fetchFollowing();
        })
        .catch(err => {
          console.log(err);
          toast.error('Failed to follow ', {
            position: toast.POSITION.TOP_CENTER,
          });
        });
    }
  };

  return (
    <button
      className={classnames('sv-button', {
        default: following,
        primary: !following,
      })}
      onClick={() => followShow(slug)}
    >
      {following ? 'following' : 'follow'}
    </button>
  );
};
