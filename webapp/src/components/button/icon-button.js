import styled from 'styled-components';

export const IconButton = styled.button`

  padding: 12px;
  border-radius: 30px;
  border: 0;
  line-height: 14px;
  width: 38px;
  font-size: 14px;
  transition: background .4s cubic-bezier(0.4,0.0,0.2,1);
  color: #767676;
  background-color: transparent;
  
  &:hover {
    background-color: #eee;
    cursor: pointer;
  }
`;
