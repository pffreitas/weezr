import React from 'react';
import styled from 'styled-components';
import Color from '../color';

export const Icon = styled.i`
  font-size: 1.1rem;
  text-align: center;
  vertical-align: middle;
  line-height: 22px;
  width: 22px;
  height: 22px;
`;

export const Button = styled.a`
  padding: 4px 8px;  
  border-radius: 4px;
  text-transform: uppercase;
  font-family: Montserrat;
  font-weight: 600;
  font-size: 0.86em;
  color: ${Color.Grey600};
`;

export const InfoButton = styled(Button)`
  border: 1px solid cornflowerblue;
  color: #1976d2;
  
  &:hover {
    color: #1976d2;
    background-color: #fff;
    border: 1px solid rgba(25, 118, 210, 0.3);
    box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.08);
  }
`;

export const DefaultLink = styled(Button)`
  color: ${Color.Grey600};
  border: 1px solid transparent;
  
  &:hover {
    color: ${Color.Grey600};
    background-color: rgba(0, 0, 0, 0.04);
    transition: background-color 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  }
`;


export const RoundedLink = styled(DefaultLink)`
  color: #8492a6;
  height: 48px;
  width: 48px;
  border-radius: 50%;
  padding: 15px;
  
  &:hover {
    background-color: rgba(0, 0, 0, 0.04);
  }
`;
