import React, { useRef, useState } from 'react';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { IconButton } from '../button/icon-button';
import classnames from 'classnames';

const SearchBoxContainer = styled.div`
  @media (min-width: 1024px) {
    width: 170px;
    height: 35px;
    background: rgba(0, 0, 0, 0.1);
    display: flex;
    color: #8a8f9c;
    border-radius: 22px;
    transition: background .4s, box-shadow .2s, -webkit-box-shadow .2s, width .4s, border-radius .2s;
    
    &:hover {
      width: 370px;
      background: #fff;
      box-shadow: 0 10px 20px rgba(0,0,0,0.14);
      color: #3a3c4c;
      border-radius: 4px;
    }
  }
`;

const SearchBoxIcon = styled.i`
  line-height: 35px;
  color: #8a8f9c;
  height: 35px;
  border-radius: 22px;
  text-align: center;
  display: none;
  font-size: 14px;
  padding: 0 15px;
  
  @media (min-width: 1024px) {
    display: block;
  }
`;

const SearchBoxInput = styled.input`
  flex: 1;
  height: 35px;
  border: 0 !important;
  background: transparent;
  color: #b3b3b3;
  font-weight: normal;
  font-size: 16px;
  line-height: 35px;
  display: none;
  width: 100%;
  margin-right: 15px;

  @media (min-width: 1024px) {
      display: block;
      
      &:focus {
        color: #3a3c4c;
      }
  }
`;

const AbsoluteSearchIconButton = styled(IconButton)`
  @media (min-width: 1024px) {
    display: none;
  }
`;

const AbsoluteSearchBoxContainer = styled.div`
    display: flex;
    padding: 15px 10px;
    height: 68px;
    width: 100%;
    position: fixed;
    background: white;
    z-index: 10;
    top: 0;
    left: 0;
    transition: all .4s ease;
    border-bottom: 1px solid #eee;
    
    &.hidden {
        transform: translateX(-130vw);
    }
    
    @media (min-width: 1024px) {
      display: none;
    }
`;

const AbsoluteSearchBoxInput = styled.input`
    height: 37px;
    border: 0;
    background: transparent;
    font-weight: normal;
    font-size: 18px;
    line-height: 35px;
    border: 0 !important;
    width: 100vw;
    margin-right: 15px;
    margin-left: 25px;
    color: #3a3c4c;
`;

const SearchBox = observer(() => {
  const [open, setOpen] = useState(false);
  const absInputRef = useRef(null);

  const history = useHistory();

  const handleEnter = e => {
    if (e.key === 'Enter') {
      history.push(`/search?q=${e.target.value}`);
    }
  };

  return (
    <SearchBoxContainer>
      <AbsoluteSearchIconButton onClick={() => {
        absInputRef.current.focus();
        setOpen(true);
      }}>
        <i className="fa fa-search" />
      </AbsoluteSearchIconButton>
      <AbsoluteSearchBoxContainer className={classnames({ 'hidden': !open })}>
        <IconButton className="fa fa-arrow-left" onClick={() => setOpen(false)} />
        <AbsoluteSearchBoxInput
          type="text"
          placeholder="Search..."
          onKeyPress={handleEnter}
          ref={absInputRef}
        />
      </AbsoluteSearchBoxContainer>

      <SearchBoxIcon className="fa fa-search" />
      <SearchBoxInput
        type="text"
        placeholder="Search..."
        onKeyPress={handleEnter}
      />
    </SearchBoxContainer>
  );
});

export default SearchBox;
