import React, { useContext, useEffect, useState } from 'react';
import createAuth0Client from '@auth0/auth0-spa-js';
import AuthStore from '../../stores/AuthStore';

export const Auth0Context = React.createContext();
export const useAuth0 = () => useContext(Auth0Context);

export const AuthState = {
  UNAUTHENTICATED: 'UNAUTHENTICATED',
  AUTHENTICATED: 'AUTHENTICATED',
  ANONYMOUS: 'ANONYMOUS',
};


export const Auth0Provider = ({ children, ...initOptions }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [authState, setAuthState] = useState(AuthState.UNAUTHENTICATED);
  const [auth0Client, setAuth0] = useState(null);
  const [loading, setLoading] = useState(true);
  const [popupOpen, setPopupOpen] = useState(false);

  useEffect(() => {
    const initAuth0 = async () => {
      const auth0FromHook = await createAuth0Client(initOptions);
      setAuth0(auth0FromHook);

      const isAuthenticated = await auth0FromHook.isAuthenticated();
      setIsAuthenticated(isAuthenticated);

      if (isAuthenticated) {
        const user = await auth0FromHook.getUser();
        const jwt = await auth0FromHook.getTokenSilently();
        AuthStore.setJwt(jwt);
        AuthStore.setUser(user);
        setAuthState(AuthState.AUTHENTICATED);
      } else {
        AuthStore.setUser(null);
        setAuthState(AuthState.ANONYMOUS);
      }

      setLoading(false);
    };

    initAuth0();
  }, []);

  const loginWithPopup = async (params = {}) => {
    setPopupOpen(true);
    try {
      await auth0Client.loginWithPopup(params);
    } catch (error) {
      console.error(error);
    } finally {
      setPopupOpen(false);
    }

    const user = await auth0Client.getUser();
    const jwt = await auth0Client.getTokenSilently();
    AuthStore.setJwt(jwt);
    setAuthState(AuthState.AUTHENTICATED);
    AuthStore.setUser(user);

    setIsAuthenticated(true);
  };

  return (
    <Auth0Context.Provider
      value={{
        isAuthenticated,
        authState,
        loading,
        popupOpen,
        loginWithPopup,
        logout: (...p) => auth0Client.logout(...p),
      }}
    >
      {children}
    </Auth0Context.Provider>
  );
};
