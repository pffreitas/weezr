import React, { useState, useEffect } from 'react';
import styled, { keyframes, css } from 'styled-components';
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import { Title6 } from '../typography';
import { Separator } from '../base';

const ModalOverlay = styled.div`
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.27);
`;

const ModalMainContainer = styled.div`
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1050;
    display: none;

    @media (min-width: 1024px){
        display: ${props => props.open ? 'block' : 'none'};
    }
`;

const slideDownAnimation = keyframes`
  from {
    transform: translateY(0px);
  }

  to {
    transform: translateY(150px);
  }
`;

const ModalContainer = styled.div`
    background-color: #fff;
    position: relative;
    max-width: 500px;
    width: auto;
    margin: 0 auto;
    box-shadow: rgba(0, 0, 0, 0.12) 0px 0px 17px 0px;
    animation: ${slideDownAnimation} 0.3s ease-out;
    transform: translateY(150px);
`;

const Header = styled.div`
  padding: 10px;
`
const Body = styled.div`
  padding: 10px; 
`
const Footer = styled.div`
  padding: 10px;
  padding-bottom: 20px;
`

const MobileModal = styled.div`
  @media (min-width: 1024px){
    display: none;
  }
`;

function useIsServer() {
    const [isServer, setIsServer] = useState(true);
    useEffect(() => {
        setIsServer(false);
    }, []);
    return isServer;
}

const Modal = ({ isOpen, onClose, children }) => {
    const isServer = useIsServer();

    if (isServer) {
        return null;
    }

    return (
        <>
            <ModalMainContainer open={isOpen}>
                <ModalOverlay onClick={onClose} />
                <ModalContainer>
                    {children}
                </ModalContainer>
            </ModalMainContainer>
            <MobileModal>
                <SwipeableBottomSheet open={isOpen} onChange={onClose} style={{ zIndex: 9 }}>
                    {children}
                </SwipeableBottomSheet>
            </MobileModal>
        </>
    );
};

const ModalTitle = ({ children }) => {
    return <Header><Title6>{children}</Title6></Header>
}

const ModalBody = ({ children }) => {
    return <Body>{children}</Body>
}

const ModalFooter = ({ children }) => {
    return (
        <Footer>
            <>
                <Separator />
                {children}
            </>
        </Footer>
    )
}

Modal.Title = ModalTitle;
Modal.Body = ModalBody;
Modal.Footer = ModalFooter;

export default Modal;