import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import Textarea from 'react-expanding-textarea';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  flex: 1;
`;


export const TextInput = ({ onConfirm, onCancel, placeholder, confirmButtonText }) => {

  const [comment, setComment] = useState('');

  const handleInputCommentChange = useCallback((e) => {
    setComment(e.target.value);
  }, []);

  return (
    <Wrapper>
      <Row>
        <Textarea
          className="textarea"
          id="comment-input"
          maxLength="3000"
          name="pet[notes]"
          onChange={handleInputCommentChange}
          placeholder={placeholder}
          value={comment}
        />
      </Row>
      <Row style={{ textAlign: 'right' }}>
        {onCancel && (
          <a className="sv-button link" onClick={onCancel}>
            Cancel
          </a>
        )}
        <a
          className="sv-button default"
          onClick={() => {
            onConfirm(comment);
            setComment('');
          }}
        >
          {confirmButtonText}
        </a>
      </Row>
    </Wrapper>
  );
};
