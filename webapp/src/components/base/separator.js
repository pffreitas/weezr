import React from 'react';
import styled from 'styled-components';
import Color from '../color';

const Separator = styled.hr`
    background: ${Color.Grey300}
`;

export default Separator;