import React from 'react';
import styled from 'styled-components';

export const PaddedContent = styled.div`
  margin: 88px 0 136px 0;
  padding: 20px 20px 20px 20px;
  
  @media (min-width: 1024px){
    margin: 0;
    padding: 88px 35px 20px 35px;
  }
`;

export const CenteredContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`