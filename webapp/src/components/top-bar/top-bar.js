import React from 'react';
import SearchBox from '../search-box/search-box';
import Login from '../login/login';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const TopBarContainer = styled.div`
  padding: 15px 10px;
  position: fixed;
  top: 0;
  z-index: 8;
  background: #fff;
  width: 100%;
  
  @media (min-width: 1024px) {
    position: absolute;
    padding: 15px 35px;
    width: calc(100vw - 250px);
    transform: translateX(250px);
    background: transparent;
  }
`;

const Column = styled.div`
  flex: 1;
`;

const Logo = styled(Link)`
  line-height: 35px;
  display: block;
  height: 35ppx;
  text-align: center;
  
  @media (min-width: 1024px) {
    display: none;
  }
`;

const LogoImg = styled.img`
  height: 20px;
  vertical-align: middle;
`;

const TopBar = () => {
  return (
    <TopBarContainer className="sv-row">
      <Column>
        <SearchBox />
      </Column>
      <Column>
        <Logo to='/' onClick={() => { window.scrollTo(0, 0); }}>
          <LogoImg src="/img/weezr.svg" alt="weezr" />
        </Logo>
      </Column>
      <Column>
        <Login />
      </Column>
    </TopBarContainer>
  );
};

export default TopBar;
