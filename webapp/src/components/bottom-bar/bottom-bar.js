import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const BottomBar = styled.div`
  height: 68px;
  width: 100vw;
  position: fixed;
  background: #fff;
  border: 1px solid #eee;
  bottom: 0;
  text-align: center;
  z-index: 8;
  
  @media (min-width: 1024px) {
    display: none;
  }
`;


const Wrapper = styled(Link)`
  width: 68px;
  height: 68px;
  line-height: 68px;
  margin: 10px 15px;
  display: inline-flex;
  flex-direction: column;
  color: #212121;
  
  :before { 
    font-size: 1.3rem;
  }
`;

const Icon = styled.span`
  font-size: 1.4rem;
  line-height: 28px;
`;

const Label = styled.span`
  line-height: 20px;
  font-size: 0.86rem;
`;

export const BottomBarButton = ({ to, label, icon, onClick }) => {
  return (
    <Wrapper to={to} onClick={onClick}>
      <Icon className={icon} />
      <Label>{label}</Label>
    </Wrapper>
  );
};
