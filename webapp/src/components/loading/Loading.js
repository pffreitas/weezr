import React from 'react';
import styled from 'styled-components';
import './loading.css';

const Wrapper = styled.div`
  text-align: center;
`

export const Loading = ({ loading }) => {
  return (
    <Wrapper>
      {loading && (
        <div className="pulse-loading">
          <div className="one" />
          <div className="two" />
          <div className="three" />
        </div>
      )}
    </Wrapper>
  );
};
