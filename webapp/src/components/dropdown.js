import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import styled from 'styled-components';
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import { DefaultLink, Icon } from './button/button';

const DropdownBackgroundContainer = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.17);
  z-index: 10;
  display: none;
  
  @media (min-width: 1024px){
    display: block;
  }
`;

const DropdownContainer = styled.div`
  position: relative;
   display: inline-block;
`;

const DropdownList = styled.div`
  display: none;
  z-index: 11;
  position: absolute;
  background: #fff;
  box-shadow: 0px 0px 17px 0px rgba(0, 0, 0, 0.12);
  right: 0px;
  border: 1px solid #eee;
  text-align: left;
  border-radius: 4px;
  
  & ::after { 
    bottom: 100%;
    left: 89%;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border: 8px solid transparent;
    border-bottom-color: white;
    margin-left: -8px;
  }
  
  
  @media (min-width: 1024px){
    display:  ${props => props.isOpen ? 'block' : 'none'};;
  }
`;


const MobileDropdownList = styled.div`
  @media (min-width: 1024px){
    display: none;
  }
`;

const List = styled.ul`
  padding: 15px;
  
  & li {
    line-height: 25px;
  }
`;


function useIsServer() {
  const [isServer, setIsServer] = useState(true);
  useEffect(() => {
    setIsServer(false);
  }, []);
  return isServer;
}

export const Dropdown = ({ children, icon, text }) => {
  const [isOpen, setIsOpen] = useState(false);
  const isServer = useIsServer();

  if (isServer) {
    return null;
  }

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const elements = React.Children.toArray(children).map(c => {
    return React.cloneElement(c, { toggle });
  });

  return (
    <>
      {isOpen && <DropdownBackgroundContainer onClick={toggle} />}
      <DropdownContainer>
        <DefaultLink onClick={() => toggle()}>
          <Icon className={classnames('fa', icon)} />
          <span>{text}</span>
        </DefaultLink>
        <DropdownList isOpen={isOpen}>
          <List>{elements}</List>
        </DropdownList>
        <MobileDropdownList>
          <SwipeableBottomSheet open={isOpen} onChange={toggle} style={{ zIndex: 9 }}>
            <List>{elements}</List>
          </SwipeableBottomSheet>
        </MobileDropdownList>
      </DropdownContainer>
    </>
  );
};

export const DropdownItem = ({ text, onClick, toggle }) => {
  return (
    <li>
      <a
        className="sv-button link"
        onClick={() => {
          onClick();
          toggle();
        }}
      >
        <span>{text}</span>
      </a>
    </li>
  );
};

export const ClickableDropdownItem = ({ children, onClick, toggle }) => {
  return <li>{children({ toggle })}</li>;
};

const Separator = styled.hr`
  margin:  20px 0;
`;


export const CreatePlaylistDropdownItem = ({ toggle, value, onChange, onSubmit }) => {
  return (
    <span>
      <Separator />
      <form
        className="sv-form"
        onSubmit={e => {
          e.preventDefault();
          onSubmit();
          toggle();
        }}
      >
        <div className="sv-input-group">
          <input
            type="text"
            name="email"
            placeholder="playlist name"
            value={value}
            onChange={onChange}
          />
          <button
            className="sv-button primary"
            type="button"
            onClick={() => {
              onSubmit();
              toggle();
            }}
          >
            ok
          </button>
        </div>
      </form>
    </span>
  );
};
