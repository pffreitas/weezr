
import React from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react';
import Modal from '../modal/modal';
import { DefaultLink } from '../button/button';
import PlaylistStore from '../../stores/PlaylistStore';
import { InfoButton } from '../button/button';

const CreatePlaylistForm = styled.div`
    display: flex;
`;

const PlaylistName = styled.input`
    flex: 1;
    margin-right: 5px;
`

const CreatePlaylist = ({ value, onChange, onSubmit }) => {
    return (
        <>
            <CreatePlaylistForm>
                <PlaylistName
                    type="text"
                    name="email"
                    placeholder="playlist name"
                    value={value}
                    onChange={onChange}
                />
                <button
                    className="sv-button primary"
                    type="button"
                    onClick={onSubmit}>ok</button>
            </CreatePlaylistForm>
        </>
    );
};

const List = styled.ul`
    max-height: 250px;
    overflow: scroll;
`;

const Item = styled.li`
    & :first-child {
        display: block;
    }
`;

const SaveToPlaylistModal = observer(() => {
    return (
        <Modal isOpen={PlaylistStore.isModalOpen.get()} onClose={PlaylistStore.toggleModal}>
            <Modal.Title>Save to playlist</Modal.Title>
            <Modal.Body>
                <List>
                    <Item key="save-to-playlist-item-listen-later">
                        <DefaultLink onClick={() => PlaylistStore.addItem('listen-later')}>Listen Later</DefaultLink>
                    </Item>
                    {PlaylistStore.playlists
                        .filter(playlist => 'user-defined' === playlist.type)
                        .map(playlist => (
                            <Item key={`save-to-playlist-item-${playlist.id}`}>
                                <DefaultLink key={playlist.id} onClick={() => PlaylistStore.addItem(playlist.id)}>{playlist.title}</DefaultLink>
                            </Item>
                        ))}
                </List>
            </Modal.Body>
            <Modal.Footer>
                <CreatePlaylist
                    value={PlaylistStore.playlistName}
                    onChange={e => PlaylistStore.setPlaylistName(e.target.value)}
                    onSubmit={() => PlaylistStore.createAndAdd()}
                />
            </Modal.Footer>
        </Modal>
    )
})

export default SaveToPlaylistModal;