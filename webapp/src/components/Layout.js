import { Link, withRouter } from 'react-router-dom';
import React from 'react';
import { ToastContainer } from 'react-toastify';
import './layout.css';
import { observer } from 'mobx-react';
import PlaylistStore from '../stores/PlaylistStore';
import ShowStore from '../stores/ShowStore';
import TopBar from './top-bar/top-bar';
import styled from 'styled-components';
import { BottomBar, BottomBarButton } from './bottom-bar/bottom-bar';

export const MenuSectionItem = ({ label, img, link }) => {
  return (
    <li>
      <Link to={link || '/'}>
        <i className={`fa ${img || 'fa-list'}`} />
        <span className="sv-ml--10">{label}</span>
      </Link>
    </li>
  );
};

export class MenuSection extends React.Component {
  render() {
    return (
      <div className="sv-row">
        <div className="sv-column wz-menu-section">
          {this.props.title && <h2>{this.props.title}</h2>}
          <ul>{this.props.children}</ul>
        </div>
      </div>
    );
  }
}

const Menu = observer(() => {
  return (
    <div className="wz-menu">
      <div className="wz-left-bg" />
      <div className="wz-left-bg-2" />
      <div className="wz-left-bg-3" />
      <div className="wz-left">
        <div className="sv-row wz-logo">
          <Link to="/" onClick={() => { window.scrollTo(0, 0); }}>
            <img src="/img/weezr.svg" height="25px" alt="Weezr" />
          </Link>
        </div>
        <MenuSection title="Playlists">
          <MenuSectionItem
            label="Listen Later"
            img="fa-clock-o"
            link="/playlist/listen-later"
          />
          <MenuSectionItem
            label="Liked"
            img="fa-heart-o"
            link="/playlist/liked"
          />
          <MenuSectionItem
            label="History"
            img="fa-history"
            link="/playlist/history"
          />
          {PlaylistStore.playlists
            .filter(playlist => 'user-defined' === playlist.type)
            .map(playlist => {
              return (
                <MenuSectionItem
                  key={playlist.id}
                  label={playlist.title}
                  link={`/playlist/${playlist.id}`}
                />
              );
            })}
        </MenuSection>

        <MenuSection title="Following">
          {ShowStore.following.map(f => {
            return (
              <li key={f.id}>
                <Link to={`/s/${f.slug}`}>{f.name}</Link>
              </li>
            );
          })}
        </MenuSection>
      </div>
    </div>
  );
});

const Content = styled.div`
  @media (min-width: 1024px) {
    margin-left: 250px;
  }
`;

const Layout = ({ children }) => {
  return (
    <div>
      <ToastContainer autoClose={3000} />
      <Menu />
      <TopBar />
      <Content>
        {children}
      </Content>
      <BottomBar>
        <BottomBarButton to="/" label="Home" icon="fa fa-home" onClick={() => { window.scrollTo(0, 0); }} />
        <BottomBarButton to="/trending" label="Trending" icon="fa fa-angle-double-up" />
        <BottomBarButton to="/library" label="Library" icon="fab fa-buffer" />
      </BottomBar>
    </div>
  );
};

export default withRouter(Layout);
