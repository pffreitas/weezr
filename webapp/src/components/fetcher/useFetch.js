import { useEffect, useMemo, useState } from 'react';
import { toast } from 'react-toastify';

function useFetch(initialState, apiPromise, inputs) {
  const [data, setData] = useState(initialState);
  const [loading, setLoading] = useState(true);

  async function fetchUrl() {
    try {
      setData(initialState);
      setLoading(true);

      const res = await apiPromise();
      if (res) {
        setData(res.data || []);
        setLoading(false);
      }
    } catch (err) {
      console.log(err);
      toast.error('Error loading data', {
        position: toast.POSITION.TOP_CENTER,
      });
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchUrl();
  }, inputs);

  return [data, loading, setData];
}

function usePaginatedFetch(initialState, pageSize, apiPromise, inputs) {
  const [data, setData] = useState(initialState || []);
  const [loading, setLoading] = useState(true);
  const [offset, setOffset] = useState(0);

  const loadMore = async () => await fetchUrl(pageSize, pageSize + offset);
  const reset = () => setData([]);

  const fetchUrl = async (limit, offset) => {
    try {
      setLoading(true);

      const res = await apiPromise(limit, offset);
      if (res && res.data) {
        setData([...data, ...res.data]);
        setOffset(offset);
      }
    } catch (err) {
      console.log(err);
      toast.error('Error loading data', {
        position: toast.POSITION.TOP_CENTER,
      });
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => reset(), inputs);

  useEffect(() => {
    fetchUrl(pageSize, 0);
  }, [...inputs]);

  return [data, loading, loadMore, setData];
}

function useInfiniteFetch(initialState, pageSize, apiPromise, inputs) {
  const [data, setData] = useState(initialState ? { data: initialState, offset: 0, hasMore: true } : { data: [], offset: 0, hasMore: true });

  const fetchUrl = async (limit, offset) => {
    try {
      const results = await apiPromise(limit, offset);
      if (results != null && results.length > 0) {
        setData({ offset, data: offset === 0 ? results : [...data.data, ...results], hasMore: true });
      } else {
        setData({ offset, data: data.data, hasMore: false });
      }
    } catch (err) {
      console.log(err);
      toast.error('Error loading data', {
        position: toast.POSITION.TOP_CENTER,
      });
    }
  };

  const loadMore = async () => await fetchUrl(pageSize, data.offset + pageSize);

  const setDataClient = (d) => setData({ data: d, offset: data.offset, hasMore: true });

  const reset = () => setData({ data: [], offset: 0, hasMore: true });

  useMemo(() => {
    reset();
    fetchUrl(pageSize, 0);
  }, inputs);

  return [data.data, loadMore, data.hasMore, setDataClient];
}

export { useFetch, usePaginatedFetch, useInfiniteFetch };
