
import React from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react';
import { Facebook, Linkedin, Reddit, Telegram, Twitter, Whatsapp } from 'react-social-sharing';
import Modal from '../modal/modal';
import ShareStore from '../../stores/ShareStore';

const Container = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
`;

const ShareModal = observer(() => {
    const slug = ShareStore.slug.get();

    return (
        <Modal isOpen={ShareStore.isModalOpen.get()} onClose={ShareStore.toggleModal}>
            <Modal.Title>Share via</Modal.Title>
            <Modal.Body>
                <Container>
                    <Facebook
                        solid
                        onClick={() => ShareStore.share()}
                        link={`https://www.weezr.me/e/${slug}`}
                    />
                    <Linkedin
                        solid
                        onClick={() => ShareStore.share()}
                        link={`https://www.weezr.me/e/${slug}`}
                    />
                    <Reddit
                        solid
                        onClick={() => ShareStore.share()}
                        link={`https://www.weezr.me/e/${slug}`}
                    />
                    <Twitter
                        solid
                        onClick={() => ShareStore.share()}
                        link={`https://www.weezr.me/e/${slug}`}
                    />
                    <Telegram
                        solid
                        onClick={() => ShareStore.share()}
                        link={`https://www.weezr.me/e/${slug}`}
                    />
                    <Whatsapp
                        solid
                        onClick={() => ShareStore.share()}
                        link={`https://www.weezr.me/e/${slug}`}
                    />
                </Container>
            </Modal.Body>
        </Modal>
    )
})

export default ShareModal;