import styled from 'styled-components';



const Title4 = styled.h4`
  font-family: Montserrat, Roboto, Helvetica, Arial, sans-serif;
  color: #4e4e4e;
  font-size: 2.125rem;
  font-weight: 500;
  line-height: 1.235;
  letter-spacing: 0em;
`;

const Title5 = styled.h5`
  font-family: Montserrat, Roboto, Helvetica, Arial, sans-serif;
  color: #4e4e4e;
  font-size: 1.5rem;
  font-weight: 500;
  line-height: 1.334;
  letter-spacing: 0em;
`;

const Title6 = styled.h6`
  font-family: Montserrat, Roboto, Helvetica, Arial, sans-serif;
  font-weight: 500;
  line-height: 1.6;
  color: #4e4e4e;
  font-size: 1.25rem;
`;

const SubTitle1 = styled.p`
  font-family: Montserrat, Roboto, Helvetica, Arial, sans-serif;
  font-weight: 400;
  color: #4e4e4e;
  font-size: 1rem;
`;

export { Title4, Title5, Title6, SubTitle1 };
