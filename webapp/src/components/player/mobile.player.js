import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import classnames from 'classnames';
import PlayerStore from '../../stores/PlayerStore';
import { toJS } from 'mobx';
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import { PlayButton } from './components/buttons/play.button';
import { ProgressSlider } from './components/progress/progress.slider';
import { Title6 } from '../typography';
import { ImgPlaceholder, TitlePlaceholder } from './components/placeholder/episode.placeholder';
import Forward from './components/buttons/forward.button'
import Backward from './components/buttons/backward.button'
import { Spacing } from '../style';
import { ShareButton, SaveButton, LikeButton } from '../episode/episode-card-buttons';

const MobilePlayerWrapper = styled.div`
  @media (min-width: 1024px) {
    display: none;
  } 
`;

const MobilePlayerContainer = styled.div`
  height: calc(100vh - 80px);
  background-color: ${props => props.isOpen ? '#fff' : 'transparent'};
  border-radius:  ${props => props.isOpen ? '20px 20px 0 0' : '0px'};
`;

const MobilePlayerContainerCollapsed = styled.div`
  height: 68px;
  width: 100vw;
  position: relative;
  background-color: #f9fbfc;
  -webkit-transition: transform 0.5s ease;
  -o-transition: transform 0.5s ease;
  transition: transform 0.5s ease;
  display: flex;
  flex-direction: column;
  border-top: 1px solid #ddd;
  
  &.hidden {
    transform: translateY(68px);
  }
`;

const MobilePlayerContainerCollapsedContent = styled.div`
  padding: 10px;
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 25px;

  img {
    border-radius: 6px;
  }
`;

const EpisodeTitle = styled(Title6)`
  font-size: 1rem;
  line-height: 1.18rem;
  margin: 0;
  padding: 0;
  align-self: center;
  justify-self: start;
  max-height: 32px;
  overflow: hidden;
`;

const MobilePlayerExpandedContainer = styled.div`
  padding-top: 1px;
`;

const ImageContainer = styled.div`
  width: 100%;
  margin-top: 35px;
  text-align: center;
  
  img {
    border-radius: 6px;
  }
`;

const TitleContainer = styled.div`
  margin-top: 35px;
  
`;

const Title = styled(Title6)`
  text-align: center;
  max-width: 100vw;
  max-height: 4rem;
  overflow: hidden;
  padding: 0 15px;
`;

const SubTitle = styled.h4`
  text-align: center;
  font-size: 1rem;
  color: #656565;
  font-weight: normal;
  
`;

const PlayActionsContainer = styled.div`
  margin-top: 35px;
  display: flex;
  justify-content: space-around;
  align-items: flex-end;
`;

const ProgressContainer = styled.div`
  margin: 40px 35px 10px;
`;

const EpisodeInfo = styled.div`
  display: grid;
  grid-template-columns: 45px 1fr;
  grid-gap: 15px;
`

const EpisodeActionsContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

function useIsServer() {
  const [isServer, setIsServer] = useState(true);
  useEffect(() => {
    setIsServer(false);
  }, []);
  return isServer;
}

const MobilePlayer = observer(({ audioRef }) => {
  const episode = toJS(PlayerStore.episode);
  const isServer = useIsServer();
  const [isOpen, setIsOpen] = useState(false);
  const [render, setRender] = useState(false);

  useEffect(() => {
    setRender(window.innerWidth < 1024);
  }, []);

  if (isServer) {
    return null;
  }

  window.addEventListener('resize', (e) => {
    setRender(window.innerWidth < 1024);
  });

  if (!render) {
    return null;
  }

  return (
    <MobilePlayerWrapper>
      <SwipeableBottomSheet overflowHeight={PlayerStore.isOpen.get() ? isOpen ? 0 : 136 : 0} open={isOpen}
        onChange={(b) => setIsOpen(b)} marginTop={20}
        bodyStyle={{ background: 'transparent' }} topShadow={false}
        style={{ zIndex: isOpen ? 9 : 7 }}>
        <MobilePlayerContainer isOpen={isOpen} onClick={() => setIsOpen(true)}>
          {!isOpen && (
            <MobilePlayerContainerCollapsed className={classnames({ hidden: !PlayerStore.isOpen.get() })}>

              <MobilePlayerContainerCollapsedContent>
                {!episode.audio_file_url && (
                  <EpisodeInfo>
                    <ImgPlaceholder />
                    <TitlePlaceholder />
                  </EpisodeInfo>
                )}

                {episode.audio_file_url && (
                  <EpisodeInfo>
                    <img src={`https://img.weezr.me/${episode.image_key}-60.png`} width="45px" height="45px" />
                    <EpisodeTitle>{episode.title}</EpisodeTitle>
                  </EpisodeInfo>
                )}
                <PlayButton size={Spacing.Size.S} audioRef={audioRef} />
              </MobilePlayerContainerCollapsedContent>
              <ProgressSlider collapsed audioRef={audioRef} episodeSlug={episode.slug} />

            </MobilePlayerContainerCollapsed>
          )}
          {isOpen && (
            <MobilePlayerExpandedContainer>
              <ImageContainer>
                <img src={`https://img.weezr.me/${episode && episode.image_key}-240.png`} width="180px" height="180px" />
              </ImageContainer>

              <TitleContainer>
                <Title>{episode && episode.title}</Title>
                <SubTitle>{episode && episode.showTitle}</SubTitle>
              </TitleContainer>

              <PlayActionsContainer>
                <Backward onClick={() => audioRef.current.currentTime -= 10} />
                <PlayButton size={Spacing.Size.L} audioRef={audioRef} />
                <Forward onClick={() => audioRef.current.currentTime += 30} />
              </PlayActionsContainer>

              <ProgressContainer>
                <ProgressSlider audioRef={audioRef} episodeSlug={episode.slug} />
              </ProgressContainer>

              <EpisodeActionsContainer>
                <ShareButton strong slug={episode.slug} />
                <SaveButton strong episodeId={episode.id} />
                <LikeButton strong slug={episode.slug} likedByCurrentUser={episode.likeByCurrentUser} />
              </EpisodeActionsContainer>

            </MobilePlayerExpandedContainer>
          )}
        </MobilePlayerContainer>
      </SwipeableBottomSheet>
    </MobilePlayerWrapper>
  );
});

export default MobilePlayer;
