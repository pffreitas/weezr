import React, { useEffect } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import _isEmpty from 'lodash/isEmpty';
import PlayerStore from '../../stores/PlayerStore';

const Audio = observer(({ audioRef }) => {
  const episode = toJS(PlayerStore.episode);
  const initialTime = toJS(PlayerStore.initialTime);

  useEffect(() => {
    if (!_isEmpty(episode.audio_file_url) && audioRef.current.src !== episode.audio_file_url) {
      audioRef.current.src = episode.audio_file_url;
      audioRef.current.play();
    }

    if ('mediaSession' in navigator) {
      navigator.mediaSession.metadata = new MediaMetadata({
        title: episode.title,
        artist: episode.showTitle,
        album: episode.showTitle,
        artwork: [{
          src: `https://img.weezr.me/${episode.image_key}-240.png`,
          sizes: '240x240',
          type: 'image/png'
        }]
      });
      navigator.mediaSession.setActionHandler('seekbackward', () => audioRef.current.currentTime -= 10);
      navigator.mediaSession.setActionHandler('seekforward', () => audioRef.current.currentTime += 30);
    }
  }, [episode.audio_file_url]);

  useEffect(() => {
    if (audioRef && audioRef.current) {
      audioRef.current.currentTime = initialTime;
    }
  }, [initialTime]);

  return (
    <audio ref={audioRef} preload="auto" />
  );
});


export default Audio;
