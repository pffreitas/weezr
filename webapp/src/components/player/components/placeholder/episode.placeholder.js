import React from 'react';
import styled, { keyframes } from 'styled-components';
import Color from '../../../color';

const loading = keyframes`
  100% {
    transform: translateX(100%);
  }
`;

const Placeholder = styled.div`
  background-color: ${Color.Grey200};
  border-radius: 4px;
  position: relative;
  overflow: hidden;

  &::after {
    display: block;
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    transform: translateX(-100%);
    background: linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.6), transparent);
    animation: ${loading} 1.5s infinite;
  }
`
const ImgPlaceholder = styled(Placeholder)`
  max-width: 45px;
  max-height: 45px;
`;

const TitlePlaceholder = styled(Placeholder)`
  height: 20px;
`;

export {
  ImgPlaceholder,
  TitlePlaceholder,
}