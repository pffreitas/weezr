import React from 'react';
import styled from 'styled-components';
import BackwardIcon from '../icons/backward.svg';
import { Button } from '../../../button';
import Color from '../../../color'

const Backward = styled(BackwardIcon)`
    width: 24px;
    height: auto;
    fill: ${Color.BlueGrey800};
    stroke: ${Color.BlueGrey800};
`;

const BackwardButton = ({ onClick }) => {
    return (
        <Button onClick={onClick}>
            <Backward />
        </Button>
    )
};

export default BackwardButton;