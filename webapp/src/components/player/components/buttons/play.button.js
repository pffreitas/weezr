import React, { useState } from 'react';
import styled from 'styled-components';
import PlayIcon from '../icons/play.svg';
import PauseIcon from '../icons/pause.svg';
import Color from '../../../color'

const Button = styled.button`
  border: 0;
  background: transparent;
  width: ${props => props.width};
  height: auto;

  svg {
    fill: ${Color.BlueGrey800};
  }
`;

export const PlayButton = ({ size, audioRef }) => {
  const [isPaused, setIsPaused] = useState(audioRef.current.paused);

  audioRef.current.onplay = () => {
    setIsPaused(false);
  }

  audioRef.current.onpause = () => {
    setIsPaused(true);
  }

  const play = (e) => {
    setIsPaused(false);
    audioRef.current.play();

    e.stopPropagation();
    e.cancelBubble = true;
  }

  const pause = (e) => {
    setIsPaused(true);
    audioRef.current.pause();

    e.stopPropagation();
    e.cancelBubble = true;
  }

  return (
    <>
      {isPaused && (
        <Button width={size} onClick={play} >
          <PlayIcon />
        </Button>)}

      {!isPaused && (
        <Button width={size} onClick={pause} >
          <PauseIcon />
        </Button>
      )}
    </>
  );
};
