import React from 'react';
import styled from 'styled-components';
import ForwardIcon from '../icons/forward.svg';
import { Button } from '../../../button';
import Color from '../../../color'

const Forward = styled(ForwardIcon)`
    width: 24px;
    height: auto;
    fill: ${Color.BlueGrey800};
    stroke: ${Color.BlueGrey800};
`;

const ForwardButton = ({ onClick }) => {
    return (
        <Button onClick={onClick}>
            <Forward />
        </Button>
    )
};

export default ForwardButton;