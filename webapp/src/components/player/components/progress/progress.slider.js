import React, { useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components';
import * as moment from 'moment';
import _debounce from 'lodash/debounce';
import ListenAPI from '../../../../api/ListenAPI';
import Color from '../../../color';
import Error from '../icons/error.svg';
import Warn from '../icons/warn.svg';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;


const Slider = styled.input(({ networkState }) => `
    position: relative;
    top: 0px;
    -webkit-appearance: none;
    background: transparent;
    height: 4px;
    border-radius: 6px;
    width: 100%;
    position: absolute;

    &:disabled::-webkit-slider-thumb {
      background: ${networkState === 1 ? Color.Yellow600 : networkState == 2 ? Color.Red600 : Color.BlueGrey100};
      box-shadow: none;
    }
    
    &::-webkit-slider-thumb {
      -webkit-appearance: none; /* Override default look */
      appearance: none;
      width: 12px;
      height: 12px;
      background: #159958;
      border-radius: 30px;
      box-shadow: 0px 0px 4px 1px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      z-index: 10;
    }

    &::-moz-range-thumb {
      width: 12px;
      height: 12px;
      background: #159958;
      border-radius: 30px;
      box-shadow: 0px 0px 4px 1px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      z-index: 10;
    }
`);

const Track = styled.div(({ collapsed }) => `
  width: 100%;
  height: ${collapsed ? '2px' : '4px'};
  border-radius: ${collapsed ? '0' : '6px'};
  background-color: ${ Color.BlueGrey100};
  position: relative;
`);

const Progress = styled.div(({ collapsed }) => `
  height: ${collapsed ? '2px' : '4px'};
  border-radius: ${collapsed ? '0' : '6px'};
  background-color: #159958;
  position: absolute;
`);

const loadingKeyframes = keyframes`
    0%,
    100% {
        background-color: cornflowerblue;
        width: 0;
    }
    0% {
        float: left;
    }
    100% {
        float: right;
    }
    50% {
        background-color: lime;
        width: 100%;
    }
`;

const Loading = styled.div(({ collapsed, loading }) => `
  position: absolute;
  height: ${collapsed ? '2px' : '4px'};
  border-radius: ${collapsed ? '0' : '6px'};
  background-color: ${Color.BlueGrey100};
  display: ${loading ? 'block' : 'none'};      
`);

const LoadingBar = styled.div`
  height: 100%;
  border-radius: 6px;
  background-color: #159958;
  width: 100%;
  position: absolute;
  transition: 0.7s all ease;
  animation: ${loadingKeyframes} 2s ease-in-out alternate infinite;
  position: relative;
`;

const Buffer = styled.div`
  position: absolute;
  height: 4px;
  border-radius: 6px;
  background-color: ${Color.Grey400};
`;

const DurationContainer = styled.div`
  display: flex;
  margin-top: 5px;
`;

const Label = styled.div(({ flex, justifyContent }) => `
  flex: ${flex};
  justify-content: ${justifyContent};
  display: flex;
`);

const Chip = styled.div(({ background, color }) => `
    background-color: ${background};
    color: ${color};
    border-radius: 40px;
    height: 22px;
    display: flex;
    align-items: center;
    flex: 1;
    max-width: 200px;

    svg {
      width: 16px;
      height: auto;
      margin-left: 4px;
      stroke-width: 2px;
      stroke: ${color};
      fill: ${color};
    }
`);

const ChipLabel = styled.div(({ color }) => `
    color: ${color};
    text-transform: uppercase;
    font-size: 0.8rem;
    line-height: 0.8rem;
    flex: 1;
    text-align: center;
`);

export const ProgressSlider = ({ collapsed, audioRef, episodeSlug }) => {
  const [duration, setDuration] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [networkState, setNetworkState] = useState(0);

  const progress = currentTime * 100 / duration;


  const debouncedTrackProcess = _debounce(
    (slug, currentTime, duration) => ListenAPI.trackProgress(slug, parseInt(currentTime, 10), parseInt(duration, 10)),
    1000,
    { leading: true, trailing: false, });

  useEffect(() => {
    setDuration(audioRef.current.duration);
    setCurrentTime(audioRef.current.currentTime);

    audioRef.current.ondurationchange = () => setDuration(audioRef.current.duration);

    audioRef.current.ontimeupdate = () => {
      if (audioRef.current && audioRef.current.error == null) {
        setCurrentTime(audioRef.current.currentTime);

        const currentTimeInt = parseInt(audioRef.current.currentTime, 10)
        if (episodeSlug != null && currentTimeInt > 0 && currentTimeInt % 10 === 0) {
          debouncedTrackProcess(episodeSlug, audioRef.current.currentTime, audioRef.current.duration);
        }
      }
    }

    audioRef.current.onstalled = () => {
      setNetworkState(1);
    }

    audioRef.current.onerror = (e) => {
      setNetworkState(2);

      setTimeout(() => {
        const currentTimeSnapshot = audioRef.current.currentTime;

        audioRef.current.load();
        audioRef.current.play();
        audioRef.current.currentTime = currentTimeSnapshot;
      }, 5000);
    }

  }, [episodeSlug]);

  useEffect(() => {
    if (audioRef.current.readyState <= 1) {
      setIsLoading(true);
    }

    if (audioRef.current.readyState >= 3) {
      setNetworkState(0);
      setIsLoading(false);
    }
  }, [audioRef.current.readyState]);

  let lastBuffer = 0;
  if (audioRef.current != null && audioRef.current.buffered != null) {
    for (let i = 0; i < audioRef.current.buffered.length; i++) {
      const end = audioRef.current.buffered.end(i);

      if (end > currentTime) {
        lastBuffer = end;
        break;
      }
    }
  }

  return (
    <Container>
      {collapsed && (
        <Track collapsed={collapsed}>
          <Progress style={{ width: `${progress}%` }} collapsed={collapsed} />
          <Loading loading={isLoading ? 1 : 0} collapsed={collapsed} style={{ width: '100%' }}>
            <LoadingBar />
          </Loading>
        </Track>
      )}
      {!collapsed && (
        <>
          <Track>
            {!isLoading && <Buffer style={{ width: `${lastBuffer * 100 / duration}%` }} />}
            <Progress style={{ width: `${progress}%` }} />
            <Loading loading={isLoading ? 1 : 0} style={{ width: `${progress}%` }} >
              <LoadingBar />
            </Loading>

            <Slider
              type="range"
              className="seeker"
              value={currentTime}
              disabled={isLoading}
              max={duration || 0}
              networkState={networkState}
              onChange={e => {
                setCurrentTime(e.target.value);
                audioRef.current.currentTime = e.target.value;
              }}
            />
          </Track>
          <DurationContainer>
            <Label flex={1}> {
              moment()
                .hour(0)
                .minute(0)
                .second(currentTime)
                .format('HH:mm:ss')
            }
            </Label>
            <Label flex={2} justifyContent={'center'}>
              {networkState > 0 && (
                <Chip background={networkState === 1 ? Color.Yellow600 : networkState === 2 ? Color.Red500 : Color.BlueGrey200}
                  color={networkState === 1 ? Color.BlueGrey800 : networkState === 2 ? Color.White : Color.BlueGrey200}>
                  {networkState === 1 && (
                    <>
                      <Warn />
                      <ChipLabel color={Color.BlueGrey800}>Network unstable</ChipLabel>
                    </>
                  )}
                  {networkState === 2 && (
                    <>
                      <Error />
                      <ChipLabel color={Color.White}>Network error</ChipLabel>
                    </>
                  )}
                </Chip>
              )}
            </Label>
            <Label flex={1} justifyContent={'flex-end'}>{moment()
              .hour(0)
              .minute(0)
              .second(duration)
              .format('HH:mm:ss')}
            </Label>
          </DurationContainer>
        </>
      )}
    </Container>
  );
};
