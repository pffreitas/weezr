import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import styled, { keyframes } from 'styled-components';
import classnames from 'classnames';
import PlayerStore from '../../stores/PlayerStore';
import { toJS } from 'mobx';
import { PlayButton } from './components/buttons/play.button';
import { ProgressSlider } from './components/progress/progress.slider';
import _isEmpty from 'lodash/isEmpty';
import { ImgPlaceholder, TitlePlaceholder } from './components/placeholder/episode.placeholder';
import { Title6 } from '../typography';
import { Spacing } from '../style';

const DesktopPlayerContainerCollapsed = styled.div`
  height: 68px;
  padding: 10px;
  width: 100vw;
  position: fixed;
  bottom: 0;
  background-color: #f9fbfc;
  -webkit-transition: transform 0.5s ease;
  -o-transition: transform 0.5s ease;
  transition: transform 0.5s ease;
  z-index: 9;
  border-top: 1px solid #ddd;
  
  &.hidden {
    transform: translateY(68px);
  }
  
  display: none;
  @media (min-width: 1024px) {
    display: grid;
  } 

  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 15px;
`;

const EpisodeTitle = styled(Title6)`
  max-height: 1.6em;
  overflow: hidden;
`;

const Img = styled.img`
  max-width: 45px;
  max-height: 45px;
  border-radius: 4px;
`;


const PlayerContainer = styled.div`
  display: grid;
  grid-template-columns: 45px 1fr;
  grid-gap: 15px;
  align-items: center;
`;

const EpisodeInfo = styled.div`
  display: grid;
  grid-template-columns: 45px 1fr;
  grid-gap: 15px;
`

function useIsServer() {
  const [isServer, setIsServer] = useState(true);
  useEffect(() => {
    setIsServer(false);
  }, []);
  return isServer;
}

const Player = observer(({ audioRef }) => {
  const episode = toJS(PlayerStore.episode);
  const isServer = useIsServer();
  const [render, setRender] = useState(false);

  useEffect(() => {
    setRender(window.innerWidth >= 1024);
  }, []);

  if (isServer) {
    return null;
  }

  window.addEventListener('resize', (e) => {
    setRender(window.innerWidth >= 1024);
  });

  if (!render) {
    return null;
  }

  return (
    <DesktopPlayerContainerCollapsed className={classnames({ hidden: !PlayerStore.isOpen.get() })}>
      {episode.audio_file_url && (
        <EpisodeInfo>
          <Img src={`https://img.weezr.me/${episode.image_key}-60.png`} />
          <EpisodeTitle>{episode.title}</EpisodeTitle>
        </EpisodeInfo>
      )}
      {!episode.audio_file_url && (
        <EpisodeInfo>
          <ImgPlaceholder />
          <TitlePlaceholder />
        </EpisodeInfo>
      )}

      <PlayerContainer>
        <PlayButton size={Spacing.Size.M} audioRef={audioRef} />
        <ProgressSlider audioRef={audioRef} episodeSlug={episode.slug} />
      </PlayerContainer>
    </DesktopPlayerContainerCollapsed>
  );
});

export default Player;
