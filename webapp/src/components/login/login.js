import React, { useState } from 'react';
import { useAuth0 } from '../auth/AuthProvider';
import AuthStore from '../../stores/AuthStore';
import classnames from 'classnames';

const Login = () => {
  const { loginWithPopup, logout } = useAuth0();
  const user = AuthStore.getUser();
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className="wz-user">
      {user && (
        <div className="wz-user-wg">
          <img
            src={user.picture}
            width="45px"
            height="auto"
            onClick={() => setIsOpen(!isOpen)}
          />
        </div>
      )}
      {!user && (
        <a
          href="#"
          className="sv-button sv-link"
          onClick={() => loginWithPopup()}
        >
          sign in
        </a>
      )}
      <div className={classnames({ 'hidden': !isOpen }, 'wz-user-details')}>
        <div className="sv-row">
          <div className="sv-column">
            <span className="wz-user-name">{user && user.name}</span>
          </div>
        </div>
        <div className="sv-row">
          <div className="sv-column">
            <hr/>
          </div>
        </div>
        <div className="sv-row sv-no-margins">
          <div className="sv-column">
            <ul>
              <li>
                <a
                  className="sv-button sv-link"
                  onClick={() => logout({ returnTo: process.env.PUBLIC_URL })}
                >
                  <i className="fa fa-sign-out-alt sv-mh--10"/>
                  Logout
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};


export default Login;
