import React from 'react';
import styled from 'styled-components';

const HorizontalScroll = styled.div`
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    overflow-x: scroll;
    scroll-snap-type: x proximity;
    grid-template-columns: 1px;
    grid-auto-columns: 275px;
    padding-bottom: 15px;
    margin-bottom: 15px;
    scrollbar-width: none;
    
    ::-webkit-scrollbar {
      display: none;
    }
    
    :before,
    :after {
      content: '';
      width: 1px;
    }
`;

export default HorizontalScroll;