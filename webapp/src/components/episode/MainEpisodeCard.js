import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { LikeButton, PlayButton, SaveButton, ShareButton } from './episode-card-buttons';
import Color from '../color';

const Card = styled.div((props) => `
  display: flex;
  flex-direction: column;
  background: #fbfbfb;
  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);
  border-radius: 4px;  
  height: ${props.height ? `${props.height}px` : '100%'};
  opacity: ${props.disabled ? '0.2' : 1};
`);

const MainEpisodeCardContainer = styled.div`
  display: grid;
  grid-template-columns: 45px 1fr;
  grid-template-rows: 45px min-content auto;
  grid-gap: 15px 10px;
  padding: 12px;
  flex: 1;
`;

const ShowImage = styled.img`
  width: 45px;
  height: 45px;
  display: block;
  overflow: hidden;
  border-radius: 4px;
`;

const ShowTitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const ShowTitle = styled.p`
  margin: 0;
  padding: 0;
  max-width: 100%;
  overflow: hidden;
`;

const PublishedAt = styled.p`
  margin: 0;
  padding: 0;
  color: #888;
`;

const EpisodeTitle = styled.p`
  grid-column: 1 / 3;
  font-family: Montserrat, Roboto, Helvetica, Arial, sans-serif; 
  font-weight: 600;
  font-style: normal;
  font-size: 1.15em;
  margin: 0;
  padding: 0;
  max-height: 100px;
  line-height: 25px;
  overflow: hidden;
  
  & a {
    font-weight: 600;
    color: #212121;
  }
    
  & a:hover {
    color: #344a7d;
  }
`;

const EpisodeDescription = styled.p`
  grid-column: 1 / 3;
  font-family: Roboto, Helvetica, Arial, sans-serif; 
  font-weight: 400;
  font-style: normal;
  word-break: break-word;
  margin: 0;
  padding: 0;
  color: #666;
`;

const ToolBar = styled.div`
  grid-column: 1 / 3;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

const Tools = ({ children }) => children;

const ProgressTrack = styled.div`
  grid-column: 1 / 3;
  background-color: ${Color.Grey300};
  height: 2px;
  border-radius: 0 0 4px 4px;
`;

const ProgressBar = styled.div`
  background-color: ${Color.Indigo300};
  height: 2px;
  border-radius: 0 0 0 4px;
`;

const Progress = ({ finished, total, progress }) => {
  if (!progress || progress <= 0 || finished) {
    return null;
  }

  return (
    <ProgressTrack>
      <ProgressBar style={{ width: `${progress * 100 / total || 0}%` }} />
    </ProgressTrack>
  )
}

const MainEpisodeCard = ({ children, item, height, showDesc = true, disabled }) => {
  if (!item) {
    return null;
  }

  const additionalTools = React.Children.toArray(children).filter(c => c.type == Tools);

  return (
    <Card height={height} disabled={disabled}>
      <MainEpisodeCardContainer>
        <ShowImage src={`https://img.weezr.me/${item.image_key}-60.png`} />

        <ShowTitleContainer>
          <ShowTitle><Link to={`/s/${item.show_slug}`}>{item.show_title}</Link></ShowTitle>
          <PublishedAt>{moment(item.published_at).fromNow()}</PublishedAt>
        </ShowTitleContainer>

        <EpisodeTitle><Link to={`/e/${item.slug}`}>{item.title}</Link></EpisodeTitle>

        {<EpisodeDescription>{showDesc && item.description}</EpisodeDescription>}

        <ToolBar>
          {additionalTools.map(t => t)}
          <SaveButton episodeId={item.id} />
          <ShareButton slug={item.slug} />
          <LikeButton slug={item.slug} likedByCurrentUser={item.likeByCurrentUser} />
          <PlayButton slug={item.slug} />
        </ToolBar>

      </MainEpisodeCardContainer>
      <Progress finished={item.finished} total={item.total_length} progress={item.progress} />
    </Card>
  );
};


MainEpisodeCard.Tools = Tools;

export { MainEpisodeCard };
