import React, { useState } from 'react';
import styled from 'styled-components';
import numeral from 'numeral';
import Play from './icons/play.svg';
import Share from './icons/share.svg';
import Save from './icons/save.svg';
import Like from './icons/like.svg';
import Liked from './icons/liked.svg';
import Trash from './icons/trash.svg';
import { RoundedLink } from '../button/button';
import PlaylistStore from '../../stores/PlaylistStore';
import ShareStore from '../../stores/ShareStore';
import PlayerStore from '../../stores/PlayerStore';
import Color from '../color';
import ListenAPI from '../../api/ListenAPI';

const Icon = styled.div(({ strong }) => `
    svg {
        fill: ${strong ? Color.BlueGrey700 : Color.BlueGrey200};
        stroke: ${strong ? Color.BlueGrey700 : Color.BlueGrey200};
        stroke-width: ${strong ? '2px' : '0px'};
    }

    ${RoundedLink}:hover & svg  {
        fill: ${Color.Indigo500};
        stroke: ${Color.Indigo500};
        stroke-width: 2px;
      }
`);

const ShareButton = ({ slug, strong }) => {
    return (
        <RoundedLink onClick={() => ShareStore.toggleModal(slug)}>
            <Icon strong={strong}>
                <Share />
            </Icon>
        </RoundedLink>
    )
}

const SaveButton = ({ episodeId, strong }) => {
    return (
        <RoundedLink onClick={() => PlaylistStore.toggleModal(episodeId)}>
            <Icon strong={strong}>
                <Save />
            </Icon>
        </RoundedLink>
    )
}

const LikeButtonWrapper = styled.div`
    cursor: pointer;
    display: flex;

    span {
        font-size: 0.8rem;
        max-width: 48px;
        line-height: 48px;
        text-align: center;
        color: ${Color.BlueGrey700};
        margin: 0 10px;
    }
`;


const LikeButton = ({ slug, strong, likedByCurrentUser, showLikeCount, likeCount }) => {
    const [liked, setLiked] = useState(likedByCurrentUser);
    const [likeCnt, setLikeCount] = useState(likeCount);

    const unlike = () => {
        ListenAPI.undoLike(slug)
            .then(({ data }) => {
                setLiked(data.likeByCurrentUser);
                setLikeCount(data.likeCount);
            })
            .catch(err => {
                console.log(err);
                toast.error('Failed to like episode', {
                    position: toast.POSITION.TOP_CENTER,
                });
            });
    }

    const like = () => {
        ListenAPI.like(slug)
            .then(({ data }) => {
                setLiked(data.likeByCurrentUser);
                setLikeCount(data.likeCount);
            })
            .catch(err => {
                console.log(err);
                toast.error('Failed to like episode', {
                    position: toast.POSITION.TOP_CENTER,
                });
            });
    }

    if (liked) {
        return (
            <LikeButtonWrapper onClick={() => unlike()}>
                <RoundedLink>
                    <Liked fill={Color.Red300} />
                </RoundedLink>
                <span>{showLikeCount && likeCnt && numeral(likeCnt).format('0 a')}</span>
            </LikeButtonWrapper>
        )
    }

    return (
        <LikeButtonWrapper onClick={() => like()}>
            <RoundedLink>
                <Icon strong={strong}>
                    <Like />
                </Icon>
            </RoundedLink>
            <span>{showLikeCount && likeCnt && numeral(likeCnt).format('0 a')}</span>
        </LikeButtonWrapper>
    )
}

const PlayButton = ({ slug, strong }) => {
    return (
        <RoundedLink onClick={() => PlayerStore.openAndPlay(slug)}>
            <Icon strong={strong}>
                <Play />
            </Icon>
        </RoundedLink>
    )
}

const RemoveEpisodeFromPlaylistButton = ({ strong, onClick }) => {
    return (
        <RoundedLink onClick={onClick}>
            <Icon strong={strong}>
                <Trash />
            </Icon>
        </RoundedLink>
    )
}

export { ShareButton, SaveButton, LikeButton, PlayButton, RemoveEpisodeFromPlaylistButton };