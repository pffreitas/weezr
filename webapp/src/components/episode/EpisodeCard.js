import { Link } from 'react-router-dom';
import moment from 'moment';
import React from 'react';
import './episode-cards.css';
import classnames from 'classnames';
import styled from 'styled-components';

export const EpisodeCard = ({ item, className, children }) => {
  return (
    <Link to={`/e/${item.slug}`}>
      <div className={classnames('wz-feed-item', className)}>
        <div className="wz-feed-item-img">
          <img src={`https://img.weezr.me/${item.image_key}-60.png`} width="75px" height="75px" alt={item.title} />
        </div>
        <div className="wz-feed-item-data">
          <p className="episode-title">{item.title}</p>
          <p className="show-title">{item.showTitle}</p>
          <p className="published-at" title={item.published_at}>{moment(item.published_at).fromNow()}</p>
        </div>
        <div className="wz-feed-items-action-bar">
          <div>{children}</div>
        </div>
      </div>
    </Link>
  );
};

export const CondensedEpisodeCard = ({ item }) => {
  return (
    <Link to={`/e/${item.slug}`}>
      <div className="wz-feed-item condensed">
        <div className="wz-feed-item-img">
          <img src={`https://img.weezr.me/${item.image_key}-60.png`} width="75px" height="75px" />
        </div>
        <div className=" wz-feed-item-data">
          <p className="episode-title">{item.title}</p>
          <p className="published-at" title={item.published_at}>{moment(item.published_at).fromNow()}</p>
        </div>
      </div>
    </Link>
  );
};


const SimpleCardShow = styled.div`
  display: flex;
  margin-bottom: 10px;
`;

const SimpleCardShowAvatar = styled.div`
  width: 45px;
`;

const ShowImage = styled.img`
    max-width: 45px;
    max-height: 45px;
    overflow: hidden;
`;

const SimpleCardShowName = styled.div`
  height: 45px;
  padding: 5px;
  margin-left: 5px;
`;

export const SimpleEpisodeCard = ({ item }) => {
  return (
    <div className="wz-feed-item simple">
      <SimpleCardShow>
        <SimpleCardShowAvatar>
          <ShowImage src={`https://img.weezr.me/${item.image_key}-60.png`} width="45px" height="45px"
            alt={item.title} />
        </SimpleCardShowAvatar>
        <SimpleCardShowName>
          <Link to={`/s/${item.show_slug}`}>
            <p className="show-title">{item.show_title}</p>
          </Link>
          <p className="published-at" title={item.published_at}>{moment(item.published_at).fromNow()}</p>
        </SimpleCardShowName>
      </SimpleCardShow>
      <div>
        <Link to={`/e/${item.slug}`}>
          <p className="episode-title">{item.title}</p>
        </Link>
      </div>
    </div>
  );
};
