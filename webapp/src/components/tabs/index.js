import React, { useState } from 'react';
import { SubTitle1 } from '../typography';
import styled from 'styled-components';


const TabContainer = styled.div`
  display: grid;
`;

const TabHeaderContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-auto-columns: max-content;
  margin: 20px 0 30px;
  border-bottom: 1px solid #e6e6e6;
`;

const TabHeader = styled.a`
  padding: 0 10px;
  border-bottom: 2px solid ${props => props.active ? 'cornflowerblue' : 'transparent'};
  
  :hover { 
    color: #000;
  }
`;

const TabContent = styled.div``;

const Tabs = ({ children }) => {
  const [selectedTab, setSelectedTab] = useState(0);
  const titles = children.map(c => c.props.text);
  const content = children.map(c => c.props.children);

  return (
    <TabContainer>
      <TabHeaderContainer>
        {titles.map((text, i) => (
          <TabHeader key={`${i}-${text}`} active={selectedTab === i} onClick={() => setSelectedTab(i)}>
            <SubTitle1>{text}</SubTitle1>
          </TabHeader>
        ))}
      </TabHeaderContainer>
      {content[selectedTab]}
    </TabContainer>
  );
};


const Tab = ({ text }) => {
  return (<></>);
};


export { Tabs, Tab };
