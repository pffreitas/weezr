import React from 'react';
import Slider from './slider';

const BannerSlider = ({ children }) => {
  const sliderProps = {
    infinite: true,
    dots: true,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 7000,
    swipe: false,
    draggable: false,
  };

  return (
    <Slider {...sliderProps} >
      {children}
    </Slider>
  );
};

export default BannerSlider;
