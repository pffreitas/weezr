const Padding = {
  L: '20px',
  M: '10px',
  S: '5px',
};

const Margin = {
  L: '40px',
  M: '20px',
  S: '10px',
};

const Size = {
  L: '38px',
  M: '28px',
  S: '18px',
}

export {
  Padding,
  Margin,
  Size,
};
