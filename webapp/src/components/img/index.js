import React from 'react';
import styled from 'styled-components';
import Color from '../color';

const ShowImage = styled.img(({ size }) => `
    width: ${size}px;
    height: ${size}px;
    display: block;
    overflow: hidden;
    border-radius: 4px;
    background-color: ${Color.BlueGrey100};
`);

export {
    ShowImage
}