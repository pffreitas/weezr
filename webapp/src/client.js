import App from './App';
const BrowserRouter = require('react-router-dom').BrowserRouter;
import React from 'react';
import { hydrate } from 'react-dom';
import * as serviceWorker from './serviceWorker';

const BrowserApp = () => {
  return (
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
};

hydrate(<BrowserApp />, document.getElementById('root'));

if (module.hot) {
  module.hot.accept();
}

serviceWorker.register()