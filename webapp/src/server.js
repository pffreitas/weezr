import App from './App';
import React from 'react';
import { matchPath, StaticRouter } from 'react-router-dom';
import express from 'express';
import { renderToString } from 'react-dom/server';
import { useStaticRendering } from 'mobx-react';
import routes from './Routes';
import compression from 'compression';
import { Helmet } from 'react-helmet';
import { ServerStyleSheet } from 'styled-components';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    return false;
  }

  return compression.filter(req, res);
}

const fetchInitialData = async (url) => {
  let initialData = {};
  try {
    const data = await Promise.all(routes.map(async (route, index) => {
      const match = matchPath(url, { path: route.path, exact: true });
      if (match) {
        let { initialData } = route.component;
        if (initialData) {
          return await initialData({ match });
        }
      }
    }));

    initialData = data.reduce((a, b) => ({ ...a, ...b }), {});
  } catch (e) {
    console.log('failed to load initial data: ', e);
  }

  return initialData;
};

const server = express();
server
  .disable('x-powered-by')
  .use(compression({ filter: shouldCompress }))
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', async (req, res) => {
    const sheet = new ServerStyleSheet();

    try {
      res.set('Cache-Control', 'max-age=600');
      useStaticRendering(true);

      const initialData = await fetchInitialData(req.url);

      const context = {};
      const markup = renderToString(
        sheet.collectStyles(
          <StaticRouter context={context} location={req.url}>
            <App initialData={initialData} />
          </StaticRouter>),
      );
      const helmet = Helmet.renderStatic();
      const styleTags = sheet.getStyleTags();

      if (context.url) {
        res.redirect(context.url);
      } else {
        res.status(200).send(
          `
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="manifest" href="/manifest.webmanifest">
    <link href="https://img.weezr.me" rel="preconnect" crossorigin>
    <link href="https://api.weezr.me" rel="preconnect" crossorigin>
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="theme-color" content="#000000" />
    <meta name="google-signin-client_id" content="241471743006-n1ecjdcibiiiah767a6cscgn5a7e8qfg.apps.googleusercontent.com"/>
    ${helmet.meta.toString()}

    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png" />

    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://kit.fontawesome.com/caabfe8361.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">

    <title>Weezr</title>
    ${helmet.title.toString()}
   
    ${styleTags}
    ${assets.client.css ? `<link rel="stylesheet" href="${assets.client.css}">` : ''}
    ${
          process.env.NODE_ENV === 'production'
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
          }
  </head>

  <body style="background-color: #f4f4f4">
    <noscript>
      You need to enable JavaScript to run this app.
    </noscript>
    <div id="root">${markup}</div>
    
  </body>
</html>
`,
        );
      }
    } catch (e) {
      console.log('Failed to server side render: ', e);
    } finally {
      sheet.seal();
    }
  });

export default server;
