import './weezr.css';
import './base.css';
import './css/wz-button.css';
import { Auth0Provider } from './components/auth/AuthProvider';

import { Route, Switch, useHistory } from 'react-router-dom';

import Layout from './components/Layout';
import React, { useRef } from 'react';
import routes from './Routes';

import ReactGA from 'react-ga';
import MobilePlayer from './components/player/mobile.player';
import Audio from './components/player/audio';
import Player from './components/player/desktop.player';
import SaveToPlaylistModal from './components/playlist/save-to-playlist.modal';
import ShareModal from './components/share/share.modal';

ReactGA.initialize('UA-158093371-1');

const App = ({ initialData }) => {

  const history = useHistory();
  history.listen((location, action) => {
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname);
  });

  const audioRef = useRef(null);

  return (
    <div>
      <Auth0Provider
        domain="weezr.us.auth0.com"
        client_id="nYENx95VT7e1jmzAwue535uiLzN1BTc0"
        redirect_uri={process.env.PUBLIC_URL}
        audience="https://api.weezr.me"
      >
        <Audio audioRef={audioRef} />
        <MobilePlayer audioRef={audioRef} />
        <Player audioRef={audioRef} />
        <SaveToPlaylistModal />
        <ShareModal />
        <Switch>
          {routes.map(({ path, exact, component: Comp }) => (
            <Route
              key={path}
              path={path}
              exact={exact}
              render={props => (
                <Layout {...props}>
                  <Comp {...{
                    ...props,
                    initialData,
                  }} />
                </Layout>
              )}
            />
          ))}
        </Switch>
      </Auth0Provider>
    </div>
  );
};

App.displayName = 'App';
export default App;
