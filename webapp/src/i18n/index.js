import _get from 'lodash/get';
import en_US from './en_US';

const languages = {
  'en-US': en_US,
};

const currentLanguage = languages[navigator.language];

function get(code) {
  return _get(currentLanguage, code);
}

const i18n = {
  get: get,
  ADD_SHOW: {
    IMAGE_UPLOAD_FAILED: get('ADD_SHOW.IMAGE_UPLOAD_FAILED'),
    IMAGE_TOO_SMALL: get('ADD_SHOW.IMAGE_TOO_SMALL'),
  },
};

export default i18n;
