const en_US = {
  last_added: 'Last Added',
  ADD_SHOW: {
    IMAGE_UPLOAD_FAILED: 'Image upload failed',
    IMAGE_TOO_SMALL: 'Image is too small',
  },
};

export default en_US;
