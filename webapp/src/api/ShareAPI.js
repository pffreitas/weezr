import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class ShareAPI {
  share(slug) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/share/${slug}`,
      method: 'POST',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new ShareAPI();
