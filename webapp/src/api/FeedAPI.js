import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class FeedAPI {
  list(limit, offset) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/feed?limit=${limit}&offset=${offset}&language=pt`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  getBanner() {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/feed/banner`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  getLatestFromFollowingShows() {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/feed/latest-from-following`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  continueToListen() {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/feed/continue-to-listen`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }


}

export default new FeedAPI();
