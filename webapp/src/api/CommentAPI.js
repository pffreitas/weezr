import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class CommentAPI {
  addComment(episodeId, comment) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/comment`,
      method: 'post',
      headers: { Authorization: `Bearer ${jwt}` },
      data: {
        EpisodeID: episodeId,
        Text: comment,
      },
    });
  }

  addCommentResponse(parentId, comment) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/comment/${parentId}/response`,
      method: 'post',
      headers: { Authorization: `Bearer ${jwt}` },
      data: {
        Text: comment,
      },
    });
  }

  loadComments(episode, limit, offset) {
    if (episode) {
      const jwt = AuthStore.getJwt();
      return axios({
        url: `${process.env.RAZZLE_WEEZR_API_URL}/comment?episodeId=${episode.id}&limit=${limit}&offset=${offset}`,
        headers: { Authorization: `Bearer ${jwt}` },
      });
    }
  }

  loadCommentResponses(parentId, limit, offset) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/comment/${parentId}/response?limit=${limit}&offset=${offset}`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new CommentAPI();
