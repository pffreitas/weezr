import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class SearchAPI {
  open(showSlug) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/show/${showSlug}`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  getShowEpisodes(show, limit, offset) {
    if (show) {
      const jwt = AuthStore.getJwt();
      return axios({
        url: `${process.env.RAZZLE_WEEZR_API_URL}/show/${show.slug}/episodes`,
        headers: { Authorization: `Bearer ${jwt}` },
        params: { limit, offset },
      });
    }
  }

  following() {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/following/shows`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new SearchAPI();
