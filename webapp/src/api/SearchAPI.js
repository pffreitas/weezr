import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class SearchAPI {
  search(queryText, limit, offset) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/search`,
      params: { queryText, limit, offset },
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  searchShows(queryText) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/search/show`,
      params: { queryText },
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new SearchAPI();
