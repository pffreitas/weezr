import AuthStore from '../stores/AuthStore';
import axios from 'axios';

class ListenAPI {
  open(cslug, sslug, eslug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/channel/${cslug}/show/${sslug}/episode/${eslug}`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  trackProgress(slug, progress, duration) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/play/${slug}/progress`,
      method: 'post',
      data: {
        Progress: progress,
        Finished: (duration - progress) < 180,
        Total: duration,
        Context: {
          agent: window.navigator.userAgent,
          language: window.navigator.language,
        },
      },
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  getProgress(slug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/play/${slug}/progress`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  getRecommendations(cslug, sslug, eslug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/channel/${cslug}/show/${sslug}/episode/${eslug}/recommendations`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  like(eslug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/e/${eslug}/like`,
      method: 'put',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  undoLike(eslug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/e/${eslug}/like`,
      method: 'delete',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  followShow(showSlug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/show/${showSlug}/follow`,
      method: 'post',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  unfollowShow(showSlug) {
    const jwt = AuthStore.getJwt();

    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/show/${showSlug}/follow`,
      method: 'delete',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new ListenAPI();
