import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class UserAPI {
  startSession(user) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/user/session`,
      method: 'POST',
      data: {
        ...user,
        familyName: user.family_name,
        givenName: user.given_name,
        username: user.sub,
      },
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new UserAPI();
