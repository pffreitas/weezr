import axios from 'axios';
import AuthStore from '../stores/AuthStore';

class PlaylistAPI {
  fetch(playlistId) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist/${playlistId}`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  getItems(playlistId, limit, offset) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist/${playlistId}/items?limit=${limit}&offset=${offset}`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  findAll() {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist`,
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  create(name) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist`,
      method: 'post',
      data: {
        title: name,
        type: 'user-defined',
      },
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  delete(playlistId) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist/${playlistId}`,
      method: 'delete',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  addItem(playlistId, itemId) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist/${playlistId}/item/${itemId}`,
      method: 'post',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }

  removeItem(playlistId, itemId) {
    const jwt = AuthStore.getJwt();
    return axios({
      url: `${process.env.RAZZLE_WEEZR_API_URL}/playlist/${playlistId}/item/${itemId}`,
      method: 'delete',
      headers: { Authorization: `Bearer ${jwt}` },
    });
  }
}

export default new PlaylistAPI();
