import HomeScreen from './pages/home/home.screen';
import ListenPage from './pages/Listen';
import { SearchResultsPage } from './pages/search/search.screen';
import PlaylistScreen from './pages/playlist/playlist.screen';
import ShowPage from './pages/show/ShowPage';
import { TrendingPage } from './pages/trending/TrendingPage';
import { LibraryPage } from './pages/library/LibraryPage';

const routes = [
  {
    path: '/',
    exact: true,
    component: HomeScreen,
  },
  {
    path: '/e/:eslug',
    exact: true,
    component: ListenPage,
  },
  {
    path: '/s/:showSlug',
    exact: true,
    component: ShowPage,
  },
  {
    path: '/search',
    exact: true,
    component: SearchResultsPage,
  },
  {
    path: '/playlist/:playlistId',
    exact: true,
    component: PlaylistScreen,
  },
  {
    path: '/trending',
    exact: true,
    component: TrendingPage,
  },
  {
    path: '/library',
    exact: true,
    component: LibraryPage,
  },
];
export default routes;
