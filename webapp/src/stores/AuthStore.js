import { action, observable } from 'mobx';
import { KJUR } from 'jsrsasign';
import PlaylistStore from './PlaylistStore';
import ShowStore from './ShowStore';
import UserAPI from '../api/UserAPI';
import ReactGA from 'react-ga';

class AuthStore {
  user = observable({});
  jwt = null;

  constructor() {
    this.createAnonymousJwt();
  }

  setUser = action(user => {
    this.user = user;
    if (user != null) {
      ReactGA.set({
        userId: user.sub,
        email: user.email,
        nickname: user.nickname,
      });

      UserAPI.startSession(user);
      PlaylistStore.loadPlaylists();
      ShowStore.fetchFollowing();
    }
  });

  setJwt(jwt) {
    this.jwt = jwt;
  }

  getFingerprint() {
    // const nav = window && window.navigator;
    // const screen = window && window.screen;
    // let guid = nav.mimeTypes.length;
    // guid += nav.userAgent.replace(/\D+/g, '');
    // guid += nav.plugins.length;
    // guid += screen.height || '';
    // guid += screen.width || '';
    // guid += screen.pixelDepth || '';

    return Math.random();
  }

  getJwt() {
    return this.jwt;
  }

  getUser() {
    return this.user;
  }

  createAnonymousJwt() {
    this.setJwt(this.getAnonymousJwt());
  }

  getAnonymousJwt() {
    var oHeader = { alg: 'HS256', typ: 'JWT' };
    var oPayload = {};
    var tNow = KJUR.jws.IntDate.get('now');
    var tEnd = KJUR.jws.IntDate.get('now + 1day');
    oPayload.iss = 'weezr.me';
    oPayload.sub = this.getFingerprint();
    oPayload.nbf = tNow;
    oPayload.iat = tNow;
    oPayload.exp = tEnd;
    oPayload.jti = 'id123456';
    oPayload.aud = 'https://api.weezr.me';

    var sHeader = JSON.stringify(oHeader);
    var sPayload = JSON.stringify(oPayload);

    return KJUR.jws.JWS.sign(
      'HS256',
      sHeader,
      sPayload,
      'NU6uvIl1c1YTEodfsXkve6T4tOaTzlcP',
    );
  }
}

export default new AuthStore();
