import { action, observable } from 'mobx';
import ListenAPI from '../api/ListenAPI';
import _assign from 'lodash/assign';
import { toast } from 'react-toastify';

class PlayerStore {
  initialTime = observable.box(0);
  isOpen = observable.box(false);
  episode = observable({});

  setCurrentTime = action(currentTime => {
    this.initialTime.set(parseInt(currentTime));
  });

  openAndPlay = action(slug => {
    this.isOpen.set(true);
    ListenAPI.open('cslug', 'sslug', slug)
      .then(({ data: episode }) => this.play(episode))
      .catch(err => {
        console.log(err);
        toast.error('Failed to play episode', {
          position: toast.POSITION.TOP_CENTER,
        });
      })
  })

  play = action(episode => {
    this.isOpen.set(true);
    this.episode = _assign(this.episode, episode);

    ListenAPI.getProgress(this.episode.slug)
      .then(res => {
        this.setCurrentTime(res.data.progress);
      })
      .catch(err => {
        this.setCurrentTime(0);
      });
  });
}

export default new PlayerStore();
