import { action, observable } from 'mobx';
import PlaylistAPI from '../api/PlaylistAPI';
import { toast } from 'react-toastify';

class PlaylistStore {
  playlists = observable.array([]);
  playlistName = observable.box('');
  playlist = observable.map({});
  loading = observable.box(false);
  isModalOpen = observable.box(false);
  episodeId = observable.box('');

  toggleModal = action((episodeId) => {
    this.isModalOpen.set(!this.isModalOpen.get());
    this.episodeId.set(episodeId);
  })

  setPlaylistName = action(playlistName => {
    this.playlistName.set(playlistName);
  });

  loadPlaylists = action(() => {
    PlaylistAPI.findAll()
      .then(res => {
        this.playlists.replace(res.data || []);
      })
      .catch(err => {
        this.playlists.replace([]);
        toast.error('Failed to playlists', {
          position: toast.POSITION.TOP_CENTER,
        });
      });
  });

  addItem = action((playlistId) => {
    PlaylistAPI.addItem(playlistId, this.episodeId.get())
      .then(res => {
        toast.success('Episode added to playlist', {
          position: toast.POSITION.TOP_CENTER,
        });
      })
      .catch(err => {
        console.log(err);
        toast.error('Failed to add to playlist', {
          position: toast.POSITION.TOP_CENTER,
        });
      }).finally(() => {
        this.episodeId.set('');
        this.isModalOpen.set(false);
      });
  });

  createAndAdd = action(() => {
    PlaylistAPI.create(this.playlistName)
      .then(res => {
        PlaylistAPI.addItem(res.data.id, this.episodeId.get());
        this.loadPlaylists();
      })
      .catch(err => {
        toast.error('Failed to create playlist', {
          position: toast.POSITION.TOP_CENTER,
        });
      })
      .finally(() => {
        this.playlistName.set('');
        this.episodeId.set('');
        this.isModalOpen.set(false);
      });
  });
}

export default new PlaylistStore();
