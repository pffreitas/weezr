import { action, observable, runInAction, computed } from 'mobx';
import CommentAPI from '../api/CommentAPI';
import { toast } from 'react-toastify';

class EpisodeCommentStore {
    comments = observable({});

    loadComments = action((episodeId) => {
        CommentAPI.loadComments(episodeId)
            .then(res => {
                this.comments = res.data;
            })
            .catch(err => {
                console.log(err);
                toast.error('Failed to load feed', {
                    position: toast.POSITION.TOP_CENTER,
                });
            });
    });
}

export default new EpisodeCommentStore();
