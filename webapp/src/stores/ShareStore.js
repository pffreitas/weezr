import { action, observable } from 'mobx';
import ShareAPI from '../api/ShareAPI';

class ShareStore {
    slug = observable.box('');
    isModalOpen = observable.box(false);

    constructor() {
    }

    toggleModal = action((slug) => {
        this.isModalOpen.set(!this.isModalOpen.get());
        this.slug.set(slug);
    })

    share = action(() => {
        ShareAPI.share(this.slug.get()).finally(() => {
            this.isModalOpen.set(false);
            this.slug.set('');
        });
    })
}

export default new ShareStore();
