import { action, observable } from 'mobx';

import { toast } from 'react-toastify';
import ShowAPI from '../api/ShowAPI';

class ShowStore {
  following = observable.array([]);

  fetchFollowing = () => {
    ShowAPI.following()
      .then(
        action(res => {
          this.following.replace(res.data || []);
        }),
      )
      .catch(err => {
        console.log(err);
        toast.error('Failed to load feed', {
          position: toast.POSITION.TOP_CENTER,
        });
      });
  };
}

export default new ShowStore();
