import { action, observable } from 'mobx';

import ListenAPI from '../api/ListenAPI';
import { toast } from 'react-toastify';
import ShowStore from './ShowStore';

class ListenStore {
  openedEpisode = observable.box('');
  episode = observable({});
  recommendations = observable.array([]);
  following = observable.box(false);
  followingCount = observable.box(0);
  isLoading = observable.box(false);


  followShow = showSlug => {
    if (this.following.get()) {
      ListenAPI.unfollowShow(showSlug)
        .then(
          action(res => {
            this.following.set(false);
            this.followingCount.set(this.followingCount.get() - 1);
            ShowStore.fetchFollowing();
          }),
        )
        .catch(err => {
          console.log(err);
          toast.error('Failed to like episode', {
            position: toast.POSITION.TOP_CENTER,
          });
        });
    } else {
      ListenAPI.followShow(showSlug)
        .then(
          action(res => {
            this.following.set(true);
            this.followingCount.set(this.followingCount.get() + 1);
            ShowStore.fetchFollowing();
          }),
        )
        .catch(err => {
          console.log(err);
          toast.error('Failed to like episode', {
            position: toast.POSITION.TOP_CENTER,
          });
        });
    }
  };
}

export default new ListenStore();
