import React, { useEffect, useState } from 'react';
import { EpisodeCard } from '../../components/episode/EpisodeCard';
import { Loading } from '../../components/loading/Loading';
import { FollowButton } from '../../components/follow/Follow';
import { useFetch, usePaginatedFetch } from '../../components/fetcher/useFetch';
import ShowAPI from '../../api/ShowAPI';
import { Helmet } from 'react-helmet';
import { PaddedContent } from '../../components/base/content';
import styled from 'styled-components';


const ShowContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  
  @media (min-width: 768px){
    grid-template-columns: 240px 1fr;
    grid-template-rows: 60px 260px;
    grid-column-gap: 40px;
   
    max-width: 740px;
    margin-left: calc(50% - 740px / 2);
  }
`;

const ShowImageContainer = styled.div`
  padding-bottom: 20px;
  max-width: 240px;
  margin-left: calc(50% - 120px);
  
  @media (min-width: 768px){
    grid-row: 1 /  2;
    grid-column: 1;
  }
`;

const ShowImage = styled.img`
  border-radius: 4px;
  width: 240px;
  height: 240px;
`;


const FollowingContainer = styled.div`
  display: flex;
`;

const FollowingCount = styled.span`
  line-height: 36px;
  display: inline-block;
  flex: 1;
`;

const ShowTitle = styled.h1`
  grid-row: 2;
  
  @media (min-width: 768px){
    grid-row: 1;
    grid-column: 2;
  }
`;

const ShowDescription = styled.p`
  grid-row: 3;
`;

const EpisodeContainer = styled.div`
  padding-top: 25px;
  
  @media (min-width: 768px){
    grid-row: 2 / 4;
    grid-column: 2;
  }
`;

const ShowPage = ({ match, initialData }) => {

  const { showSlug } = match.params;

  const [show, showLoading] = useFetch(initialData && initialData.show, () => ShowAPI.open(showSlug), [match.params]);
  const [episodes, loadingEpisodes, loadMoreEpisodes] = usePaginatedFetch(initialData && initialData.episodes, 50, (limit, offset) => ShowAPI.getShowEpisodes(show, limit, offset), [show]);

  const [followingCount, setFollowingCount] = useState((show && show.following_count) || 0);
  const [following, setFollowing] = useState(show && show.followedByCurrentUser || false);

  useEffect(() => {
    if (show) {
      setFollowing(show.followedByCurrentUser);
      setFollowingCount(show.following_count);
    }
  }, [show]);

  console.log(episodes);

  return (
    <PaddedContent>
      {show && (
        <Helmet>
          <title>Weezr - {show.name}</title>
          <meta property="og:url" content={`http://weezr.me/s/${showSlug}`} />
          <meta property="og:type" content="website" />
          <meta property='og:title' content={(show.name || '').substring(0, 60)} />
          <meta property="og:description" content={(show.description || '').substring(0, 155)} />
          <meta property='og:image' content={`https://img.weezr.me/${show.image_key}-240.png`} />
          <meta name='twitter:card' content='summary' />
          <meta name='twitter:site' content={`http://weezr.me/s/${showSlug}`} />
          <meta name='twitter:title' content={(show.name || '').substring(0, 60)} />
          <meta name='twitter:description' content={(show.description || '').substring(0, 155)} />
          <meta name='twitter:image' content={`https://img.weezr.me/${show.image_key}-240.png`} />
        </Helmet>
      )}
      <Loading loading={showLoading} />
      {show && (
        <ShowContainer>
          <ShowTitle>{show.name}</ShowTitle>
          <ShowImageContainer>
            <ShowImage src={`https://img.weezr.me/${show.image_key}-240.png`} />
            <FollowingContainer>
              <FollowingCount>{followingCount} following</FollowingCount>
              <FollowButton slug={show.slug}
                following={following}
                onFollow={() => {
                  setFollowing(true);
                  setFollowingCount(followingCount + 1);
                }}
                onUnfollow={() => {
                  setFollowing(false);
                  setFollowingCount(followingCount - 1);
                }}
              />
            </FollowingContainer>
          </ShowImageContainer>
          <ShowDescription>{show.description}</ShowDescription>

          <EpisodeContainer>
            <ul className="wz-episode-list">
              {episodes && episodes.map(episode => {
                return (
                  episode && (
                    <li key={`${episode.id}-${episode.slug}`}>
                      <EpisodeCard item={episode} className="transparent" />
                    </li>
                  )
                );
              })}
            </ul>
            {!loadingEpisodes &&
              <a className="sv-button link link-info sv-ph--0" onClick={() => loadMoreEpisodes()}>Load more</a>}
            <Loading loading={loadingEpisodes} />
          </EpisodeContainer>
        </ShowContainer>
      )}
    </PaddedContent>
  );
};

ShowPage.initialData = async ({ match }) => {
  const { showSlug } = match.params;

  const show = (await ShowAPI.open(showSlug)).data;
  const episodes = (await ShowAPI.getShowEpisodes(show, 50, 0)).data;

  return {
    show,
    episodes,
  };
};

export default ShowPage;
