import React from 'react';
import { PaddedContent } from '../../components/base/content';
import { MenuSection, MenuSectionItem } from '../../components/Layout';
import PlaylistStore from '../../stores/PlaylistStore';
import ShowStore from '../../stores/ShowStore';
import { Link } from 'react-router-dom';

export const LibraryPage = () => {
  return (
    <PaddedContent>
      <div>
        <MenuSection title="Playlists">
          <MenuSectionItem
            label="Listen Later"
            img="fa-clock-o"
            link="/playlist/listen-later"
          />
          <MenuSectionItem
            label="Liked"
            img="fa-heart-o"
            link="/playlist/liked"
          />
          <MenuSectionItem
            label="History"
            img="fa-history"
            link="/playlist/history"
          />
          {PlaylistStore.playlists
            .filter(playlist => 'user-defined' === playlist.type)
            .map(playlist => {
              return (
                <MenuSectionItem
                  key={playlist.id}
                  label={playlist.title}
                  link={`/playlist/${playlist.id}`}
                />
              );
            })}
        </MenuSection>
      </div>
      <div>
        <MenuSection title="Following">
          {ShowStore.following.map(f => {
            return (
              <li key={f.id}>
                <Link to={`/s/${f.slug}`}>{f.name}</Link>
              </li>
            );
          })}
        </MenuSection>
      </div>
    </PaddedContent>
  );
};
