import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { observer } from 'mobx-react';
import { toast } from 'react-toastify';
import { AuthState, useAuth0 } from '../../components/auth/AuthProvider';
import { useFetch, useInfiniteFetch } from '../../components/fetcher/useFetch';
import PlaylistAPI from '../../api/PlaylistAPI';
import { Loading } from '../../components/loading/Loading';
import { PaddedContent } from '../../components/base/content';
import PlaylistStore from '../../stores/PlaylistStore';
import {
  Description,
  EpisodeCollectionContainer,
  EpisodeContainer,
  EpisodeScroll,
  FollowingContainer,
  Image,
  ImageContainer,
  MosaicImage,
  Title,
  TitleContainer,
} from '../../components/layout/episode-collection';
import { MainEpisodeCard } from '../../components/episode/MainEpisodeCard';
import { RemoveEpisodeFromPlaylistButton } from '../../components/episode/episode-card-buttons';

const PlaylistScreen = observer(({ match }) => {
  const { playlistId } = match.params;

  const history = useHistory();
  const { authState } = useAuth0();
  const [removeItemLoading, setRemoveItemLoading] = useState('');

  const [playlist, playlistLoading, setPlaylist] = useFetch(null, () => {
    if (AuthState.AUTHENTICATED === authState) {
      return PlaylistAPI.fetch(playlistId);
    }
  }, [match.params, authState]);

  const [playlistItems, loadMore, hasMore, setPlaylistItems] = useInfiniteFetch([], 20, async (limit, offset) => {
    if (playlist != null && playlist.id != null) {
      const { data } = await PlaylistAPI.getItems(playlist.id, limit, offset);
      return data;
    }
  }, [playlist]);

  const removeItem = (playlistId, itemId) => {
    setRemoveItemLoading(itemId);

    PlaylistAPI.removeItem(playlistId, itemId)
      .then(() => setPlaylistItems(playlistItems.filter(({ id }) => id !== itemId)))
      .catch(err => {
        console.log(err);
        toast.error('Failed to remove item from playlist', {
          position: toast.POSITION.TOP_CENTER,
        });
      })
      .finally(() => setRemoveItemLoading(''));
  };

  const deletePlaylist = () => {
    PlaylistAPI.delete(playlistId)
      .then(() => {
        PlaylistStore.loadPlaylists();
        history.push('/');
      })
      .catch(err => {
        console.log(err);
        toast.error('Failed to delete playlist', {
          position: toast.POSITION.TOP_CENTER,
        });
      });
  };

  if (authState === AuthState.ANONYMOUS) return <div>LOGIN</div>;

  let image = null;
  if (playlistItems != null && playlistItems.length > 0) {
    if (playlistItems.length >= 4) {
      image = (
        <MosaicImage>
          {playlistItems.slice(0, 4).map(item => <img key={`mosaic-image-${item.image_key}`}
            src={`https://img.weezr.me/${item.image_key}-240.png`} />)}
        </MosaicImage>
      );
    } else {
      image = <Image src={`https://img.weezr.me/${playlistItems[0].image_key}-240.png`} />;
    }
  }

  return (
    <PaddedContent>
      <Loading loading={playlistLoading} />
      {playlist && (
        <EpisodeCollectionContainer>
          <TitleContainer>
            <Title>{playlist.title || playlist.code}</Title>
            <RemoveEpisodeFromPlaylistButton strong onClick={deletePlaylist} />
          </TitleContainer>
          <ImageContainer>
            {image}
            <FollowingContainer>
            </FollowingContainer>
          </ImageContainer>
          <Description />
          <EpisodeContainer>
            {playlistItems.length > 0 && (
              <EpisodeScroll pageStart={0} loadMore={() => loadMore()} hasMore={hasMore}>
                {playlistItems.map(item => (
                  <MainEpisodeCard key={`playlist-episode-${item.id}`} item={item}
                    disabled={removeItemLoading === item.id}>
                    <MainEpisodeCard.Tools>
                      <RemoveEpisodeFromPlaylistButton onClick={() => {
                        removeItem(playlist.id, item.id);
                      }} />
                    </MainEpisodeCard.Tools>
                  </MainEpisodeCard>
                ))}
              </EpisodeScroll>
            )}
            <Loading loading={hasMore} />
          </EpisodeContainer>
        </EpisodeCollectionContainer>
      )}
    </PaddedContent>
  );
});

export default PlaylistScreen;
