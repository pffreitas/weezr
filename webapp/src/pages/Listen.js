import PlayerStore from '../stores/PlayerStore';
import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { observer } from 'mobx-react';
import numeral from 'numeral';
import ListenAPI from '../api/ListenAPI';
import CommentAPI from '../api/CommentAPI';
import '../css/comment.css';
import { CommentInput, Comment } from '../components/comment';
import { useFetch, useInfiniteFetch } from '../components/fetcher/useFetch';
import { Loading } from '../components/loading/Loading';
import throttle from 'lodash/throttle';
import { FollowButton } from '../components/follow/Follow';
import { Helmet } from 'react-helmet';
import { EpisodeCard } from '../components/episode/EpisodeCard';
import { PaddedContent } from '../components/base/content';
import styled from 'styled-components';
import { PlayButton, ShareButton, SaveButton, LikeButton } from '../components/episode/episode-card-buttons';
import { Title4 } from '../components/typography';

const EpisodeContainer = styled.div`
  display: grid;
  grid-gap: 30px;
  
  @media (min-width: 768px){
    grid-template-columns: 1fr 300px;
    grid-gap: 20px;
  }
`;

const EpisodeContentContainer = styled.div`
`;

const EpisodeSubTitle = styled.div`
  display: grid; 
  border-bottom: 1px solid #ccc;
  margin: 20px 0 40px 0;
  padding-bottom: 20px;
  grid-template-columns: 1fr;
  
  @media (min-width: 1280px){
    grid-template-columns: 1fr 1fr;
  }
`;

const PlayedTimes = styled.p`
  margin: 0;
  padding: 0;
  font-weight: 400;
  color: #aaa;
  line-height: 28px;
  font-size: 18px;
`;

const PublishedAt = styled.p`
  margin: 0;
  color: #666;
`;

const EpisodeToolsList = styled.div`
  margin-top: 15px;
  display: flex;
  justify-content: space-between;

  @media (min-width: 1280px){
    grid-column: 2;
    grid-row: 1 / 3;
    justify-content: flex-end;  
    margin: 0;
  }
`;

const EpisodeContent = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-template-columns: 50px 1fr auto;
`;

const EpisodeNotes = styled.div`
  grid-column: 1 / 4;
`;

const ShowTitle = styled.h2`
  padding: 0;
  margin: 0;
  line-height: 24px;
  
  & a {
    display: block;
    max-height: 24px;
    overflow: hidden;
  }
  
  & small {
    color: #666;
    font-weight: normal;
  }
`;

const RecommendationsContainer = styled.div`
  @media (min-width: 768px){
    grid-row: 1 / 3;
    grid-column: 2;
  }
`;

const CommentsContainer = styled.div`
  @media (min-width: 768px){
    grid-row: 2;
    grid-column: 1;
  }
`;

const CommentList = styled(InfiniteScroll)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`;

const ListenPage = observer(({ match, initialData }) => {
  const { cslug, sslug, eslug } = match.params;

  const [episode, episodeLoading] = useFetch(initialData && initialData.episode, () => ListenAPI.open(cslug, sslug, eslug), [match.params]);
  const [recommendations, recommendationsLoading] = useFetch(initialData && initialData.recommendations, () => ListenAPI.getRecommendations(cslug, sslug, eslug), [match.params]);
  const [following, setFollowing] = useState(false);
  const [followingCount, setFollowingCount] = useState(false);

  const [comments, loadMore, hasMore, setComments] = useInfiniteFetch(initialData && initialData.comments, 5, async (limit, offset) => {
    if (episode != null) {
      const { data } = await CommentAPI.loadComments(episode, limit, offset);
      return data;
    }
  }, [episode]);

  useEffect(() => {
    if (episode) {
      setFollowing(episode.followedByCurrentUser);
      setFollowingCount(episode.followingCount);
    }
  }, [episode]);

  if (episodeLoading) {
    return (
      <PaddedContent>
        <Loading loading={episodeLoading} />
      </PaddedContent>
    )
  }

  if (episode == null) {
    return null;
  }

  return (
    <PaddedContent>
      <Helmet>
        <title>Weezr - {episode.title.substring(0, 20)}</title>
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`http://weezr.me/e/${episode.slug}`} />
        <meta property='og:title' content={(episode.title || '').substring(0, 60)} />
        <meta property="og:description" content={(episode.tidyContent || '').substring(0, 155)} />
        <meta property='og:image' content={`https://img.weezr.me/${episode.image_key}-240.png`} />
        <meta property='og:audio' content={episode.audio_file_url} />
        <meta name='twitter:card' content='summary' />
        <meta name='twitter:site' content={`http://weezr.me/e/${episode.slug}`} />
        <meta name='twitter:title' content={(episode.title || '').substring(0, 60)} />
        <meta name='twitter:description' content={(episode.tidyContent || '').substring(0, 155)} />
        <meta name='twitter:image' content={`https://img.weezr.me/${episode.image_key}-240.png`} />
      </Helmet>
      <EpisodeContainer>
        <EpisodeContentContainer>
          <Title4>{episode.title}</Title4>
          <EpisodeSubTitle>
            <PlayedTimes>Played {episode.playbackCount} times</PlayedTimes>
            <PublishedAt>Published {moment(episode.published_at).format('MMMM Do, YYYY')}</PublishedAt>
            <EpisodeToolsList>
              <PlayButton strong slug={episode.slug} />
              <LikeButton strong slug={episode.slug} likedByCurrentUser={episode.likeByCurrentUser} likeCount={episode.likeCount} />
              <ShareButton strong slug={episode.slug} />
              <SaveButton strong episodeId={episode.id} />
            </EpisodeToolsList>
          </EpisodeSubTitle>
          <EpisodeContent>
            <img height="48px" width="48px" src={`https://img.weezr.me/${episode.image_key}-60.png`} />
            <ShowTitle>
              <Link to={`/s/${episode.showSlug}`}>{episode.showTitle}</Link>
              <small>{numeral(followingCount).format('0 a')} following</small>
            </ShowTitle>
            <FollowButton slug={episode.showSlug}
              following={following}
              onFollow={() => {
                setFollowing(true);
                setFollowingCount(followingCount + 1);
              }}
              onUnfollow={() => {
                setFollowing(false);
                setFollowingCount(followingCount - 1);
              }}
            />
            <EpisodeNotes dangerouslySetInnerHTML={{ __html: episode.content || episode.description }} />
          </EpisodeContent>
        </EpisodeContentContainer>
        <RecommendationsContainer>
          <Loading loading={recommendationsLoading} />
          <ul className="wz-episode-list">
            {recommendations && recommendations.map(item => (
              <li><EpisodeCard item={item} className="transparent" /></li>))}
          </ul>
        </RecommendationsContainer>
        <CommentsContainer>
          <CommentInput episodeId={episode.id} onConfirm={(comment) => {
            setComments([comment, ...comments]);
          }} placeholder="Add a comment..." />

          <CommentList pageStart={0} loadMore={() => loadMore()} hasMore={hasMore}>
            {comments.map(c => <Comment key={`comment-${c.ID}`} comment={c} responseEnabled={true} />)}
          </CommentList>
          <Loading loading={hasMore} />
        </CommentsContainer>
      </EpisodeContainer>
    </PaddedContent>
  );
});

ListenPage.initialData = async ({ match }) => {
  const { cslug, sslug, eslug } = match.params;

  const episode = (await ListenAPI.open(cslug, sslug, eslug)).data;
  const recommendations = (await ListenAPI.getRecommendations(cslug, sslug, eslug)).data;
  const comments = (await CommentAPI.loadComments(episode, 20, 0)).data;

  return {
    episode,
    recommendations,
    comments,
  };
};

export default ListenPage;
