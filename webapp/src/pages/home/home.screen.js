import React from 'react';
import FeedAPI from '../../api/FeedAPI';
import TopBarPanel from './panels/top-bar.panel/top-bar.panel';
import { Body } from '../../components/layout/layout';
import { FeedPanel } from './panels/feed.panel/feed.panel';


const HomeScreen = ({ initialData }) => {
  return (
    <>
      <TopBarPanel/>
      <Body>
        <FeedPanel feed={initialData && initialData.feed}/>
      </Body>
    </>
  );
};

HomeScreen.initialData = async () => {
  const feed = (await FeedAPI.list(50, 0)).data;

  return {
    feed,
  };
};

export default HomeScreen;
