import React from 'react';
import styled from 'styled-components';
import { useFetch } from '../../../../components/fetcher/useFetch';
import FeedAPI from '../../../../api/FeedAPI';
import BannerSlider from '../../../../components/slider/banner-slider';
import EpisodeSlider from '../../../../components/slider/episode-slider';
import { SimpleEpisodeCard } from '../../../../components/episode/EpisodeCard';
import './top-bar.css';

const TopBar = styled.div`
  width: 100%;
  height: 455px
  
  @media (min-width: 1210px) {
    height: 390px;
  }
`;

const Container = styled.div`
  height: 455px;
  background-size: 100% 300px;
  filter: grayscale(0.45);
  
  @media (min-width: 1210px) {
    height: 390px;
  }
`;


const Title = styled.h1`
  font-size: 3.25em;
  margin-bottom: 0;
  color: #2e3331;
  padding-left: 35px;
  
  & small {
    display: block;
    width: 100%;
    font-size: 12px;
    color: #525252;
    margin-bottom: 3px;
    text-transform: uppercase;  
  }
`;

const Content = styled.div`  
  position: absolute;
  top: 0;
  width: 100%;
  height: 275px;
  padding-top: 95px;
  display: grid;
  grid-template-columns: 100%;
  
  @media (min-width: 1024px) {
    grid-template-columns: 100%;
  }
  
  @media (min-width: 1210px) {
    grid-template-columns: 300px 35px calc(100% - 370px) 35px;
  }
`;

const Shadow = styled.div`
  display: none;
  @media (min-width: 1210px) {
    display: block;
    width: 100%;
    height: 100%;
    z-index: 1;
    box-shadow: ${props => props.direction && props.direction === 'left' ? '14px 0px 14px -16px rgba(0,0,0,0.75);' : '-14px 0px 14px -16px rgba(0,0,0,0.75);'};
  }
`;

const Clip = styled.svg`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 50px;
  
`;

const TopBarPanel = () => {
  const [banners, loading] = useFetch([{
    object_id: 1, object_type: '', episode_list: { name: '', episodes: [] }
  }], () => FeedAPI.getBanner(), []);

  return (
    <TopBar>
      <BannerSlider>
        {banners.map((banner, i) => {
          const { object_id, object_type, episode_list } = banner;
          const { name, episodes } = episode_list;

          return (
            <div key={`banner-${object_id}`}>
              <Container className={`bg-${i}`}>
                <Clip xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                  <polygon className="svg--sm" fill="#F4F4F4" points="0,0 30,100 65,21 90,100 100,75 100,100 0,100" />
                  <polygon className="svg--lg" fill="#F4F4F4"
                    points="0,0 15,100 33,21 45,100 50,75 55,100 72,20 85,100 95,50 100,80 100,100 0,100" />
                </Clip>
              </Container>

              <Content className={`content-${i}`}>
                {episodes && episodes.length > 0 && (
                  <>
                    <Title><small>Featured {object_type}</small>{name}</Title>
                    <Shadow direction="left" />
                    <EpisodeSlider style={{ zIndex: 200 }}>
                      {episodes.map((episode, i) => <SimpleEpisodeCard key={i} item={episode} />)}
                    </EpisodeSlider>
                    <Shadow direction="right" />
                  </>
                )}
              </Content>
            </div>
          );
        })}
      </BannerSlider>
    </TopBar>
  );
};

export default TopBarPanel;
