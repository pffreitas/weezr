import React from 'react';
import styled from 'styled-components';
import InfiniteScroll from 'react-infinite-scroller';
import FeedAPI from '../../../../api/FeedAPI';
import { MainEpisodeCard } from '../../../../components/episode/MainEpisodeCard';
import { useFetch, useInfiniteFetch } from '../../../../components/fetcher/useFetch';
import { AuthState, useAuth0 } from '../../../../components/auth/AuthProvider';
import { Loading } from '../../../../components/loading/Loading';
import FeedPromotionSection from './sections/feed-promotion.section';
import ForYouSection from './sections/for-you.section';

const Container = styled.div`
  margin-top: 45px;
`;

const FeedItems = styled(InfiniteScroll)`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
  grid-gap: 20px;
  margin-bottom: 40px;
`;


export const FeedPanel = ({ initialFeed }) => {
  const { authState } = useAuth0();

  const [latestFromFollowing] = useFetch([], () => {
    if (authState === AuthState.AUTHENTICATED)
      return FeedAPI.getLatestFromFollowingShows();
  }, [authState]);

  const [continueToListen] = useFetch([], () => {
    if (authState === AuthState.AUTHENTICATED)
      return FeedAPI.continueToListen();
  }, [authState]);

  const [feed, loadMore, hasMore] = useInfiniteFetch(initialFeed, 50, async (limit, offset) => {
    if (authState !== AuthState.UNAUTHENTICATED) {
      const { data } = await FeedAPI.list(limit, offset);
      return [...data.episodes.slice(0, 10), data.promotions[0], ...data.episodes.slice(10, 40), data.promotions[1], ...data.episodes.slice(40, 50)];
    }
  }, [authState]);

  if (!feed) {
    return null;
  }

  const feedItems = feed.map((feedItem, i) => {
    if (!feedItem) {
      return null;
    }

    if (feedItem.episode_list != null) {
      return <FeedPromotionSection key={i} promotion={feedItem} />;
    }

    return <MainEpisodeCard key={i} item={feedItem} />;
  });



  return (
    <Container>
      <ForYouSection latestFromFollowing={latestFromFollowing} continueToListen={continueToListen} />
      <FeedItems pageStart={0} loadMore={() => loadMore()} hasMore={hasMore}>
        {feedItems}
      </FeedItems>
      <Loading loading={hasMore} />
    </Container>
  );
};
