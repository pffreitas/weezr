import React from 'react';
import styled from 'styled-components';
import { Title6 } from '../../../../../components/typography';
import { Tab, Tabs } from '../../../../../components/tabs';
import { MainEpisodeCard } from '../../../../../components/episode/MainEpisodeCard';
import HorizontalScroll from '../../../../../components/episode/horizontal-scroll';
import { Spacing } from '../../../../../components/style';

const UserSection = styled.div`
  display: grid;
  padding: ${Spacing.Padding.M};
  background-color: #fff;
  border: 1px solid #eee;
  margin: ${Spacing.Margin.L} 0;
  border-radius: 4px;
`;



const ForYouSection = ({ latestFromFollowing, continueToListen }) => {
  if (latestFromFollowing.length === 0 && continueToListen.length === 0) {
    return null;
  }

  return (
    <UserSection>
      <Title6>For You</Title6>
      <Tabs>
        <Tab text="New Episodes">
          <HorizontalScroll>
            {latestFromFollowing.map((e, i) => {
              return <MainEpisodeCard key={`for-you-new-episodes-${i}-${e.slug}`} item={e} />;
            })}
          </HorizontalScroll>
        </Tab>
        <Tab text="Continue to Listen">
          <HorizontalScroll>
            {continueToListen.map((e, i) => {
              return <MainEpisodeCard key={`continue-to-listen-${i}-${e.slug}`} item={e} />;
            })}
          </HorizontalScroll>
        </Tab>
      </Tabs>
    </UserSection>
  );
};

export default ForYouSection;
