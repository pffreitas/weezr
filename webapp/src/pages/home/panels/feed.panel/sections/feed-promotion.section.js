import React from 'react';
import styled from 'styled-components';
import { MainEpisodeCard } from '../../../../../components/episode/MainEpisodeCard';
import HorizontalScroll from '../../../../../components/episode/horizontal-scroll';
import Color from '../../../../../components/color';

const FeaturedContainer = styled.div`
  position: relative;
  display: grid;
  grid-gap: 20px;
  grid-template-rows: min-content;
  grid-template-columns: 100%;
  
  margin: 45px 0;
  background-color: #ffffff;
  border-radius: 10px;
  padding: 35px 0px;
  box-shadow: 0px 0px 36px 0px rgba(0, 0, 0, .095);
  
  grid-column: 1 / -1;
`;

const Content = styled.div`
  padding: 0 50px; 
  
  & h1 {
      margin-bottom: 0px;
  }
  
  &  h1 small {
      display: block;
      width: 100%;
      font-size: 12px;
      color: #888;
      margin-bottom: 3px;
  }
`;

const Title = styled.h1`
  color: ${Color.BlueGrey800};
`;

const Description = styled.p`
  color: ${Color.BlueGrey800};
`;

const Type = styled.small`
  text-transform: uppercase;
`;

const FeedPromotionSection = ({ promotion }) => {
  if (!promotion) {
    return null;
  }

  const { name, description, episodes } = promotion.episode_list;

  if (!episodes || episodes.length < 5) {
    return null;
  }

  return (
    <FeaturedContainer>
      <Content>
        <Title>
          <Type>{promotion.object_type}</Type>
          {name}
        </Title>
        <Description>{description}</Description>
      </Content>
      <div className="featured-items">
        <HorizontalScroll>
          {episodes.map((episode, i) => <MainEpisodeCard key={i} item={episode} showDesc={false} />)}
        </HorizontalScroll>
      </div>
    </FeaturedContainer>
  );
};


export default FeedPromotionSection;
