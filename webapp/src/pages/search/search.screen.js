import React from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react';
import InfiniteScroll from 'react-infinite-scroller';
import { Link, useHistory } from 'react-router-dom';
import { Loading } from '../../components/loading/Loading';
import { MainEpisodeCard } from '../../components/episode/MainEpisodeCard';
import { PaddedContent, CenteredContent } from '../../components/base/content';
import { useFetch, useInfiniteFetch } from '../../components/fetcher/useFetch';
import { useLocation } from 'react-router-dom';
import SearchAPI from '../../api/SearchAPI';
import { Title6 } from '../../components/typography';
import { ShowImage } from '../../components/img';

const SearchResultsContainer = styled(InfiniteScroll)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 18px;

  width: 100%;
  max-width: 480px;
`;

const ShowResultsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 18px;
  width: 100%;
  max-width: 480px;
`;

const Header = styled.div`
  text-align: 'left';
  width: 100%;
  margin-bottom: 18px;
  max-width: 480px;
`;

const ShowResultsScrollView = styled.div`
  display: flex;
  overflow-x: scroll;
`;

const ShowName = styled.div``;

const ShowResult = styled.div`
  width: 120px;
  min-width: 120px;
  margin-right: 28px;
  cursor: pointer;
`;

export const SearchResultsPage = observer(() => {
  const history = useHistory();
  const urlQueryString = new URLSearchParams(useLocation().search);
  const query = urlQueryString.get('q');

  const [shows, loadingShows] = useFetch([], () => SearchAPI.searchShows(query), [query]);

  const [searchResults, loadMore, hasMore] = useInfiniteFetch([], 50, async (limit, offset) => {
    const resp = await SearchAPI.search(query, limit, offset);
    return resp.data;
  }, [query]);

  if (searchResults == null) {
    return null;
  }

  const noResults = searchResults.length === 0 && !hasMore && shows && shows.length === 0 && !loadingShows;
  const hasResults = searchResults.length > 0 || shows.length > 0;

  return (
    <PaddedContent>
      <CenteredContent>
        <Header>
          {noResults && <Title6>No results for "{query}"</Title6>}
          {hasResults && <Title6>Results for "{query}"</Title6>}
        </Header>
        {shows.length > 0 && (
          <ShowResultsContainer>
            <Title6>Shows</Title6>
            <ShowResultsScrollView>
              {shows.map(show => (
                <ShowResult onClick={() => history.push(`/s/${show.slug}`)}>
                  <ShowImage size={120} src={`https://img.weezr.me/${show.image_key}-240.png`} />
                  <ShowName><Link to={`/s/${show.slug}`}>{show.name}</Link></ShowName>
                </ShowResult>
              ))}
            </ShowResultsScrollView>
          </ShowResultsContainer>
        )}
        <SearchResultsContainer pageStart={0} loadMore={() => loadMore()} hasMore={hasMore}>
          {searchResults.length > 0 && <Title6>Episodes</Title6>}
          {searchResults.map(item => <MainEpisodeCard key={`search-${item.slug}`} item={item} />)}
          <Loading loading={hasMore} />
        </SearchResultsContainer>
      </CenteredContent>
    </PaddedContent>
  );
});


