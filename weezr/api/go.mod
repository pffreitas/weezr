module weezr.me/weezr/api

go 1.13

require (
	github.com/adjust/rmq/v2 v2.0.0
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20201104130540-2e1f801663c6
	github.com/expectedsh/go-sonic v0.0.0-20200417165347-1cfe7c425fff
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gorilla/mux v1.7.4
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.7.1
	github.com/robbert229/jwt v2.0.0+incompatible
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.30.0
	weezr.me/weezr/business v0.0.0
	weezr.me/weezr/commons v0.0.0
	weezr.me/weezr/db v0.0.0
	weezr.me/weezr/episode v0.0.0
	weezr.me/weezr/feed v0.0.0
	weezr.me/weezr/feedclnt v0.0.0
	weezr.me/weezr/model v0.0.0
	weezr.me/weezr/scavenger v0.0.0
	weezr.me/weezr/search v0.0.0
	weezr.me/weezr/test v0.0.0
	weezr.me/weezr/winj v0.0.0
)

replace (
	github.com/adjust/rmq/v2 => github.com/pffreitas/rmq/v2 v2.0.0
	weezr.me/weezr/business => ../business
	weezr.me/weezr/commons => ../commons
	weezr.me/weezr/db => ../db
	weezr.me/weezr/episode => ../episode
	weezr.me/weezr/feed => ../feed/feed
	weezr.me/weezr/feedclnt => ../feed/feedclnt
	weezr.me/weezr/model => ../model
	weezr.me/weezr/scavenger => ../scavenger
	weezr.me/weezr/search => ../search
	weezr.me/weezr/test => ../test
	weezr.me/weezr/winj => ../winj
)
