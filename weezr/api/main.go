package main

import (
	"fmt"
	"log"
	"net/http"

	"weezr.me/weezr/api/application"
)

func main() {
	app := application.New()
	config := app.Config

	if config.ServerPort == "" {
		log.Fatal("$PORT must be set")
	}

	log.Printf("Running %s on %s", app.Name, config.ServerPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", config.ServerPort), app.Router))
}
