package endpoints_test

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"weezr.me/weezr/test"

	"github.com/icrowley/fake"

	"weezr.me/weezr/api/application"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
)

func TestEpisodeLike(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(episodeRepository model.EpisodeRepository, showRepository model.ShowRepository) int {
		assertions := assert.New(t)
		router := app.Router

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		assertions.Nil(err)

		referenceEpisode, err := test.InsertDefaultEpisode(episodeRepository, show)
		assertions.Nil(err)

		// Like episode
		resp := exec(router, put(fmt.Sprintf("/e/%s/like", referenceEpisode.Slug), "", nil))
		assertions.Equal(http.StatusCreated, resp.Code)

		// Check like count; liked by current user and score is 1 * LikeEpisodeScoreIncr
		resp = exec(router, get(fmt.Sprintf("/channel/%s/show/%s/episode/%s", show.ChannelSlug, show.Slug, referenceEpisode.Slug)))
		assertions.Equal(http.StatusOK, resp.Code)

		var episode model.Episode
		parse(resp, &episode)
		assertions.Equal(int64(1), episode.LikeCount)
		assertions.Equal(true, episode.LikedByCurrentUser)
		assertions.Equal(referenceEpisode.ImageKey, episode.ImageKey)

		episodeFromDB, err := episodeRepository.FindByID(referenceEpisode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.LikeEpisodeScoreIncr+model.OpenEpisodeScoreIncr), episodeFromDB.Score)

		// Check liked playlist
		var likedPlaylistItems []model.SlimEpisode
		getF("/playlist/liked/items").
			WithUser("fake-user").
			Exec(router).
			ExpectStatus(http.StatusOK).
			Parse(&likedPlaylistItems)

		assertions.GreaterOrEqual(len(likedPlaylistItems), 1)

		// Like episode as another user
		resp = exec(router, put(fmt.Sprintf("/e/%s/like", referenceEpisode.Slug), "", &model.User{ID: fake.Word()}))
		assertions.Equal(http.StatusCreated, resp.Code)

		// Check like count is 2 and score is 2 * LikeEpisodeScoreIncr
		resp = exec(router, get(fmt.Sprintf("/channel/%s/show/%s/episode/%s", show.ChannelSlug, show.Slug, referenceEpisode.Slug)))
		assertions.Equal(http.StatusOK, resp.Code)
		parse(resp, &episode)
		assertions.Equal(int64(2), episode.LikeCount)

		episodeFromDB, err = episodeRepository.FindByID(referenceEpisode.ID)
		if err != nil {
			t.Fatal(err)
		}
		assertions.Equal(int64(2*model.LikeEpisodeScoreIncr+(2*model.OpenEpisodeScoreIncr)), episodeFromDB.Score)

		// Undo like
		resp = exec(router, delete(fmt.Sprintf("/e/%s/like", referenceEpisode.Slug)))
		assertions.Equal(http.StatusOK, resp.Code)

		// Check like count is 1; liked by current user is false; and score is 1 * LikeEpisodeScoreIncr
		resp = exec(router, get(fmt.Sprintf("/channel/%s/show/%s/episode/%s", show.ChannelSlug, show.Slug, referenceEpisode.Slug)))
		assertions.Equal(http.StatusOK, resp.Code)
		parse(resp, &episode)
		assertions.Equal(int64(1), episode.LikeCount)
		assertions.Equal(false, episode.LikedByCurrentUser)

		episodeFromDB, err = episodeRepository.FindByID(referenceEpisode.ID)
		if err != nil {
			t.Fatal(err)
		}
		assertions.Equal(int64(model.LikeEpisodeScoreIncr+(3*model.OpenEpisodeScoreIncr)), episodeFromDB.Score)

		return 0
	})
}

func TestDuplicatedEpisodeSlug(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(episodeRepository model.EpisodeRepository, repository model.ShowRepository) int {
		assertions := assert.New(t)

		show, err := test.InsertShow(repository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		episode := model.Episode{
			Title:        fake.WordsN(3),
			AudioFileURL: fake.DomainName(),
			PublishedAt:  time.Now().UTC(),
			ShowID:       show.ID,
			ShowSlug:     show.Slug,
		}
		episode, err = test.InsertEpisode(episodeRepository, episode)
		assertions.Nil(err)

		// duplicated episode -> error
		episode, err = test.InsertEpisode(episodeRepository, episode)
		assertions.NotNil(err)

		// duplicated slug -> new slug
		duplicatedSlugEpisode := model.Episode{
			Title:        episode.Title,
			AudioFileURL: fake.DomainName(),
			PublishedAt:  time.Now().UTC(),
			ShowID:       show.ID,
			ShowSlug:     show.Slug,
		}

		duplicatedSlugEpisode, err = test.InsertEpisode(episodeRepository, duplicatedSlugEpisode)
		assertions.Nil(err)

		return 0
	})
}
