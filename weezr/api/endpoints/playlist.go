package endpoints

import (
	"fmt"
	"strconv"

	"weezr.me/weezr/business"

	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/model"
)

type PlaylistEndpoints struct {
	repository model.PlaylistRepository
	service    business.PlaylistService
}

func NewPlaylistEndpoints(repository model.PlaylistRepository, service business.PlaylistService) *PlaylistEndpoints {
	return &PlaylistEndpoints{repository, service}
}

func (e PlaylistEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Create Playlist",
			Pattern: "/playlist",
			Method:  "POST",
			Handler: e.createPlaylist,
		},
		{
			Name:    "Gets Playlist by Id or Type",
			Pattern: "/playlist/{playlistID}",
			Method:  "GET",
			Handler: e.getPlaylist,
		},
		{
			Name:    "Gets Playlist Items",
			Pattern: "/playlist/{playlistID}/items",
			Method:  "GET",
			Handler: e.getPlaylistItems,
		},
		{
			Name:    "Find All Playlists",
			Pattern: "/playlist",
			Method:  "GET",
			Handler: e.findAllPlaylists,
		},
		{
			Name:    "Deletes Playlist",
			Pattern: "/playlist/{playlistID}",
			Method:  "DELETE",
			Handler: e.deletePlaylist,
		},
		{
			Name:    "Adds Item to Playlist",
			Pattern: "/playlist/{playlistID}/item/{episodeID}",
			Method:  "POST",
			Handler: e.addItem,
		},
		{
			Name:    "Removes Item from Playlist",
			Pattern: "/playlist/{playlistID}/item/{episodeID}",
			Method:  "DELETE",
			Handler: e.removeItem,
		},
	}
}

type CreatePlaylistPayload struct {
	Params struct {
		Username string
	}
	Body model.Playlist
}

func (e PlaylistEndpoints) createPlaylist(payload CreatePlaylistPayload) whttp.Response {
	playlist := payload.Body
	playlist.Username = payload.Params.Username

	persisted, err := e.repository.Insert(playlist)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist")
		return whttp.ServerError(err)
	}

	return whttp.Ok(persisted)
}

type FindAllPlaylistsPayload struct {
	Params struct {
		Username string
	}
}

func (e PlaylistEndpoints) findAllPlaylists(payload FindAllPlaylistsPayload) whttp.Response {
	playlists, err := e.repository.FindAll(payload.Params.Username)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist")
		return whttp.ServerError(err)
	}

	return whttp.Ok(playlists)
}

type DeletePlaylistPayload struct {
	Params struct {
		PlaylistID string
	}
}

func (e PlaylistEndpoints) deletePlaylist(payload DeletePlaylistPayload) whttp.Response {
	playlistId, err := strconv.ParseInt(payload.Params.PlaylistID, 10, 64)
	if err != nil {
		logrus.WithError(err).WithField("playlistId", payload.Params.PlaylistID).Error("failed to find playlist")
		return whttp.NotFound(err)
	}

	err = e.repository.Delete(playlistId)
	if err != nil {
		logrus.WithError(err).Error("failed to delete playlist")
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}

type AddItemPayload struct {
	Params struct {
		Username   string
		PlaylistID string
		EpisodeID  string
	}
}

func (e PlaylistEndpoints) addItem(payload AddItemPayload) whttp.Response {
	episodeId, err := strconv.ParseInt(payload.Params.EpisodeID, 10, 64)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist item; failed to parse episode id")
		return whttp.ServerError(err)
	}

	err = e.service.AddItem(payload.Params.Username, payload.Params.PlaylistID, episodeId)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist item")
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}

func (e PlaylistEndpoints) removeItem(payload AddItemPayload) whttp.Response {
	episodeId, err := strconv.ParseInt(payload.Params.EpisodeID, 10, 64)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist item; failed to parse episode id")
		return whttp.ServerError(err)
	}

	logrus.WithField("payload", fmt.Sprintf("%+v", payload.Params)).Info("removing item from playlist")

	err = e.service.RemoveItem(payload.Params.Username, payload.Params.PlaylistID, episodeId)
	if err != nil {
		logrus.
			WithError(err).
			WithField("payload", fmt.Sprintf("%+v", payload.Params)).
			Error("failed to remove item from playlist")
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}

type GetPlaylistPayload struct {
	Params struct {
		PlaylistID string
		Username   string
	}
}

func (e PlaylistEndpoints) getPlaylist(payload GetPlaylistPayload) whttp.Response {
	logrus.WithFields(logrus.Fields{"id": payload.Params.PlaylistID, "username": payload.Params.Username}).Info("getting playlist by id")

	playlist, err := e.service.GetPlaylist(payload.Params.Username, payload.Params.PlaylistID)
	if err != nil {
		logrus.
			WithError(err).
			WithFields(logrus.Fields{"username": payload.Params.Username, "playlistId": payload.Params.PlaylistID}).
			Error("failed to get playlist")

		return whttp.ServerError(err)
	}

	return whttp.Ok(playlist)
}

type GetPlaylistItemsPayload struct {
	Params struct {
		PlaylistID string
		Username   string
		Limit      string
		Offset     string
	}
}

func (e PlaylistEndpoints) getPlaylistItems(payload GetPlaylistItemsPayload) whttp.Response {
	logrus.WithFields(logrus.Fields{"id": payload.Params.PlaylistID, "username": payload.Params.Username}).
		Info("getting playlist items")

	offset, err := strconv.ParseInt(payload.Params.Offset, 10, 64)
	if err != nil {
		offset = 0
	}

	limit, err := strconv.ParseInt(payload.Params.Limit, 10, 64)
	if err != nil {
		limit = 50
	}

	items, err := e.service.GetItems(payload.Params.Username, payload.Params.PlaylistID, model.Page{Offset: offset, Limit: limit})
	if err != nil {
		logrus.
			WithError(err).
			WithFields(logrus.Fields{"username": payload.Params.Username, "playlistId": payload.Params.PlaylistID}).
			Error("failed to get playlist")

		return whttp.ServerError(err)
	}

	return whttp.Ok(items)
}
