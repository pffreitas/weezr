package endpoints

import (
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
)

type UserEndpoints struct {
	repo              model.UserRepository
	userActionService business.UserActionService
}

func NewUserEndpoints(repo model.UserRepository, userActionService business.UserActionService) *UserEndpoints {
	return &UserEndpoints{repo, userActionService}
}

func (e UserEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Save User",
			Pattern: "/user/session",
			Method:  "POST",
			Handler: e.startSession,
		},
	}
}

type SaveUserPayload struct {
	Params struct {
		Username string
	}
	Body model.User
}

func (e UserEndpoints) startSession(payload SaveUserPayload) whttp.Response {
	logrus.WithField("username", payload.Params.Username).Info("starting session")

	payload.Body.ID = payload.Params.Username

	err := e.userActionService.Insert(model.UserAction{
		Username:   payload.Params.Username,
		ObjectId:   -1,
		ActionType: model.StartedSession,
	})
	if err != nil {
		return whttp.ServerError(err)
	}

	err = e.repo.Save(payload.Body)
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}
