package endpoints_test

import (
	"fmt"
	"net/http"
	"testing"

	"weezr.me/weezr/test"

	"github.com/icrowley/fake"

	"weezr.me/weezr/api/application"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
)

func TestDuplicatedShowSlug(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(repository model.ShowRepository) int {
		assertions := assert.New(t)

		showTitle := fake.WordsN(3)
		feedUrl := fake.DomainName()
		show := model.Show{
			Name:    showTitle,
			FeedURL: feedUrl,
		}

		show, err := test.InsertShow(repository, show)
		assertions.Nil(err)

		// same feed url and same slug -> error
		_, err = test.InsertShow(repository, show)
		assertions.NotNil(err)

		// same slug, diff feed url -> generated slug
		_, err = test.InsertShow(repository, model.Show{
			Name:    showTitle,
			FeedURL: fake.DomainName(),
		})
		assertions.Nil(err)

		// same feed url, diff slug -> error
		_, err = test.InsertShow(repository, model.Show{
			Name:    fake.WordsN(3),
			FeedURL: feedUrl,
		})
		assertions.NotNil(err)

		return 0
	})
}

func TestFollowShowEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(episodeRepository model.EpisodeRepository, showRepository model.ShowRepository) int {
		assertions := assert.New(t)
		router := app.Router

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		episode, err := test.InsertEpisode(episodeRepository, test.DefaultEpisode(show))
		if err != nil {
			t.Fatal(err)
		}

		for i := 0; i < 20; i++ {
			e, err := test.InsertEpisode(episodeRepository, test.DefaultEpisode(show))
			if err != nil {
				t.Fatal(err)
			}

			// like episode
			putF(fmt.Sprintf("/e/%s/like", e.Slug)).
				WithUser("fake-user").
				Exec(router).
				ExpectStatus(http.StatusCreated)
		}

		// user follows show
		showResponse := exec(router, post(fmt.Sprintf("/show/%s/follow", show.Slug), nil))
		assertions.Equal(200, showResponse.Code)

		// episode api returns FollowedByCurrentUser and FollowingCount
		episodeResponse := exec(router, get(fmt.Sprintf("/channel/cslug/show/%s/episode/%s", show.Slug, episode.Slug)))
		parse(episodeResponse, &episode)
		assertions.Equal(true, episode.FollowedByCurrentUser)
		assertions.Equal(int64(1), episode.FollowingCount)

		// list of shows user follows
		var following []model.Show
		followingResponse := exec(router, get(fmt.Sprintf("/following/shows")))
		parse(followingResponse, &following)
		assertions.GreaterOrEqual(len(following), 1)

		// Get show by slug
		getShowResponse := exec(router, get(fmt.Sprintf("/show/%s", show.Slug)))
		var getShow model.Show
		parse(getShowResponse, &getShow)
		assertions.Equal(show.Name, getShow.Name)
		assertions.Equal(int64(1), getShow.FollowingCount)

		// Get show episodes
		getShowEpisodes := exec(router, get(fmt.Sprintf("/show/%s/episodes?limit=10&offset=0", show.Slug)))
		var feedItems []model.SlimEpisode
		parse(getShowEpisodes, &feedItems)
		assertions.Equal(10, len(feedItems))
		assertions.True(feedItems[0].LikedByCurrentUser)

		getShowEpisodes = exec(router, get(fmt.Sprintf("/show/%s/episodes?limit=5&offset=10", show.Slug)))
		parse(getShowEpisodes, &feedItems)
		assertions.Equal(5, len(feedItems))

		// unfollow
		showResponse = exec(router, delete(fmt.Sprintf("/show/%s/follow", show.Slug)))
		assertions.Equal(200, showResponse.Code)

		// episode api returns FollowedByCurrentUser and FollowingCount
		episodeResponse = exec(router, get(fmt.Sprintf("/channel/cslug/show/%s/episode/%s", show.Slug, episode.Slug)))
		parse(episodeResponse, &episode)
		assertions.Equal(false, episode.FollowedByCurrentUser)
		assertions.Equal(int64(0), episode.FollowingCount)

		return 0
	})

}
