package endpoints

import (
	"strconv"

	"github.com/expectedsh/go-sonic/sonic"

	"weezr.me/weezr/business"

	"github.com/sirupsen/logrus"

	"weezr.me/weezr/model"

	"weezr.me/weezr/search/search"

	"weezr.me/weezr/api/whttp"
)

type SearchEndpoints struct {
	episodeRepository model.EpisodeRepository
	showRepository    model.ShowRepository
	userActionService business.UserActionService
	searchService     search.SearchService
}

func NewSearchEndpoints(
	episodeRepository model.EpisodeRepository,
	showRepository model.ShowRepository,
	userActionService business.UserActionService,
	searchService search.SearchService) *SearchEndpoints {

	return &SearchEndpoints{
		episodeRepository,
		showRepository,
		userActionService,
		searchService,
	}
}

func (ce SearchEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Search",
			Pattern: "/search",
			Method:  "GET",
			Handler: ce.search,
		},
		{
			Name:    "Sonic Search",
			Pattern: "/search/sonic",
			Method:  "GET",
			Handler: ce.sonicSearch,
		},
		{
			Name:    "Search",
			Pattern: "/search/show",
			Method:  "GET",
			Handler: ce.searchShows,
		},
	}
}

type SearchPayload struct {
	Params struct {
		Username  string
		QueryText string
		Limit     string
		Offset    string
	}
}

func (ce *SearchEndpoints) sonicSearch(payload SearchPayload) whttp.Response {
	sonicSearch, err := sonic.NewSearch("sonic-service", 1491, "SecretPassword")
	if err != nil {
		panic(err)
	}

	results, _ := sonicSearch.Query("episode", "title", payload.Params.QueryText, 10, 0)
	return whttp.Ok(results)
}

func (ce *SearchEndpoints) search(payload SearchPayload) whttp.Response {
	offset, err := strconv.ParseInt(payload.Params.Offset, 10, 64)
	if err != nil {
		offset = 0
	}

	limit, err := strconv.ParseInt(payload.Params.Limit, 10, 64)
	if err != nil {
		limit = 50
	}

	results, err := ce.searchService.SearchEpisodes(payload.Params.QueryText, model.Page{
		Offset: offset,
		Limit:  limit,
	})

	return whttp.Ok(results)
}

func (ce *SearchEndpoints) searchShows(payload SearchPayload) whttp.Response {
	results, err := ce.searchService.SearchShows(payload.Params.QueryText, model.Page{
		Offset: 0,
		Limit:  20,
	})

	if err != nil {
		logrus.WithError(err).Error("Failed to call search-service")
		whttp.ServerError(err)
	}

	return whttp.Ok(results)
}
