package endpoints_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"weezr.me/weezr/test"

	"github.com/sirupsen/logrus"

	"github.com/icrowley/fake"

	"weezr.me/weezr/api/application"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
)

func get(url string) *http.Request {
	logrus.Infof("GET %s", url)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err.Error())
	}

	req.Header["Authorization-Test"] = []string{"{\"Id\": \"fake-user\"}"}

	return req
}

func delete(url string) *http.Request {
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		panic(err.Error())
	}

	req.Header["Authorization-Test"] = []string{"{\"Id\": \"fake-user\"}"}

	return req
}

func put(url string, payload interface{}, username *model.User) *http.Request {
	requestBody, err := json.Marshal(payload)
	if err != nil {
		panic(err.Error())
	}

	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(requestBody))
	if err != nil {
		panic(err.Error())
	}

	var user *model.User

	if username == nil {
		user = &model.User{ID: "fake-user"}
	} else {
		user = username
	}

	jsonUser, _ := json.Marshal(user)

	req.Header["Authorization-Test"] = []string{string(jsonUser)}

	return req
}

func post(url string, payload interface{}) *http.Request {
	logrus.Infof("POST %s", url)
	requestBody, err := json.Marshal(payload)
	if err != nil {
		panic(err.Error())
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	if err != nil {
		panic(err.Error())
	}

	req.Header["Authorization-Test"] = []string{"{\"Id\": \"fake-user\"}"}

	return req
}

type RequestBuilder struct {
	Request  *http.Request
	Response *httptest.ResponseRecorder
}

func postTo(url string, payload interface{}) *RequestBuilder {
	logrus.Infof("POST %s", url)

	requestBody, err := json.Marshal(payload)
	if err != nil {
		panic(err.Error())
	}

	request, _ := http.NewRequest("POST", url, bytes.NewReader(requestBody))
	request.Header["Authorization-Test"] = []string{fmt.Sprintf("{\"Id\": \"%s\"}", fake.UserName())}

	return &RequestBuilder{Request: request}
}

func putF(url string) *RequestBuilder {
	logrus.Infof("GET %s", url)

	request, _ := http.NewRequest("PUT", url, nil)
	request.Header["Authorization-Test"] = []string{fmt.Sprintf("{\"Id\": \"%s\"}", fake.UserName())}

	return &RequestBuilder{Request: request}
}

func getF(url string) *RequestBuilder {
	logrus.Infof("GET %s", url)

	request, _ := http.NewRequest("GET", url, nil)
	request.Header["Authorization-Test"] = []string{fmt.Sprintf("{\"Id\": \"%s\"}", fake.UserName())}

	return &RequestBuilder{Request: request}
}

func (b *RequestBuilder) WithUser(username string) *RequestBuilder {
	b.Request.Header["Authorization-Test"] = []string{fmt.Sprintf("{\"Id\": \"%s\"}", username)}
	return b
}

func (b *RequestBuilder) Exec(router *mux.Router) *RequestBuilder {
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, b.Request)
	b.Response = resp
	return b
}

func (b *RequestBuilder) ExpectStatus(status int) *RequestBuilder {
	if b.Response.Code != status {
		panic(fmt.Sprintf("expected %d; got %d", status, b.Response.Code)) //TODO
	}
	return b
}

func (b *RequestBuilder) Parse(dest interface{}) *RequestBuilder {
	responseBytes := b.Response.Body.Bytes()
	logrus.Infof("parsing: %s", responseBytes)

	err := json.Unmarshal(responseBytes, dest)
	if err != nil {
		logrus.Errorf("failed to parse response: %s -- %+v", responseBytes, err)
		panic(err)
	}

	logrus.Infof("parsed: %+v", dest)
	return b
}

func exec(router *mux.Router, request *http.Request) *httptest.ResponseRecorder {
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, request)
	return resp
}

func TestPlaybackEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(
		episodeRepository model.EpisodeRepository,
		showRepository model.ShowRepository,
		repo model.PlaybackProgressRepository,
		userActionRepository model.UserActionRepository) int {

		assertions := assert.New(t)
		router := app.Router

		show, _ := test.InsertShow(showRepository, test.DefaultShow())
		episode, _ := test.InsertDefaultEpisode(episodeRepository, show)
		slug := episode.Slug
		username := "fake-user"

		// 10 secs progress
		resp := exec(router, post(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
			Progress: 10,
			Total:    23,
			Context:  model.PlaybackContext{"aa": "aa"},
		}))
		assertions.Equal(http.StatusOK, resp.Code)

		episode, err := episodeRepository.FindByID(episode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.PlayEpisodeScoreIncr), episode.Score)

		// 20 secs progress
		resp2 := exec(router, post(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
			Progress: 20,
			Finished: true,
			Total:    23,
			Context:  model.PlaybackContext{"aa": "aa"},
		}))
		assertions.Equal(http.StatusOK, resp2.Code)

		episode, err = episodeRepository.FindByID(episode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.PlayEpisodeScoreIncr+model.FinishPlayingEpisodeScoreIncr), episode.Score)

		// get progress
		getProgressResp := exec(router, get(fmt.Sprintf("/play/%s/progress", slug)))
		var progress model.PlaybackProgress
		parse(getProgressResp, &progress)

		assertions.Equal(20, progress.Progress)
		assertions.Equal(23, progress.Total)
		assertions.True(progress.Finished)
		assertions.Equal(model.PlaybackContext{"aa": "aa"}, progress.Context)

		finishedPlayingAction, err := userActionRepository.Get(username, episode.ID, model.FinishedPlaying)
		assertions.Nil(err)
		assertions.NotNil(finishedPlayingAction.CreatedAt)

		// find all
		all, err := repo.FindAll(episode.ID, username)
		assertions.Nil(err)
		assertions.Equal(1, len(all))
		assertions.Equal(20, all[0].Progress)

		return 0
	})
}

func TestPlaybackFinished(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(
		episodeRepository model.EpisodeRepository,
		showRepository model.ShowRepository,
		repo model.PlaybackProgressRepository,
		userActionRepository model.UserActionRepository) int {

		assertions := assert.New(t)
		router := app.Router

		show, _ := test.InsertShow(showRepository, test.DefaultShow())
		episode, _ := test.InsertDefaultEpisode(episodeRepository, show)
		slug := episode.Slug
		username := fake.UserName()

		_ = repo.Insert(model.PlaybackProgress{
			EpisodeID: episode.ID,
			Username:  username,
			Progress:  0,
			CreatedAt: time.Now().UTC().Add(-10 * time.Minute),
			UpdatedAt: time.Now().UTC().Add(-10 * time.Minute),
		})

		for _ = range []int{1, 2, 3} {
			_ = postTo(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
				Progress: 20,
				Finished: true,
			}).
				WithUser(username).
				Exec(router).
				ExpectStatus(http.StatusOK)
		}

		finishedPlayingActions, err := userActionRepository.FindAllByUsernameAndType(username, model.FinishedPlaying)
		assertions.Nil(err)
		assertions.Equal(1, len(finishedPlayingActions))

		// user goes back to 10s and it means he hasn't finished yet
		_ = postTo(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
			Progress: 10,
			Finished: false,
		}).
			WithUser(username).
			Exec(router).
			ExpectStatus(http.StatusOK)

		finishedPlayingActions, err = userActionRepository.FindAllByUsernameAndType(username, model.FinishedPlaying)
		assertions.Nil(err)
		assertions.Equal(0, len(finishedPlayingActions))

		// user goes to 20s again and it means he has finished
		_ = postTo(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
			Progress: 20,
			Finished: true,
		}).
			WithUser(username).
			Exec(router).
			ExpectStatus(http.StatusOK)

		finishedPlayingActions, err = userActionRepository.FindAllByUsernameAndType(username, model.FinishedPlaying)
		assertions.Nil(err)
		assertions.Equal(1, len(finishedPlayingActions))

		return 0
	})
}

func TestPlaybackAddToHistoryPlaylist(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(
		userActionRepository model.UserActionRepository,
		episodeRepository model.EpisodeRepository,
		showRepository model.ShowRepository,
		repo model.PlaybackProgressRepository) int {

		assertions := assert.New(t)
		router := app.Router

		show, _ := test.InsertShow(showRepository, test.DefaultShow())
		episode, _ := test.InsertDefaultEpisode(episodeRepository, show)
		slug := episode.Slug

		// 10 secs progress
		resp := exec(router, post(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
			Progress: 10,
			Context:  model.PlaybackContext{"aa": "aa"},
		}))
		assertions.Equal(http.StatusOK, resp.Code)

		// 20 secs progress
		resp2 := exec(router, post(fmt.Sprintf("/play/%s/progress", slug), model.PlaybackProgress{
			Progress: 20,
			Context:  model.PlaybackContext{"aa": "aa"},
		}))
		assertions.Equal(http.StatusOK, resp2.Code)

		// get progress
		getProgressResp := exec(router, get(fmt.Sprintf("/play/%s/progress", slug)))

		var progress model.PlaybackProgress
		_ = json.Unmarshal(getProgressResp.Body.Bytes(), &progress)

		assertions.Equal(20, progress.Progress)
		assertions.Equal(model.PlaybackContext{"aa": "aa"}, progress.Context)

		// get history playlist
		var historyPlaylistItems []model.SlimEpisode
		getF("/playlist/history/items").
			WithUser("fake-user").
			Exec(router).
			ExpectStatus(http.StatusOK).
			Parse(&historyPlaylistItems)

		assertions.GreaterOrEqual(len(historyPlaylistItems), 1)

		playedAction, err := userActionRepository.Get("fake-user", episode.ID, model.Played)
		assertions.Nil(err)
		assertions.NotNil(playedAction.CreatedAt)

		return 0
	})
}

func TestFindLastPlayedEpisode(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(
		userActionRepository model.UserActionRepository,
		episodeRepository model.EpisodeRepository,
		showRepository model.ShowRepository,
		repo model.PlaybackProgressRepository) int {

		assertions := assert.New(t)
		router := app.Router

		username := fake.UserName()
		var lastEpisode model.Episode
		var expectedLastEpisode model.Episode

		show, _ := test.InsertShow(showRepository, test.DefaultShow())

		// 10 episodes with 10 sec progress
		for i := 0; i < 10; i++ {
			episode, _ := test.InsertDefaultEpisode(episodeRepository, show)

			postTo(fmt.Sprintf("/play/%s/progress", episode.Slug), model.PlaybackProgress{
				Progress: 10,
			}).
				WithUser(username).
				Exec(router).
				ExpectStatus(http.StatusOK)

			expectedLastEpisode = episode
		}

		getF("/play/latest").
			WithUser(username).
			Exec(router).
			ExpectStatus(http.StatusOK).
			Parse(&lastEpisode)

		assertions.Equal(expectedLastEpisode.Slug, lastEpisode.Slug)

		return 0
	})
}
