package endpoints_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/api/application"
	"weezr.me/weezr/model"
)

func TestUserEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(userRepository model.UserRepository) int {
		assertions := assert.New(t)
		router := app.Router

		resp := exec(router, post("/user/session", model.User{
			Name:       "Paulo Freitas",
			Nickname:   "pffreitas",
			GivenName:  "Paulo",
			FamilyName: "Freitas",
			Email:      "pfreitas1@gmail.com",
			LastIP:     "129.129.129.129",
			Picture:    "https://img.weezr.me/me.png",
		}))

		assertions.Equal(200, resp.Code)

		user, err := userRepository.FindByID("fake-user")
		assertions.Nil(err)
		assertions.NotNil(user)
		assertions.Equal("pfreitas1@gmail.com", user.Email)

		resp = exec(router, post("/user/session", model.User{
			Name:       "Paulo Freitas",
			Nickname:   "pffreitas",
			GivenName:  "Paulo",
			FamilyName: "Freitas",
			Email:      "otis@gmail.com",
			LastIP:     "129.129.129.129",
			Picture:    "https://img.weezr.me/me.png",
			Locale:     "en",
		}))

		assertions.Equal(200, resp.Code)

		user, err = userRepository.FindByID("fake-user")
		assertions.Nil(err)
		assertions.NotNil(user)
		assertions.Equal("otis@gmail.com", user.Email)
		assertions.Equal("en", user.Locale)

		return 0
	})
}
