package endpoints_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/icrowley/fake"

	"weezr.me/weezr/test"

	"weezr.me/weezr/api/application"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
)

func TestPlaylistEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(repository model.EpisodeRepository, showRepository model.ShowRepository) int {
		assertions := assert.New(t)
		router := app.Router

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		assertions.Nil(err)

		episode, err := test.InsertDefaultEpisode(repository, show)
		assertions.Nil(err)

		episode2, err := test.InsertDefaultEpisode(repository, show)
		assertions.Nil(err)

		username := fake.UserName()

		// like episode
		putF(fmt.Sprintf("/e/%s/like", episode.Slug)).
			WithUser(username).
			Exec(router).
			ExpectStatus(http.StatusCreated)

		// Create Playlist
		playlist := model.Playlist{
			Type: model.UserDefined,
			Code: "custom-playlist",
		}

		resp := exec(router, post("/playlist", playlist))
		assertions.Equal(http.StatusOK, resp.Code)

		var persistedPlaylist model.Playlist
		parse(resp, &persistedPlaylist)

		// Find all playlists
		findAllResp := exec(router, get("/playlist"))
		assertions.Equal(http.StatusOK, findAllResp.Code)

		// Find custom playlist items
		findAllResp = exec(router, get(fmt.Sprintf("/playlist/%d", persistedPlaylist.ID)))
		assertions.Equal(http.StatusOK, findAllResp.Code)

		// Create Playlist
		playlist = model.Playlist{
			Type: model.UserDefined,
			Code: "custom-playlist-2",
		}

		resp = exec(router, post("/playlist", playlist))
		assertions.Equal(http.StatusOK, resp.Code)
		parse(resp, &persistedPlaylist)

		// Find all playlists
		findAllResp = exec(router, get("/playlist"))
		assertions.Equal(http.StatusOK, findAllResp.Code)

		// Add item
		addItemResp := exec(router, post(fmt.Sprintf("/playlist/%d/item/%d", persistedPlaylist.ID, episode.ID), ""))
		assertions.Equal(http.StatusOK, addItemResp.Code)

		// check score is incremented
		episode, err = repository.FindByID(episode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.AddEpisodeToPlaylistScoreIncr+model.LikeEpisodeScoreIncr), episode.Score)

		// Adds same item; should be idempotent
		addItemResp = exec(router, post(fmt.Sprintf("/playlist/%d/item/%d", persistedPlaylist.ID, episode.ID), ""))
		assertions.Equal(http.StatusOK, addItemResp.Code)

		// check score is incremented
		episode, err = repository.FindByID(episode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.AddEpisodeToPlaylistScoreIncr+model.LikeEpisodeScoreIncr), episode.Score)

		// Add item 2
		addItemResp = exec(router, post(fmt.Sprintf("/playlist/%d/item/%d", persistedPlaylist.ID, episode2.ID), ""))
		assertions.Equal(http.StatusOK, addItemResp.Code)

		// Get Playlist's items
		var playlistItems []model.SlimEpisode
		getF(fmt.Sprintf("/playlist/%d/items", persistedPlaylist.ID)).
			WithUser(username).
			Exec(router).
			ExpectStatus(200).
			Parse(&playlistItems)

		assertions.Equal(2, len(playlistItems))
		assertions.True(len(playlistItems[0].Description) > 0)
		assertions.True(len(playlistItems[0].ShowTitle) > 0)
		assertions.True(len(playlistItems[0].ShowSlug) > 0)
		assertions.True(len(playlistItems[0].Slug) > 0)
		assertions.True(len(playlistItems[0].Title) > 0)
		assertions.True(len(playlistItems[0].ImageKey) > 0)
		assertions.False(playlistItems[0].LikedByCurrentUser) // desc sorted
		assertions.True(playlistItems[1].LikedByCurrentUser)

		// Delete item
		deleteItemResp := exec(router, delete(fmt.Sprintf("/playlist/%d/item/%d", persistedPlaylist.ID, episode.ID)))
		assertions.Equal(http.StatusOK, deleteItemResp.Code)

		// check item is deleted (playlist has 1 item - episode 2)
		getItemsResp := exec(router, get(fmt.Sprintf("/playlist/%d/items", persistedPlaylist.ID)))
		assertions.Equal(http.StatusOK, getItemsResp.Code)
		parse(getItemsResp, &playlistItems)
		assertions.Equal(1, len(playlistItems))

		// check score is decremented
		episode, err = repository.FindByID(episode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.LikeEpisodeScoreIncr), episode.Score)

		// Add item again (previously deleted)
		addItemResp = exec(router, post(fmt.Sprintf("/playlist/%d/item/%d", persistedPlaylist.ID, episode.ID), ""))
		assertions.Equal(http.StatusOK, addItemResp.Code)

		getItemsResp = exec(router, get(fmt.Sprintf("/playlist/%d/items", persistedPlaylist.ID)))
		assertions.Equal(http.StatusOK, getItemsResp.Code)
		parse(getItemsResp, &playlistItems)
		assertions.Equal(2, len(playlistItems))

		deleteItemResp = exec(router, delete(fmt.Sprintf("/playlist/%d/item/%d", persistedPlaylist.ID, episode.ID)))
		assertions.Equal(http.StatusOK, deleteItemResp.Code)

		// Delete Playlist
		deletePlaylistResp := exec(router, delete(fmt.Sprintf("/playlist/%d", persistedPlaylist.ID)))
		assertions.Equal(http.StatusOK, deletePlaylistResp.Code)

		// Get Playlist's items should throw 404 as playlist was deleted
		getItemsResp = exec(router, get(fmt.Sprintf("/playlist/%d/item", persistedPlaylist.ID)))
		assertions.Equal(http.StatusNotFound, getItemsResp.Code)

		// add item to listen-later
		addToListenLaterResp := exec(router, post(fmt.Sprintf("/playlist/listen-later/item/%d", episode.ID), ""))
		assertions.Equal(http.StatusOK, addToListenLaterResp.Code)

		// check score is incremented
		episode, err = repository.FindByID(episode.ID)
		assertions.Nil(err)
		assertions.Equal(int64(model.AddEpisodeToPlaylistScoreIncr+model.LikeEpisodeScoreIncr), episode.Score)

		for i := 0; i < 50; i++ {
			e, err := test.InsertEpisode(repository, test.DefaultEpisode(show))
			if err != nil {
				t.Fatal(err)
			}

			addToListenLaterResp := exec(router, post(fmt.Sprintf("/playlist/listen-later/item/%d", e.ID), ""))
			assertions.Equal(http.StatusOK, addToListenLaterResp.Code)

			playbackProgressResp := exec(router, post(fmt.Sprintf("/play/%s/progress", e.Slug),
				model.PlaybackProgress{Progress: 180, Total: 183, Finished: true}))
			assertions.Equal(http.StatusOK, playbackProgressResp.Code)
		}

		// Get listen later playlist
		var listenLaterPlaylist model.Playlist
		getF("/playlist/listen-later").
			WithUser("fake-user").
			Exec(router).
			ExpectStatus(200).
			Parse(&listenLaterPlaylist)
		assertions.Equal("listen-later", listenLaterPlaylist.Code)
		assertions.Greater(listenLaterPlaylist.ID, int64(0))
		assertions.Equal(0, len(listenLaterPlaylist.Items))

		// Get listen later items
		getListenLaterItemsResp := exec(router, get("/playlist/listen-later/items?limit=25&offset=0"))
		assertions.Equal(http.StatusOK, getListenLaterItemsResp.Code)

		var listenLaterItems []model.SlimEpisode
		parse(getListenLaterItemsResp, &listenLaterItems)
		assertions.Equal(25, len(listenLaterItems))
		assertions.True(listenLaterItems[24].Finished)
		assertions.Equal(180, listenLaterItems[24].Progress)
		assertions.Equal(183, listenLaterItems[24].TotalLength)

		return 0
	})

}

func parse(recorder *httptest.ResponseRecorder, dest interface{}) {
	_ = json.Unmarshal(recorder.Body.Bytes(), dest)
}
