package endpoints_test

import (
	"fmt"
	"testing"

	"weezr.me/weezr/test"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/api/application"
	"weezr.me/weezr/model"
)

func TestCommentEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(userRepository model.UserRepository, showRepository model.ShowRepository, episodeRepository model.EpisodeRepository) int {
		assertions := assert.New(t)
		router := app.Router

		_ = userRepository.Save(model.User{
			ID:         "fake-user",
			Username:   "fake-user",
			Name:       "fake-user",
			Nickname:   "fake-user",
			GivenName:  "Fake",
			FamilyName: "User",
			Email:      "fake@user.com",
			LastIP:     "129.129.129.129",
			Picture:    "http://img.weezr.me/fake-user.png",
		})

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		episode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		comment := model.Comment{
			EpisodeID: episode.ID,
			Text:      fake.WordsN(230),
		}

		insertComment := exec(router, post("/comment", comment))
		assertions.Equal(200, insertComment.Code)

		var insertedComment model.Comment
		parse(insertComment, &insertedComment)
		assertions.Equal(insertedComment.Text, comment.Text)
		assertions.Equal(insertedComment.User.Email, "fake@user.com")

		episodeFromDB, err := episodeRepository.FindByID(episode.ID)
		if err != nil {
			t.Fatal(err)
		}
		assertions.Equal(int64(model.AddCommentToEpisodeScoreIncr), episodeFromDB.Score)

		getComment := exec(router, get(fmt.Sprintf("/comment?EpisodeId=%d", episode.ID)))
		assertions.Equal(200, getComment.Code)

		var comments []model.Comment
		parse(getComment, &comments)
		assertions.Equal(1, len(comments))
		assertions.Equal(comment.Text, comments[0].Text)
		assertions.Equal("fake-user", comments[0].User.Username)
		assertions.Equal("fake-user", comments[0].User.Name)
		assertions.Equal("http://img.weezr.me/fake-user.png", comments[0].User.Picture)
		assertions.Equal("Fake", comments[0].User.GivenName)
		assertions.Equal("User", comments[0].User.FamilyName)

		commentResponse := model.Comment{
			Text: fake.WordsN(330),
		}
		insertResponse := exec(router, post(fmt.Sprintf("/comment/%d/response", comments[0].ID), commentResponse))
		assertions.Equal(200, insertResponse.Code)

		var insertedResponse model.Comment
		parse(insertResponse, &insertedResponse)
		assertions.Equal(insertedResponse.Text, commentResponse.Text)
		assertions.Equal(insertedResponse.User.Email, "fake@user.com")

		episodeFromDB, err = episodeRepository.FindByID(episode.ID)
		if err != nil {
			t.Fatal(err)
		}
		assertions.Equal(int64(2*model.AddCommentToEpisodeScoreIncr), episodeFromDB.Score)

		getCommentResponse := exec(router, get(fmt.Sprintf("/comment/%d/response", comments[0].ID)))
		assertions.Equal(200, getCommentResponse.Code)

		var commentResponses []model.Comment
		parse(getCommentResponse, &commentResponses)
		assertions.Equal(1, len(commentResponses))
		assertions.Equal(commentResponse.Text, commentResponses[0].Text)
		assertions.Equal("fake-user", commentResponses[0].Username)
		assertions.Equal("fake-user", commentResponses[0].User.Username)
		assertions.Equal("fake-user", commentResponses[0].User.Name)
		assertions.Equal("http://img.weezr.me/fake-user.png", commentResponses[0].User.Picture)
		assertions.Equal("Fake", commentResponses[0].User.GivenName)
		assertions.Equal("User", commentResponses[0].User.FamilyName)

		getComment = exec(router, get(fmt.Sprintf("/comment?EpisodeId=%d", episode.ID)))
		assertions.Equal(200, getComment.Code)

		var comments2 []model.Comment
		parse(getComment, &comments2)
		assertions.Equal(1, len(comments2))
		assertions.Equal(int64(1), comments2[0].ResponseCount)
		assertions.Equal(int64(0), comments2[0].ParentID)

		return 0
	})

}
