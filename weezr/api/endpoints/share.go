package endpoints

import (
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
)

type ShareEndpoints struct {
	business.ShareService
}

func NewShareEndpoints(shareService business.ShareService) *ShareEndpoints {
	return &ShareEndpoints{shareService}
}

func (e ShareEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Share episode on social media",
			Pattern: "/share/{slug}",
			Method:  "POST",
			Handler: e.share,
		},
	}
}

type SharePayload struct {
	Params struct {
		Username string
		Slug     string
	}
}

func (e ShareEndpoints) share(payload SharePayload) whttp.Response {
	logrus.WithFields(logrus.Fields{"username": payload.Params.Username, "slug": payload.Params.Slug}).Info("sharing episode")

	err := e.ShareService.Share(payload.Params.Username, model.EpisodeSlug(payload.Params.Slug))
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}
