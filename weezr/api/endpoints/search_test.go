package endpoints_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"weezr.me/weezr/search/search"

	"weezr.me/weezr/api/application"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
)

func TestSearchEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(repository model.EpisodeRepository, showRepository model.ShowRepository) int {

		marshal, _ := json.Marshal(search.NewIndexedEpisode(model.Episode{
			ID:   10,
			Slug: "bla",
		}))

		fmt.Println(string(marshal))

		assertions := assert.New(t)
		router := app.Router

		shareResponse := exec(router, get("/search?queryText=nerdcast&limit=50&offset=50"))
		assertions.Equal(200, shareResponse.Code)

		fmt.Printf("%+v", string(shareResponse.Body.Bytes()))
		return 0
	})

}
