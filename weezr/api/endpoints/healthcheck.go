package endpoints

import (
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
)

type HealthCheckEndpoints struct {
}

func NewHealthCheckEndpoints() *HealthCheckEndpoints {
	return &HealthCheckEndpoints{}
}

func (he HealthCheckEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Health Check",
			Pattern: "/health",
			Method:  "GET",
			Handler: he.healthCheckGet,
		},
	}
}

func (he *HealthCheckEndpoints) healthCheckGet() whttp.Response {
	logrus.Info("Health check")
	return whttp.Response{
		Code: 200,
		Body: map[string]string{
			"DB": "OK",
		},
	}
}
