package endpoints

import (
	"strconv"

	"weezr.me/weezr/business"

	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/model"
)

type CommentsEndpoints struct {
	commentRepository model.CommentRepository
	scoreService      business.ScoreService
}

func NewCommentsEndpoints(repository model.CommentRepository, scoreService business.ScoreService) *CommentsEndpoints {
	return &CommentsEndpoints{repository, scoreService}
}

func (e CommentsEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return []whttp.EndpointDefinition{
		{
			Name:    "Insert Comment",
			Pattern: "/comment",
			Method:  "POST",
			Handler: e.Insert,
		},
		{
			Name:    "Get Comments",
			Pattern: "/comment",
			Method:  "GET",
			Handler: e.Get,
		},
		{
			Name:    "Insert Comment Response",
			Pattern: "/comment/{parentID}/response",
			Method:  "POST",
			Handler: e.InsertResponse,
		},
		{
			Name:    "Get Comment Response",
			Pattern: "/comment/{parentID}/response",
			Method:  "GET",
			Handler: e.GetResponses,
		},
	}
}

type InsertCommentPayload struct {
	Params struct {
		Username string
	}
	Body model.Comment
}

func (e CommentsEndpoints) Insert(payload InsertCommentPayload) whttp.Response {
	logrus.WithField("episode-id", payload.Body.EpisodeID).Info("inserting comment")

	payload.Body.Username = payload.Params.Username

	commentId, err := e.commentRepository.Insert(payload.Body)
	if err != nil {
		logrus.WithError(err).Errorf("failed to insert comment %+v", payload)
		return whttp.ServerError(err)
	}

	err = e.scoreService.IncrEpisodeScore(payload.Body.EpisodeID, model.AddCommentToEpisodeScoreIncr)
	if err != nil {
		logrus.WithError(err).Errorf("failed to insert comment; failed to incr score; %+v", payload)
		return whttp.ServerError(err)
	}

	comment, err := e.commentRepository.FindByID(commentId)
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok(comment)
}

type GetCommentPayload struct {
	Params struct {
		EpisodeId string
		Offset    string
		Limit     string
	}
}

func (e CommentsEndpoints) Get(payload GetCommentPayload) whttp.Response {
	logrus.WithField("episode-id", payload.Params.EpisodeId).Info("fetching comments")

	episodeId, err := strconv.ParseInt(payload.Params.EpisodeId, 10, 64)
	if err != nil {
		logrus.WithError(err).Errorf("failed to get comments %+v", payload)
		return whttp.ServerError(err)
	}

	if len(payload.Params.Offset) == 0 {
		payload.Params.Offset = "0"
	}

	if len(payload.Params.Limit) == 0 {
		payload.Params.Limit = "20"
	}

	offset, err := strconv.ParseInt(payload.Params.Offset, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	limit, err := strconv.ParseInt(payload.Params.Limit, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	comments, err := e.commentRepository.Get(episodeId, model.Page{
		Offset: offset,
		Limit:  limit,
	})
	if err != nil {
		logrus.WithError(err).Errorf("failed to get comments %+v", payload)
		return whttp.ServerError(err)
	}

	return whttp.Ok(comments)
}

type InsertResponsePayload struct {
	Params struct {
		ParentID string
		Username string
	}
	Body model.Comment
}

func (e CommentsEndpoints) InsertResponse(payload InsertResponsePayload) whttp.Response {
	logrus.WithField("parent-id", payload.Params.ParentID).Info("inserting comment response")

	parentId, err := strconv.ParseInt(payload.Params.ParentID, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	parentComment, err := e.commentRepository.FindByID(parentId)
	if err != nil {
		return whttp.ServerError(err)
	}

	payload.Body.Username = payload.Params.Username

	responseID, err := e.commentRepository.InsertCommentResponse(parentComment, payload.Body)
	if err != nil {
		return whttp.ServerError(err)
	}

	err = e.scoreService.IncrEpisodeScore(parentComment.EpisodeID, model.AddCommentToEpisodeScoreIncr)
	if err != nil {
		logrus.WithError(err).Errorf("failed to insert comment; failed to incr score; %+v", payload)
		return whttp.ServerError(err)
	}

	comment, err := e.commentRepository.FindByID(responseID)
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok(comment)
}

type GetResponsePayload struct {
	Params struct {
		ParentID string
		Limit    string
		Offset   string
	}
}

func (e CommentsEndpoints) GetResponses(payload GetResponsePayload) whttp.Response {
	logrus.WithField("parent-id", payload.Params.ParentID).Info("getting comment response")

	parentId, err := strconv.ParseInt(payload.Params.ParentID, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	if len(payload.Params.Offset) == 0 {
		payload.Params.Offset = "0"
	}

	if len(payload.Params.Limit) == 0 {
		payload.Params.Limit = "20"
	}

	offset, err := strconv.ParseInt(payload.Params.Offset, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	limit, err := strconv.ParseInt(payload.Params.Limit, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	responses, err := e.commentRepository.GetCommentResponses(parentId, model.Page{
		Offset: offset,
		Limit:  limit,
	})
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok(responses)
}
