package endpoints_test

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"weezr.me/weezr/business"

	"weezr.me/weezr/feed/feedsvc"

	"github.com/icrowley/fake"

	"weezr.me/weezr/test"

	"github.com/stretchr/testify/assert"

	"weezr.me/weezr/model"

	"weezr.me/weezr/api/application"
)

func TestFeedEndpoints(t *testing.T) {
	assertions := assert.New(t)

	app := application.New()
	router := app.Router

	app.Container.Invoke(func(
		repository model.EpisodeRepository,
		showRepository model.ShowRepository,
		service feedsvc.FeedService,
		promotionsRepository model.PromotionRepository) int {

		username := fake.UserName()

		var shows = make([]model.Show, 3)
		for i := 0; i < 3; i++ {
			show, err := showRepository.Insert(test.DefaultShow())
			if err != nil {
				t.Fatal(err)
			}
			shows[i] = show
		}

		for i := 0; i < 15; i++ {
			episode, err := test.InsertDefaultEpisode(repository, shows[0])
			if err != nil {
				t.Fatal(err)
			}

			err = service.PutEpisode(username, episode, episode.Score)
			if err != nil {
				t.Fatal(err)
			}

			// like episode
			putF(fmt.Sprintf("/e/%s/like", episode.Slug)).
				WithUser(username).
				Exec(router).
				ExpectStatus(http.StatusCreated)

			englishEpisode := test.DefaultEpisode(shows[0])
			englishEpisode.Language = model.English
			englishEpisode, err = test.InsertEpisode(repository, englishEpisode)
			if err != nil {
				t.Fatal(err)
			}

			err = service.PutEpisode("", englishEpisode, englishEpisode.Score)
			if err != nil {
				t.Fatal(err)
			}
		}

		for _, show := range shows {
			err := promotionsRepository.Insert(model.FeedPromotion{
				FeedObjectReference: model.FeedObjectReference{
					ObjectType: model.ShowFeedObjectType,
					ObjectID:   show.ID,
				},
				Placement:   model.FeedPlacement,
				Impressions: 10,
				ExpiresAt:   time.Now().UTC().Add(2 * time.Hour),
			})
			assertions.Nil(err)
		}

		var feedPage model.FeedPage
		getF("/feed?limit=10&offset=0&language=pt").
			WithUser(username).
			Exec(router).
			ExpectStatus(200).
			Parse(&feedPage)
		assertions.Equal(10, len(feedPage.Episodes))
		assertions.Equal(2, len(feedPage.Promotions))
		assertions.Equal(shows[0].ID, feedPage.Promotions[0].ObjectID)
		assertions.Equal(shows[1].ID, feedPage.Promotions[1].ObjectID)
		assertions.Equal(10, len(feedPage.Promotions[0].EpisodeList.Episodes))
		assertions.True(feedPage.Episodes[0].LikedByCurrentUser)

		getF("/feed?limit=10&offset=10&language=pt").
			WithUser(username).
			Exec(router).
			ExpectStatus(200).
			Parse(&feedPage)
		assertions.Equal(5, len(feedPage.Episodes))
		assertions.Equal(2, len(feedPage.Promotions))
		return 0
	})
}

func TestFeedBanner(t *testing.T) {
	assertions := assert.New(t)

	app := application.New()
	router := app.Router

	app.Container.Invoke(func(
		repository model.EpisodeRepository,
		showRepository model.ShowRepository,
		episodeRepository model.EpisodeRepository,
		service feedsvc.FeedService,
		promotionsRepository model.PromotionRepository) int {

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		episode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		for i := 0; i < 5; i++ {
			err := promotionsRepository.Insert(model.FeedPromotion{
				FeedObjectReference: model.FeedObjectReference{
					ObjectID:   show.ID,
					ObjectType: model.ShowFeedObjectType,
				},
				Placement:   model.HomeBannerPlacement,
				Impressions: 10,
				ExpiresAt:   time.Now().UTC().Add(2 * time.Hour),
			})
			assertions.Nil(err)
		}

		var bannerPromo []model.FeedPromotion
		getF("/feed/banner").
			Exec(router).
			ExpectStatus(200).
			Parse(&bannerPromo)

		assertions.Equal(5, len(bannerPromo))
		assertions.Equal(show.ID, bannerPromo[0].EpisodeList.ID)
		assertions.Equal(show.Slug, bannerPromo[0].EpisodeList.Slug)
		assertions.Equal(show.ImageKey, bannerPromo[0].EpisodeList.ImageKey)

		assertions.Equal(1, len(bannerPromo[0].EpisodeList.Episodes))
		assertions.Equal(episode.Slug, bannerPromo[0].EpisodeList.Episodes[0].Slug)
		assertions.Equal(episode.Title, bannerPromo[0].EpisodeList.Episodes[0].Title)
		assertions.Equal(episode.ImageKey, bannerPromo[0].EpisodeList.Episodes[0].ImageKey)

		return 0
	})
}

func TestLatestFromFollowing(t *testing.T) {
	assertions := assert.New(t)

	app := application.New()
	router := app.Router

	app.Container.Invoke(func(
		repository model.EpisodeRepository,
		showRepository model.ShowRepository,
		episodeRepository model.EpisodeRepository,
		service feedsvc.FeedService,
		promotionsRepository model.PromotionRepository, actionService business.UserActionService) int {

		username := fake.UserName()

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		_, err = test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		_, err = test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		show2, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		_, err = test.InsertDefaultEpisode(episodeRepository, show2)
		if err != nil {
			t.Fatal(err)
		}

		oldEpisode := test.DefaultEpisode(show2)
		oldEpisode.PublishedAt = time.Now().Add(-(13 * 24) * time.Hour)
		_, err = test.InsertEpisode(episodeRepository, oldEpisode)
		if err != nil {
			t.Fatal(err)
		}

		err = actionService.Insert(model.UserAction{
			Username:   username,
			ObjectId:   show.ID,
			ActionType: model.FollowedShow,
		})
		if err != nil {
			t.Fatal(err)
		}

		err = actionService.Insert(model.UserAction{
			Username:   username,
			ObjectId:   show2.ID,
			ActionType: model.FollowedShow,
		})
		if err != nil {
			t.Fatal(err)
		}

		var latestEpisodes []model.SlimEpisode
		getF("/feed/latest-from-following").
			WithUser(username).
			Exec(router).
			ExpectStatus(200).
			Parse(&latestEpisodes)

		assertions.Equal(3, len(latestEpisodes))

		return 0
	})
}

func TestContinueToListen(t *testing.T) {
	assertions := assert.New(t)

	app := application.New()
	router := app.Router

	app.Container.Invoke(func(
		repository model.EpisodeRepository,
		showRepository model.ShowRepository,
		episodeRepository model.EpisodeRepository,
		service feedsvc.FeedService,
		promotionsRepository model.PromotionRepository,
		actionService business.UserActionService,
		progressRepository model.PlaybackProgressRepository) int {

		username := fake.UserName()

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		episode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		oldEpisode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		finishedEpisode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		err = progressRepository.Insert(model.PlaybackProgress{
			EpisodeID: episode.ID,
			Username:  username,
			Progress:  8,
			Finished:  false,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		})
		if err != nil {
			t.Fatal(err)
		}

		err = progressRepository.Insert(model.PlaybackProgress{
			EpisodeID: episode.ID,
			Username:  username,
			Progress:  18,
			Finished:  false,
			CreatedAt: time.Now().Add(5 * time.Minute),
			UpdatedAt: time.Now().Add(5 * time.Minute),
		})
		if err != nil {
			t.Fatal(err)
		}

		err = progressRepository.Insert(model.PlaybackProgress{
			EpisodeID: finishedEpisode.ID,
			Username:  username,
			Progress:  100,
			Finished:  true,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		})
		if err != nil {
			t.Fatal(err)
		}

		err = progressRepository.Insert(model.PlaybackProgress{
			EpisodeID: oldEpisode.ID,
			Username:  username,
			Progress:  30,
			Finished:  false,
			CreatedAt: time.Now().Add(-(46 * 24) * time.Hour),
			UpdatedAt: time.Now().Add(-(46 * 24) * time.Hour),
		})
		if err != nil {
			t.Fatal(err)
		}

		var continueToListenEpisodes []model.SlimEpisode
		getF("/feed/continue-to-listen").
			WithUser(username).
			Exec(router).
			ExpectStatus(200).
			Parse(&continueToListenEpisodes)

		assertions.Equal(1, len(continueToListenEpisodes))

		return 0
	})
}
