package endpoints

import (
	"fmt"
	"regexp"
	"strings"

	"weezr.me/weezr/episode"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
	"weezr.me/weezr/search/search"
)

type EpisodeEndpoints struct {
	business.EpisodeService
	episodeRepository model.EpisodeRepository
	userActionService business.UserActionService
	episodeSvc        episode.Service
	searchService     search.SearchService
}

func NewEpisodeEndpoints(
	episodeService business.EpisodeService,
	episodeRepository model.EpisodeRepository,
	userActionService business.UserActionService,
	episodeSvc episode.Service,
	searchService search.SearchService) *EpisodeEndpoints {

	return &EpisodeEndpoints{
		episodeService,
		episodeRepository,
		userActionService,
		episodeSvc,
		searchService,
	}
}

func (se EpisodeEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Get Episode",
			Pattern: "/channel/{slug}/show/{showSlug}/episode/{episodeSlug}",
			Method:  "GET",
			Handler: se.getEpisode,
		},
		{
			Name:    "Get Episode Recommendations",
			Pattern: "/channel/{slug}/show/{showSlug}/episode/{episodeSlug}/recommendations",
			Method:  "GET",
			Handler: se.getRecommendations,
		},
		{
			Name:    "Like Episode",
			Pattern: "/e/{EpisodeSlug}/like",
			Method:  "PUT",
			Handler: se.likeEpisode,
		},
		{
			Name:    "Undo Like Episode",
			Pattern: "/e/{EpisodeSlug}/like",
			Method:  "DELETE",
			Handler: se.undoLikeEpisode,
		},
	}
}

type InsertEpisodeRequestPayload struct {
	Body   model.Episode
	Params struct {
		ShowSlug string
	}
}

type GetEpisodeRequestPayload struct {
	Params struct {
		Username    string
		EpisodeSlug string
	}
}

func (se *EpisodeEndpoints) getEpisode(payload GetEpisodeRequestPayload) whttp.Response {
	e, err := se.episodeSvc.GetEpisode(payload.Params.Username, payload.Params.EpisodeSlug)
	if err != nil {
		log.WithError(err).Error("failed to get episode; failed to post to related episode fan out queue")
		return whttp.ServerError(err)
	}
	return whttp.Ok(e)
}

func (se *EpisodeEndpoints) getRecommendations(payload GetEpisodeRequestPayload) whttp.Response {
	e, err := se.EpisodeRepository.FindBySlug(model.EpisodeSlug(payload.Params.EpisodeSlug))
	if err != nil {
		log.Error(err)
		return whttp.ServerError(err)
	}

	content := e.TidyContent
	if len(content) == 0 {
		content = e.Description
	}

	content = strings.ToLower(content)
	r, _ := regexp.Compile("[^a-zA-Z ]")
	content = r.ReplaceAllString(content, " ")

	response, err := se.searchService.SearchEpisodes(content, model.Page{
		Offset: 0,
		Limit:  15,
	})

	if err != nil {
		logrus.WithError(err).Error("Failed to call search-service")
		whttp.ServerError(err)
	}

	return whttp.Ok(response)
}

type LikeEpisodePayload struct {
	Params struct {
		Username    string
		EpisodeSlug string
	}
}

func (se *EpisodeEndpoints) likeEpisode(payload LikeEpisodePayload) whttp.Response {
	log.WithFields(log.Fields{"payload": fmt.Sprintf("%+v", payload)}).Info("episode like")

	username := payload.Params.Username
	episodeSlug := model.EpisodeSlug(payload.Params.EpisodeSlug)

	e, err := se.EpisodeService.Like(username, episodeSlug)
	if err != nil {
		log.WithError(err).
			WithFields(log.Fields{"payload": fmt.Sprintf("%+v", payload)}).
			Error("failed to like episode")
		return whttp.ServerError(err)
	}

	return whttp.Created(e)
}

func (se *EpisodeEndpoints) undoLikeEpisode(payload LikeEpisodePayload) whttp.Response {
	log.WithFields(log.Fields{"payload": fmt.Sprintf("%+v", payload)}).Info("episode like")

	username := payload.Params.Username
	episodeSlug := model.EpisodeSlug(payload.Params.EpisodeSlug)

	e, err := se.EpisodeService.UndoLike(username, episodeSlug)
	if err != nil {
		log.WithError(err).
			WithFields(log.Fields{"payload": fmt.Sprintf("%+v", payload)}).
			Error("failed to undo episode like")
		return whttp.ServerError(err)
	}

	return whttp.Ok(e)
}
