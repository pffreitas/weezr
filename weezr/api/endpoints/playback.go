package endpoints

import (
	"database/sql"
	"time"

	"weezr.me/weezr/episode"

	"weezr.me/weezr/business"

	log "github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/model"
)

type PlaybackEndpoints struct {
	repository        model.PlaybackProgressRepository
	playlistService   business.PlaylistService
	episodeRepository model.EpisodeRepository
	episodeSvc        episode.Service
	userActionService business.UserActionService
	scoreService      business.ScoreService
}

func NewPlaybackEndpoints(repository model.PlaybackProgressRepository,
	playlistService business.PlaylistService,
	episodeRepository model.EpisodeRepository,
	episodeSvc episode.Service,
	userActionService business.UserActionService,
	scoreService business.ScoreService) *PlaybackEndpoints {
	return &PlaybackEndpoints{
		repository,
		playlistService,
		episodeRepository,
		episodeSvc,
		userActionService,
		scoreService,
	}
}

func (pe PlaybackEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Track Episode Progress",
			Pattern: "/play/{slug}/progress",
			Method:  "POST",
			Handler: pe.playEpisode,
		},
		{
			Name:    "Get Episode Progress",
			Pattern: "/play/{slug}/progress",
			Method:  "GET",
			Handler: pe.getEpisodeProgress,
		},
		{
			Name:    "Get Last Played Episode",
			Pattern: "/play/latest",
			Method:  "GET",
			Handler: pe.getLastPlayedEpisode,
		},
	}
}

type PlaybackRequestPayload struct {
	Params struct {
		Slug     string
		Username string
	}
	Body struct {
		Progress int
		Total    int
		Finished bool
		Context  model.PlaybackContext
	}
}

func (pe *PlaybackEndpoints) playEpisode(payload PlaybackRequestPayload) whttp.Response {
	log.Info("Recording playback progress", payload.Params)

	slug := payload.Params.Slug

	episode, err := pe.episodeRepository.FindBySlug(model.EpisodeSlug(slug))
	if err != nil {
		log.WithError(err).
			WithField("episode-slug", slug).
			Error("failed to record playback progress;")

		return whttp.ServerError(err)
	}

	playback := model.PlaybackProgress{
		EpisodeID: episode.ID,
		Username:  payload.Params.Username,
		Progress:  payload.Body.Progress,
		Total:     payload.Body.Total,
		Context:   payload.Body.Context,
		Finished:  payload.Body.Finished,
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
	}

	latest, err := pe.repository.FindLatest(playback.EpisodeID, playback.Username)
	if err != nil && err != sql.ErrNoRows {
		log.WithError(err).
			WithField("episode-slug", slug).
			Error("failed to record playback progress; failed to find latest playback progress")
		return whttp.ServerError(err)
	}

	if err == sql.ErrNoRows {
		playback, err := pe.recordInitialPlaying(episode, playback, payload.Params.Username)
		if err != nil {
			log.WithError(err).
				WithField("episode-slug", slug).
				Error("failed to record playback progress; failed to record initial progress")
			return whttp.ServerError(err)
		}

		return whttp.Ok(playback)
	}

	playback, err = pe.recordPlaybackProgress(playback, latest)
	if err != nil {
		log.WithError(err).
			WithField("episode-slug", slug).
			Error("failed to record playback progress; failed to record progress")
		return whttp.ServerError(err)
	}

	err = pe.recordFinishedPlayback(playback.Username, episode, payload.Body.Finished)
	if err != nil {
		log.WithError(err).
			WithField("episode-slug", slug).
			Error("failed to record playback progress; failed to record finished")
		return whttp.ServerError(err)
	}

	return whttp.Ok(playback)
}

func (pe *PlaybackEndpoints) recordInitialPlaying(episode model.Episode, playback model.PlaybackProgress, username string) (model.PlaybackProgress, error) {
	err := pe.playlistService.AddItemToHistory(username, episode.ID)
	if err != nil {
		log.WithError(err).Error("failed to add episode to history playlist")
		return playback, err
	}

	err = pe.episodeRepository.IncrPlayback(model.EpisodeSlug(episode.Slug))
	if err != nil {
		log.WithError(err).Error("failed to count episode playback")
		return playback, err
	}

	err = pe.scoreService.IncrEpisodeScore(episode.ID, model.PlayEpisodeScoreIncr)
	if err != nil {
		log.WithError(err).Error("failed to incr episode score; PlayEpisodeScoreIncr")
		return playback, err
	}

	err = pe.userActionService.Insert(model.UserAction{
		Username:   username,
		ObjectId:   episode.ID,
		ActionType: model.Played,
	})

	if err != nil {
		log.WithError(err).
			WithField("episode-slug", episode.Slug).
			Error("Failed to record `played` action")
	}

	err = pe.repository.Insert(playback)
	return playback, err
}

func (pe *PlaybackEndpoints) recordPlaybackProgress(playback model.PlaybackProgress, latest model.PlaybackProgress) (model.PlaybackProgress, error) {
	if time.Now().UTC().After(latest.UpdatedAt.Add(time.Minute * 1)) {
		log.WithField("episode-id", playback.EpisodeID).
			WithField("username", playback.Username).
			Info("Latest progress for episode is older than 1 minute; Inserting new progress record.")

		err := pe.repository.Insert(playback)

		if err != nil {
			log.Error("Error inserting playback progress", err)
			return playback, err
		}
	} else {
		err := pe.repository.Update(playback)

		if err != nil {
			log.Error("Error updating playback progress", err)
			return playback, err
		}
	}

	return playback, nil
}

func (pe *PlaybackEndpoints) recordFinishedPlayback(username string, episode model.Episode, finished bool) error {
	_, err := pe.userActionService.GetAction(username, episode.ID, model.FinishedPlaying)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if finished && err == sql.ErrNoRows {
		err = pe.userActionService.Insert(model.UserAction{
			Username:   username,
			ObjectId:   episode.ID,
			ActionType: model.FinishedPlaying,
		})
		if err != nil {
			log.WithError(err).
				WithField("episode-slug", episode.Slug).
				Error("Failed to record `finished playing` action")
			return err
		}

		err = pe.scoreService.IncrEpisodeScore(episode.ID, model.FinishPlayingEpisodeScoreIncr)
		if err != nil {
			log.WithError(err).
				WithField("episode-slug", episode.Slug).
				Error("Failed to incr score; FinishPlayingEpisodeScoreIncr")
			return err
		}
	}

	if !finished && err == nil {
		err = pe.userActionService.Delete(username, episode.ID, model.FinishedPlaying)
		if err != nil {
			log.WithError(err).
				WithField("episode-slug", episode.Slug).
				Error("Failed to record `finished playing` action")
			return err
		}
	}

	return nil
}

type GetEpisodeProgressPayload struct {
	Params struct {
		Slug     string
		Username string
	}
}

func (pe *PlaybackEndpoints) getEpisodeProgress(payload GetEpisodeProgressPayload) whttp.Response {
	log.Info("Get Episode Progress:", payload.Params)

	episode, err := pe.episodeRepository.FindBySlug(model.EpisodeSlug(payload.Params.Slug))
	if err != nil {
		log.WithError(err).
			WithField("episode-slug", payload.Params.Slug).
			Error("failed to get episode progress; episode not found")
		return whttp.ServerError(err)
	}

	playbackProgress, err := pe.repository.FindLatest(episode.ID, payload.Params.Username)
	if err != nil {
		log.Error(err)
		return whttp.ServerError(err)
	}

	return whttp.Ok(playbackProgress)
}

type GetLastPlayedEpisodePayload struct {
	Params struct {
		Username string
	}
}

func (pe *PlaybackEndpoints) getLastPlayedEpisode(payload GetLastPlayedEpisodePayload) whttp.Response {
	latestPlayback, err := pe.repository.FindLatestForUser(payload.Params.Username)
	if err != nil {
		log.WithError(err).
			WithField("username", payload.Params.Username).
			Error("failed to get last played episode; latest playback not found")
		return whttp.ServerError(err)
	}

	ep, err := pe.episodeRepository.FindByID(latestPlayback.EpisodeID)
	if err != nil {
		log.WithError(err).
			WithField("username", payload.Params.Username).
			Error("failed to get last played episode; failed to get ep")
		return whttp.ServerError(err)
	}

	e, err := pe.episodeSvc.GetEpisode(payload.Params.Username, ep.Slug)
	if err != nil {
		log.WithError(err).
			WithField("username", payload.Params.Username).
			Error("failed to get last played episode; failed to get episode")
		return whttp.ServerError(err)
	}

	return whttp.Ok(e)
}
