package endpoints

import (
	"strconv"

	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
)

type ShowEndpoints struct {
	showRepository model.ShowRepository
	business.UserActionService
	showService  business.ShowService
	scoreService business.ScoreService
}

func NewShowEndpoints(showRepository model.ShowRepository,
	userActionService business.UserActionService,
	showService business.ShowService,
	scoreService business.ScoreService) *ShowEndpoints {
	return &ShowEndpoints{
		showRepository,
		userActionService,
		showService,
		scoreService,
	}
}

func (e ShowEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Get Show",
			Pattern: "/show/{slug}",
			Method:  "GET",
			Handler: e.getShow,
		},
		{
			Name:    "Get Show Episodes",
			Pattern: "/show/{slug}/episodes",
			Method:  "GET",
			Handler: e.getShowEpisodes,
		},
		{
			Name:    "Follow Show",
			Pattern: "/show/{slug}/follow",
			Method:  "POST",
			Handler: e.followShow,
		},
		{
			Name:    "Unfollow Show",
			Pattern: "/show/{slug}/follow",
			Method:  "DELETE",
			Handler: e.unFollowShow,
		},
		{
			Name:    "Get Shows user follows",
			Pattern: "/following/shows",
			Method:  "GET",
			Handler: e.getShowsUserFollows,
		},
	}
}

type GetShowPayload struct {
	Params struct {
		Slug     string
		Username string
		Limit    string
		Offset   string
	}
}

func (e *ShowEndpoints) getShow(payload GetShowPayload) whttp.Response {
	show, err := e.showService.GetShow(payload.Params.Username, payload.Params.Slug)
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok(show)
}

func (e *ShowEndpoints) getShowEpisodes(payload GetShowPayload) whttp.Response {
	offset, err := strconv.ParseInt(payload.Params.Offset, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	limit, err := strconv.ParseInt(payload.Params.Limit, 10, 64)
	if err != nil {
		return whttp.ServerError(err)
	}

	showEpisodes, err := e.showService.GetShowEpisodes(payload.Params.Username, payload.Params.Slug, model.Page{
		Offset: offset,
		Limit:  limit,
	})

	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok(showEpisodes)
}

type FollowShowPayload struct {
	Params struct {
		Slug     string
		Username string
	}
}

func (e *ShowEndpoints) followShow(payload FollowShowPayload) whttp.Response {

	show, err := e.showRepository.FindBySlug(payload.Params.Slug)
	if err != nil {
		return whttp.ServerError(err)
	}

	err = e.UserActionService.Insert(model.UserAction{
		Username:   payload.Params.Username,
		ObjectId:   show.ID,
		ActionType: model.FollowedShow,
	})
	if err != nil {
		return whttp.ServerError(err)
	}

	err = e.scoreService.IncrShowScore(show.ID, model.FollowShowScoreIncr)
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}

func (e *ShowEndpoints) unFollowShow(payload FollowShowPayload) whttp.Response {

	show, err := e.showRepository.FindBySlug(payload.Params.Slug)
	if err != nil {
		return whttp.ServerError(err)
	}

	err = e.UserActionService.Delete(payload.Params.Username, show.ID, model.FollowedShow)
	if err != nil {
		return whttp.ServerError(err)
	}

	return whttp.Ok("")
}

func (e *ShowEndpoints) getShowsUserFollows(payload FollowShowPayload) whttp.Response {
	userActions, err := e.UserActionService.FindAllByUsernameAndType(payload.Params.Username, model.FollowedShow)
	if err != nil {
		return whttp.ServerError(err)
	}

	var shows []model.Show
	for _, action := range userActions {
		show, err := e.showRepository.FindByID(action.ObjectId)
		if err != nil {

		}

		shows = append(shows, show)
	}

	return whttp.Ok(shows)
}
