package endpoints

import (
	"strconv"

	"weezr.me/weezr/episode"

	"weezr.me/weezr/feed/feedsvc"

	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/model"
)

type FeedEndpoints struct {
	feedService feedsvc.FeedService
	episodeSvc  episode.Service
}

func NewFeedEndpoints(feedObjectService feedsvc.FeedService, episodeSvc episode.Service) *FeedEndpoints {
	return &FeedEndpoints{feedObjectService, episodeSvc}
}

func (e FeedEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return []whttp.EndpointDefinition{
		{
			Name:    "Feed",
			Pattern: "/feed",
			Method:  "GET",
			Handler: e.assembleFeed,
		},
		{
			Name:    "Feed Banner",
			Pattern: "/feed/banner",
			Method:  "GET",
			Handler: e.getBanner,
		},
		{
			Name:    "Feed - Latest from Following",
			Pattern: "/feed/latest-from-following",
			Method:  "GET",
			Handler: e.latestFromFollowing,
		},
		{
			Name:    "Feed - Continue to Listen",
			Pattern: "/feed/continue-to-listen",
			Method:  "GET",
			Handler: e.continueToListen,
		},
	}
}

type GetFeedPayload struct {
	Params struct {
		Username string
		Language string
		Limit    string
		Offset   string
	}
}

func (e *FeedEndpoints) assembleFeed(payload GetFeedPayload) whttp.Response {
	limit, err := strconv.ParseInt(payload.Params.Limit, 10, 64)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": payload.Params.Username}).Error("failed to fetch feed; failed to parse limit")
		return whttp.ServerError(err)
	}

	offset, err := strconv.ParseInt(payload.Params.Offset, 10, 64)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": payload.Params.Username}).Error("failed to fetch feed; failed to parse offset")
		return whttp.ServerError(err)
	}

	var lang = model.GetLanguage(payload.Params.Language)

	feedPage, err := e.feedService.AssembleFeed(payload.Params.Username, lang, model.Page{Limit: limit, Offset: offset})
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": payload.Params.Username}).Error("failed to fetch feed; failed to assemble feed")
		return whttp.ServerError(err)
	}

	return whttp.Ok(feedPage)
}

func (e *FeedEndpoints) getBanner() whttp.Response {
	bannerPromos, err := e.feedService.FetchFeedPromotions("", model.HomeBannerPlacement, 5)
	if err != nil {
		logrus.WithError(err).Error("failed to fetch banner promos")
		return whttp.ServerError(err)
	}

	return whttp.Ok(bannerPromos)
}

type LatestFromFollowingPayload struct {
	Params struct {
		Username string
	}
}

func (e *FeedEndpoints) latestFromFollowing(payload LatestFromFollowingPayload) whttp.Response {
	logrus.WithField("username", payload.Params.Username).Info("fetching latest episodes from shows user follows")

	episodes, err := e.episodeSvc.FindLatestFromShowsUserFollows(payload.Params.Username)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": payload.Params.Username}).Error("failed to fetch latest episodes from shows user follows")
		return whttp.ServerError(err)
	}

	return whttp.Ok(episodes)
}

func (e *FeedEndpoints) continueToListen(payload LatestFromFollowingPayload) whttp.Response {
	logrus.WithField("username", payload.Params.Username).Info("fetching in progress episodes for user")

	episodes, err := e.episodeSvc.FindInProgressEpisodes(payload.Params.Username)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": payload.Params.Username}).Error("failed to fetch in progress episodes")
		return whttp.ServerError(err)
	}

	return whttp.Ok(episodes)
}
