package endpoints_test

import (
	"fmt"
	"testing"

	"weezr.me/weezr/test"

	"weezr.me/weezr/api/application"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
)

func TestShareEndpoints(t *testing.T) {
	app := application.New()

	app.Container.Invoke(func(repository model.EpisodeRepository, showRepository model.ShowRepository) int {
		assertions := assert.New(t)
		router := app.Router

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		assertions.Nil(err)

		episode, err := test.InsertDefaultEpisode(repository, show)
		assertions.Nil(err)

		shareResponse := exec(router, post(fmt.Sprintf("/share/%s", episode.Slug), nil))
		assertions.Equal(200, shareResponse.Code)

		episode, err = repository.FindByID(episode.ID)
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(int64(model.ShareEpisodeScoreIncr), episode.Score)

		return 0
	})

}
