package endpoints

import (
	"database/sql"
	"strconv"
	"time"

	"github.com/go-redis/redis/v7"

	"weezr.me/weezr/scavenger/link"

	"github.com/sirupsen/logrus"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/model"
)

type ScavengeLinkEndpoints struct {
	scavengeLinkRepository model.ScavengeLinkRepository
	scavengeLinkQueue      link.Queue
	redisClient            *redis.Client
}

func NewScavengeLinkEndpoints(scavengeLinkRepository model.ScavengeLinkRepository, scavengeLinkQueue link.Queue, redisClient *redis.Client) *ScavengeLinkEndpoints {
	return &ScavengeLinkEndpoints{scavengeLinkRepository, scavengeLinkQueue, redisClient}
}

func (e ScavengeLinkEndpoints) EndpointDefinitions() []whttp.EndpointDefinition {
	return whttp.EndpointDefinitions{
		{
			Name:    "Puts a link to scavenge",
			Pattern: "/scavenge",
			Method:  "PUT",
			Handler: e.scavengeLink,
		},
	}
}

type ScavengeLinkPayload struct {
	Body model.ScavengeLink
}

func (e ScavengeLinkEndpoints) scavengeLink(payload ScavengeLinkPayload) whttp.Response {
	persisted, err := e.scavengeLinkRepository.Insert(model.ScavengeLink{
		SourceID:   payload.Body.SourceID,
		SourceType: payload.Body.SourceType,
		Link:       payload.Body.Link,
		Status:     model.NewScavengeLink,
		CreatedAt:  time.Now().UTC(),
		UpdatedAt:  time.Now().UTC(),
	})

	if err != nil {
		if err == sql.ErrNoRows {
			return whttp.Ok(payload)
		}

		logrus.WithError(err).
			WithFields(logrus.Fields{
				"source_type": payload.Body.SourceType,
				"source_id":   payload.Body.SourceID,
				"link":        payload.Body.Link,
			}).
			Errorf("failed to put link to scavenge")

		return whttp.ServerError(err)
	}

	logrus.Infof("publishing %d", persisted.ID)
	e.scavengeLinkQueue.Publish(strconv.FormatInt(persisted.ID, 10))

	return whttp.Ok(persisted)
}
