package endpoints_test

import (
	"fmt"
	"testing"

	"github.com/icrowley/fake"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/api/application"
	"weezr.me/weezr/model"
)

func TestScavengeLink(t *testing.T) {
	app := application.New()
	assertions := assert.New(t)

	app.Container.Invoke(func() int {
		router := app.Router

		link := model.ScavengeLink{
			SourceID:   fake.Word(),
			SourceType: "APPLE",
			Link:       fmt.Sprintf("https://podcasts.apple.com/us/podcast/command-line-heroes/%s", fake.Word()),
		}

		// when I insert a link, I check it was correctly inserted
		insertLinkResponse := exec(router, put("/scavenge", link, &model.User{ID: fake.Word()}))
		parse(insertLinkResponse, &link)
		assertions.Equal(200, insertLinkResponse.Code)
		assertions.Greater(link.ID, int64(0))
		assertions.Equal(link.Status, model.NewScavengeLink)

		// when I insert the same link, I check nothing has changed
		var link2 model.ScavengeLink
		insertLinkResponse2 := exec(router, put("/scavenge", link, &model.User{ID: fake.Word()}))
		parse(insertLinkResponse2, &link2)
		assertions.Equal(200, insertLinkResponse.Code)
		assertions.Greater(link.ID, link2.ID)
		assertions.Equal(link.Status, model.NewScavengeLink)
		assertions.Greater(link.UpdatedAt.Unix(), link2.UpdatedAt.Unix())

		return 0
	})
}
