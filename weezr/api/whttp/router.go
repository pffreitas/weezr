package whttp

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/gorilla/mux"
	"github.com/robbert229/jwt"
	log "github.com/sirupsen/logrus"
	"weezr.me/weezr/commons"
	"weezr.me/weezr/model"
)

func NewRouter(endpoints []EndpointProvider, config model.WeezrConfig) *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	for _, e := range endpoints {
		fmt.Printf("- Registering %v \n", reflect.TypeOf(e).Elem().Name())
		for _, ed := range e.EndpointDefinitions() {
			fmt.Printf("\t %v \n", ed)
			router.
				Methods("OPTIONS", ed.Method).
				Path(ed.Pattern).
				Name(ed.Name).
				Handler(CreateHandler(ed.Handler, config))
		}
	}

	return router
}

func readRequestParams(r *http.Request) map[string]string {
	params := map[string]string{}
	for k, v := range mux.Vars(r) {
		params[k] = v
	}

	for k, v := range r.URL.Query() {
		params[k] = v[0]
	}

	return params
}

func getHandlerFuncParamTypes(handlerFunc interface{}) []reflect.Type {
	handlerFuncType := reflect.TypeOf(handlerFunc)
	numArgs := handlerFuncType.NumIn()

	handlerFuncParamTypes := make([]reflect.Type, 0, numArgs)
	for i := 0; i < numArgs; i++ {
		p := handlerFuncType.In(i)
		handlerFuncParamTypes = append(handlerFuncParamTypes, p)
	}

	return handlerFuncParamTypes
}

func parseBody(payload reflect.Value, r *http.Request) {
	bodyField := payload.FieldByName("Body")

	if bodyField.IsValid() {
		body := reflect.New(bodyField.Type()).Interface()

		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		err := decoder.Decode(body)
		commons.CheckError(err, "failed to parse request payload")
		bodyField.Set(reflect.ValueOf(body).Elem())
	}
}

func parseParams(payload reflect.Value, user model.User, params map[string]string) {
	params["Username"] = user.ID

	paramsField := payload.FieldByName("Params")

	if paramsField.IsValid() {
		paramsStruct := reflect.New(paramsField.Type()).Elem()

		for k, v := range params {
			f := paramsStruct.FieldByName(strings.Title(k))
			if f.IsValid() {
				f.Set(reflect.ValueOf(v))
			}
		}

		paramsField.Set(paramsStruct)
	}
}

func auth(request *http.Request, config model.WeezrConfig) (model.User, error) {
	// if endpoint is protected
	testAuthorizationHeader := request.Header["Authorization-Test"]
	if len(testAuthorizationHeader) == 1 {
		var user model.User
		json.Unmarshal([]byte(testAuthorizationHeader[0]), &user)
		return user, nil
	}

	authorizationHeader := request.Header["Authorization"]
	if len(authorizationHeader) == 1 {
		authToken := authorizationHeader[0]

		if strings.HasPrefix(authToken, "Bearer ") {
			jwtToken := strings.Replace(authToken, "Bearer ", "", 1)

			hs256 := jwt.HmacSha256(config.JWTSecret)

			if hs256.Validate(jwtToken) != nil {
				log.Error("Invalid JWT token", jwtToken)
				return model.User{}, fmt.Errorf("Unauthorized")
			}

			if claims, err := hs256.Decode(jwtToken); err == nil {
				if sub, err := claims.Get("sub"); err == nil {
					return model.User{ID: fmt.Sprintf("%v", sub)}, nil
				}
			}
		}
	}

	return model.User{}, fmt.Errorf("Failed to process authorization: %v", authorizationHeader)
}

// CreateHandler .
func CreateHandler(handlerFunc interface{}, config model.WeezrConfig) http.HandlerFunc {
	return func(w http.ResponseWriter, request *http.Request) {
		defer commons.HandleError(w)

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Headers", "Authorization,Content-Type,*")
		w.Header().Set("Access-Control-Allow-Methods", "*")

		if "OPTIONS" == request.Method {
			commons.WriteJSON(w, "", 200)
			return
		}

		user, err := auth(request, config)
		if err != nil {
			commons.WriteJSON(w, "Unauthorized", 401)
			return
		}

		handlerFuncParamTypes := getHandlerFuncParamTypes(handlerFunc)
		handlerFuncArgs := make([]reflect.Value, 0, len(handlerFuncParamTypes))

		if len(handlerFuncParamTypes) > 0 {
			payload := reflect.New(handlerFuncParamTypes[0]).Elem()
			parseBody(payload, request)
			parseParams(payload, user, readRequestParams(request))
			handlerFuncArgs = append(handlerFuncArgs, payload)
		}

		response := reflect.ValueOf(handlerFunc).Call(handlerFuncArgs)
		whttpResponse := response[0].Interface().(Response)

		commons.WriteJSON(w, whttpResponse.Body, whttpResponse.Code)
	}
}
