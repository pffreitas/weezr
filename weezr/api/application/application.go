package application

import (
	"net/http"

	"weezr.me/weezr/search/search"

	"weezr.me/weezr/episode"

	"weezr.me/weezr/feedclnt"

	"weezr.me/weezr/feed/feedsvc"

	"weezr.me/weezr/scavenger/link"

	"github.com/adjust/rmq/v2"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"weezr.me/weezr/api/endpoints"
	"weezr.me/weezr/api/whttp"
	"weezr.me/weezr/business"
	"weezr.me/weezr/db"
	"weezr.me/weezr/db/postgres"
	"weezr.me/weezr/model"
	"weezr.me/weezr/scavenger/config"
	"weezr.me/weezr/winj"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func processConfig() model.WeezrConfig {
	godotenv.Load("../.env.test", "../.env")

	var config model.WeezrConfig
	envconfig.Process("weezr", &config)

	return config
}

func processDbConfig() db.DatabaseConfig {
	godotenv.Load("../.env.test", "../.env")

	var config db.DatabaseConfig
	envconfig.Process("weezr", &config)

	return config
}

func processRmqConfig() config.RmqConfig {
	godotenv.Load("../.env.test", "../.env")

	var config config.RmqConfig
	envconfig.Process("weezr", &config)

	return config
}

func processSearchConfig() search.Config {
	godotenv.Load("../.env.test", "../.env")

	var config search.Config
	envconfig.Process("weezr", &config)

	return config
}

type Application struct {
	Name      string
	Config    model.WeezrConfig
	Router    *mux.Router
	Container *winj.Container
}

func newApplication(config model.WeezrConfig, router *mux.Router, container *winj.Container) Application {
	return Application{
		Name:      "weezr",
		Config:    config,
		Router:    router,
		Container: container,
	}
}

func New() Application {
	container := winj.New()
	container.Provide(processConfig)
	container.Provide(processDbConfig)
	container.Provide(processRmqConfig)
	container.Provide(processSearchConfig)
	container.Provide(db.GetPgSession)
	container.Provide(db.GetDbxSession)
	container.Provide(config.NewRedisClient)
	container.Provide(config.NewRmqConnection)

	container.Provide(postgres.NewChannelPostgresRepository)
	container.Provide(postgres.NewUserActionPostgresRepository)
	container.Provide(postgres.NewShowPostgresRepository)
	container.Provide(postgres.NewEpisodePostgresRepository)
	container.Provide(postgres.NewPromotionPostgresRepository)
	container.Provide(postgres.NewPlaylistPostgresRepository)
	container.Provide(postgres.NewFeedObjectPostgresRepository)
	container.Provide(postgres.NewScavengeLinkPostgresRepository)
	container.Provide(postgres.NewUserPostgresRepository)
	container.Provide(postgres.NewCommentPostgresRepository)
	container.Provide(postgres.NewPlaybackProgressPostgresRepository)

	container.Provide(search.NewSearchService)
	container.Provide(link.NewScavengeLinkQueue)
	container.Provide(feedclnt.CreateRelatedEpisodeFanOutQueue)

	container.Provide(business.CreateScoreService)
	container.Provide(business.NewUserActionService)
	container.Provide(business.NewPlaylistService)
	container.Provide(business.NewEpisodeService)
	container.Provide(episode.NewEpisodeSvc)
	container.Provide(business.NewShowService)
	container.Provide(business.NewShareService)
	container.Provide(feedsvc.NewFeedService)

	container.Provide(endpoints.NewHealthCheckEndpoints)
	container.Provide(endpoints.NewPlaylistEndpoints)
	container.Provide(endpoints.NewEpisodeEndpoints)
	container.Provide(endpoints.NewShowEndpoints)
	container.Provide(endpoints.NewFeedEndpoints)
	container.Provide(endpoints.NewPlaybackEndpoints)
	container.Provide(endpoints.NewSearchEndpoints)
	container.Provide(endpoints.NewShareEndpoints)
	container.Provide(endpoints.NewCommentsEndpoints)
	container.Provide(endpoints.NewUserEndpoints)
	container.Provide(endpoints.NewScavengeLinkEndpoints)

	container.Provide(whttp.NewRouter)

	container.Provide(func(router *mux.Router, connection rmq.Connection) int {
		router.HandleFunc("/rmq-stats", func(w http.ResponseWriter, r *http.Request) {

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(200)
			s := connection.CollectStats(connection.GetOpenQueues())

			_, err := w.Write([]byte(s.GetHtml("", "1")))
			if err != nil {
				w.WriteHeader(500)
			}
		})
		return 0
	})

	container.Provide(func(router *mux.Router) int {
		router.Handle("/metrics", promhttp.Handler())
		return 0
	})

	return container.Invoke(newApplication).(Application)

}
