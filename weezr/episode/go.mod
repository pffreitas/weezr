module weezr.me/weezr/episode

go 1.13

require (
	github.com/prometheus/common v0.11.1
	weezr.me/weezr/business v0.0.0
	weezr.me/weezr/db v0.0.0 // indirect
	weezr.me/weezr/model v0.0.0
	weezr.me/weezr/test v0.0.0 // indirect
)

replace (
	github.com/adjust/rmq/v2 => github.com/pffreitas/rmq/v2 v2.0.0
	weezr.me/weezr/business => ./../business
	weezr.me/weezr/db => ./../db
	weezr.me/weezr/feedclnt => ../feed/feedclnt
	weezr.me/weezr/model => ./../model
	weezr.me/weezr/test => ./../test
)
