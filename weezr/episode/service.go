package episode

import (
	"time"

	"weezr.me/weezr/feedclnt"

	"github.com/sirupsen/logrus"

	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
)

type Service struct {
	episodeRepository         model.EpisodeRepository
	showRepository            model.ShowRepository
	userActionService         business.UserActionService
	scoreService              business.ScoreService
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue
}

func NewEpisodeSvc(episodeRepository model.EpisodeRepository,
	showRepository model.ShowRepository,
	userActionService business.UserActionService,
	scoreService business.ScoreService,
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue) Service {

	return Service{episodeRepository,
		showRepository,
		userActionService,
		scoreService,
		relatedEpisodeFanOutQueue,
	}
}

func (s *Service) GetEpisode(username string, episodeSlug string) (model.Episode, error) {
	episode, err := s.episodeRepository.FindBySlug(model.EpisodeSlug(episodeSlug))
	if err != nil {
		logrus.WithError(err).WithField("episode-slug", episodeSlug).Error("failed to get episode")
		return episode, err
	}

	count, _ := s.userActionService.Count(episode.ID, model.Liked)
	episode.LikeCount = count

	_, err = s.userActionService.GetAction(username, episode.ID, model.Liked)
	episode.LikedByCurrentUser = err == nil

	show, err := s.showRepository.FindByID(episode.ShowID)
	if err != nil {
		logrus.WithError(err).WithFields(
			logrus.Fields{
				"episode-id": episode.ID,
				"show-id":    show.ID,
			}).Error("failed to get episode; failed to get show")

		return episode, err
	}

	followingCount, _ := s.userActionService.Count(show.ID, model.FollowedShow)
	episode.FollowingCount = followingCount

	_, err = s.userActionService.GetAction(username, show.ID, model.FollowedShow)
	episode.FollowedByCurrentUser = err == nil

	err = s.scoreService.IncrEpisodeScore(episode.ID, model.OpenEpisodeScoreIncr)
	if err != nil {
		logrus.WithError(err).Error("failed to get episode; failed to incr score")
		return episode, err
	}

	err = s.userActionService.Insert(model.UserAction{
		Username:   username,
		ObjectId:   episode.ID,
		ActionType: model.AccessedEpisode,
		CreatedAt:  time.Now().UTC(),
	})
	if err != nil {
		logrus.WithError(err).Error("failed to get episode; failed to record user action")
		return episode, err
	}

	err = s.relatedEpisodeFanOutQueue.Post(username, episode, 1)
	if err != nil {
		logrus.WithError(err).Error("failed to get episode; failed to post to related episode fan out queue")
		return episode, err
	}

	return episode, nil
}

func (s Service) FindLatestFromShowsUserFollows(username string) ([]model.SlimEpisode, error) {
	following, err := s.userActionService.FindAllByUsernameAndType(username, model.FollowedShow)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": username}).Error("failed to fetch shows user follows")
		return nil, err
	}

	var followingShowsId []int64
	for _, followingShow := range following {
		followingShowsId = append(followingShowsId, followingShow.ObjectId)
	}

	episodes, err := s.episodeRepository.FindLatestByShowId(followingShowsId)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": username}).Error("failed to fetch following")
		return nil, err
	}

	episodes = s.userActionService.Enrich(username, episodes)

	return episodes, nil
}

func (s Service) FindInProgressEpisodes(username string) ([]model.SlimEpisode, error) {
	episodes, err := s.episodeRepository.FindInProgressEpisodes(username)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": username}).Error("failed to fetch in progress episodes")
		return episodes, err
	}

	episodes = s.userActionService.Enrich(username, episodes)

	return episodes, nil
}
