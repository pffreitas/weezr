package feedclnt

import (
	"encoding/json"
	"fmt"

	"github.com/adjust/rmq/v2"
	"weezr.me/weezr/model"
)

const RelatedEpisodeFanOutQueueName = "related-episode-fan-out"

type RelatedEpisodeFanOutQueue interface {
	Post(username string, episode model.Episode, factor float64) error
}

type RelatedEpisodeFanOutPayload struct {
	Username  string  `json:"username"`
	EpisodeID int64   `json:"episode_id"`
	Factor    float64 `json:"factor"`
}

type relatedEpisodeFanOutRmqQueue struct {
	rmq.Queue
}

func (q relatedEpisodeFanOutRmqQueue) Post(username string, episode model.Episode, factor float64) error {
	jsonPayload, err := json.Marshal(RelatedEpisodeFanOutPayload{username, episode.ID, factor})
	if err != nil {
		return err // TODO map err
	}

	ok := q.PublishBytes(jsonPayload)
	if !ok {
		return fmt.Errorf("") // TODO map error
	}

	return nil
}

func CreateRelatedEpisodeFanOutQueue(connection rmq.Connection) RelatedEpisodeFanOutQueue {
	queue := connection.OpenQueue(RelatedEpisodeFanOutQueueName)
	return relatedEpisodeFanOutRmqQueue{queue}
}
