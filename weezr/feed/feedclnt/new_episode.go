package feedclnt

import (
	"fmt"
	"strconv"

	"github.com/adjust/rmq/v2"
	"weezr.me/weezr/model"
)

const NewEpisodeFanOutQueueName = "new-episode-fan-out"

type NewEpisodeFanOutQueue interface {
	Post(episode model.Episode) error
}

type newEpisodeFanOutRmqQueue struct {
	rmq.Queue
}

func (q newEpisodeFanOutRmqQueue) Post(episode model.Episode) error {
	ok := q.Publish(strconv.FormatInt(episode.ID, 10))
	if !ok {
		return fmt.Errorf("failed to post to new episode fan out queue: %d", episode.ID)
	}

	return nil
}

func CreateNewEpisodeFanOutQueue(connection rmq.Connection) NewEpisodeFanOutQueue {
	queue := connection.OpenQueue(NewEpisodeFanOutQueueName)
	return newEpisodeFanOutRmqQueue{queue}
}
