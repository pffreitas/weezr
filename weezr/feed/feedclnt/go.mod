module weezr.me/weezr/feedclnt

go 1.13

require (
	github.com/adjust/rmq/v2 v2.0.0
	weezr.me/weezr/model v0.0.0
)

replace (
	github.com/adjust/rmq/v2 => github.com/pffreitas/rmq/v2 v2.0.0
	weezr.me/weezr/model => ./../../model
)
