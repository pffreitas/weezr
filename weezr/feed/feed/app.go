package feed

import (
	"github.com/adjust/rmq/v2"
	"github.com/gorilla/mux"
	"github.com/pffreitas/rmqprom"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"weezr.me/weezr/business"
	"weezr.me/weezr/db"
	"weezr.me/weezr/db/postgres"
	"weezr.me/weezr/feed/feedfanout"
	"weezr.me/weezr/feed/feedsvc"
	"weezr.me/weezr/feedclnt"
	"weezr.me/weezr/search/search"

	weego "github.com/pffreitas/weego/application"
	whttp "github.com/pffreitas/weego/application/http"
)

type RmqConfig struct {
	Address  string `envconfig:"RMQ_ADDRESS"`
	Password string `envconfig:"RMQ_PASSWORD"`
}

type Application struct {
	DatabaseConfig db.DatabaseConfig
	RmqConfig      RmqConfig
	SearchConfig   search.Config
}

func NewApp() weego.WeegoApplication {
	app := weego.New(Application{})

	app.Provide(db.GetPgSession)
	app.Provide(db.GetDbxSession)
	app.Provide(NewRedisClient)
	app.Provide(NewRmqConnection)

	app.Provide(postgres.NewFeedObjectPostgresRepository)
	app.Provide(postgres.NewShowPostgresRepository)
	app.Provide(postgres.NewEpisodePostgresRepository)
	app.Provide(postgres.NewUserActionPostgresRepository)
	app.Provide(postgres.NewPromotionPostgresRepository)
	app.Provide(postgres.NewPlaybackProgressPostgresRepository)
	app.Provide(postgres.NewPlaylistPostgresRepository)

	app.Provide(search.NewSearchService)

	app.Provide(feedclnt.CreateRelatedEpisodeFanOutQueue)
	app.Provide(feedclnt.CreateNewEpisodeFanOutQueue)

	app.Provide(business.CreateScoreService)
	app.Provide(business.NewUserActionService)
	app.Provide(business.NewShowService)
	app.Provide(business.NewPlaylistService)
	app.Provide(feedsvc.NewFeedService)

	app.Provide(feedfanout.CreateNewEpisodeFanOut)
	app.Provide(feedfanout.CreateNewEpisodeFanOutConsumer)

	app.Provide(feedfanout.CreateRelatedEpisodeFanOut)
	app.Provide(feedfanout.CreateRelatedEpisodeFanOutConsumer)

	app.Provide(whttp.NewRouter)

	app.Provide(func(router *mux.Router, connection rmq.Connection) int {
		rmqprom.RecordRmqMetrics(connection)

		router.Handle("/metrics", promhttp.Handler())
		return 0
	})

	return app
}
