package feedfanout

import (
	"encoding/json"
	"math"
	"time"

	"weezr.me/weezr/feed/feedsvc"

	"weezr.me/weezr/feedclnt"

	"weezr.me/weezr/search/search"

	"github.com/adjust/rmq/v2"
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

type RelatedEpisodeFanOutConsumer struct {
	RelatedEpisodeFanOut
}

func (c RelatedEpisodeFanOutConsumer) Consume(delivery rmq.Delivery) {
	logrus.Info(delivery.Payload())
	var payload feedclnt.RelatedEpisodeFanOutPayload
	err := json.Unmarshal([]byte(delivery.Payload()), &payload)
	if err != nil {
		logrus.WithError(err).WithField("payload", delivery.Payload()).Error("failed to fan out episode; failed to parse payload")
		delivery.Reject()
		return
	}

	err = c.fanOut(payload)
	if err != nil {
		logrus.WithError(err).WithField("episode-id", delivery.Payload()).Error("failed to fan out episode")
		delivery.Reject()
		return
	}

	delivery.Ack()
}

func CreateRelatedEpisodeFanOutConsumer(relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue, relatedEpisodeFanOut RelatedEpisodeFanOut) RelatedEpisodeFanOutConsumer {
	consumer := RelatedEpisodeFanOutConsumer{relatedEpisodeFanOut}

	if rmqQueue, ok := relatedEpisodeFanOutQueue.(rmq.Queue); ok {
		rmqQueue.StartConsuming(10, 5*time.Second)
		rmqQueue.AddConsumer(feedclnt.RelatedEpisodeFanOutQueueName, consumer)
	}

	return consumer
}

type RelatedEpisodeFanOut struct {
	episodeRepository model.EpisodeRepository
	feedObjectService feedsvc.FeedService
	searchService     search.SearchService
}

func (f RelatedEpisodeFanOut) fanOut(payload feedclnt.RelatedEpisodeFanOutPayload) error {
	episodeID := payload.EpisodeID
	username := payload.Username
	factor := payload.Factor

	logrus.WithField("episode-id", episodeID).Info("fanning out related episodes")
	episode, err := f.episodeRepository.FindByID(episodeID)
	if err != nil {
		logrus.WithError(err).
			WithField("episode-id", episodeID).
			Error("failed to fan out related episode; failed to find episode")

		return err
	}

	relatedEpisodes, err := f.findRelatedEpisodes(episode)
	if err != nil {
		logrus.WithError(err).
			WithField("episode-id", episodeID).
			Error("failed to fan out related episode; failed to find related episodes")

		return err
	}

	for _, relatedEpisode := range relatedEpisodes {
		score := float64(relatedEpisode.Score) * factor
		err = f.feedObjectService.PutEpisode(username, relatedEpisode, int64(math.Round(score)))
		if err != nil {
			return err
		}
	}

	return nil
}

func (f RelatedEpisodeFanOut) findRelatedEpisodes(episode model.Episode) ([]model.Episode, error) {
	var related []model.Episode

	response, err := f.searchService.SearchEpisodes(episode.TidyContent, model.Page{Limit: 3})
	if err != nil {
		logrus.WithError(err).
			WithField("episode-id", episode.ID).
			Error("Failed to find related episodes on related episode fan out")

		return related, err
	}

	for _, r := range response {
		episode, err := f.episodeRepository.FindByID(r.ID)
		if err == nil {
			related = append(related, episode)
		}
	}

	return related, nil
}

func CreateRelatedEpisodeFanOut(
	episodeRepository model.EpisodeRepository,
	feedObjectService feedsvc.FeedService,
	searchService search.SearchService) RelatedEpisodeFanOut {

	return RelatedEpisodeFanOut{
		episodeRepository,
		feedObjectService,
		searchService,
	}
}
