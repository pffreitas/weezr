package feedfanout

import (
	"strconv"
	"time"

	"weezr.me/weezr/feedclnt"

	"weezr.me/weezr/feed/feedsvc"

	"github.com/adjust/rmq/v2"
	"github.com/sirupsen/logrus"

	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
)

type NewEpisodeFanOutConsumer struct {
	NewEpisodeFanOut
}

func (c NewEpisodeFanOutConsumer) Consume(delivery rmq.Delivery) {
	episodeId, err := strconv.ParseInt(delivery.Payload(), 10, 64)
	if err != nil {
		logrus.WithError(err).WithField("episode-id", delivery.Payload()).Error("failed to fan out new episode; failed to parse episode id")
		delivery.Reject()
		return
	}

	err = c.fanOut(episodeId)
	if err != nil {
		logrus.WithError(err).WithField("episode-id", delivery.Payload()).Error("failed to fan out new episode")
		delivery.Reject()
		return
	}

	delivery.Ack()
}

func CreateNewEpisodeFanOutConsumer(newEpisodeFanOutQueue feedclnt.NewEpisodeFanOutQueue, newEpisodeFanOut NewEpisodeFanOut) NewEpisodeFanOutConsumer {
	consumer := NewEpisodeFanOutConsumer{newEpisodeFanOut}

	if rmqQueue, ok := newEpisodeFanOutQueue.(rmq.Queue); ok {
		rmqQueue.StartConsuming(10, 5*time.Second)
		for i := 0; i < 30; i++ {
			rmqQueue.AddConsumer(feedclnt.NewEpisodeFanOutQueueName, consumer)
		}
	}

	return consumer
}

type NewEpisodeFanOut struct {
	episodeRepository model.EpisodeRepository
	showRepository    model.ShowRepository
	userActionService business.UserActionService
	feedObjectService feedsvc.FeedService
}

func (f NewEpisodeFanOut) fanOut(episodeId int64) error {
	episode, err := f.episodeRepository.FindByID(episodeId)
	if err != nil {
		return err
	}

	show, err := f.showRepository.FindByID(episode.ShowID)
	if err != nil {
		return err
	}

	err = f.feedObjectService.PutEpisode("", episode, show.Score)
	if err != nil {
		return err
	}

	allFollowers, err := f.userActionService.FindAllByObjectIdAndType(episode.ShowID, model.FollowedShow)
	if err != nil {
		return err
	}

	for _, follower := range allFollowers {
		err = f.feedObjectService.PutEpisode(follower.Username, episode, show.Score)
		if err != nil {
			return err
		}
	}

	return nil
}

func CreateNewEpisodeFanOut(
	episodeRepository model.EpisodeRepository,
	showRepository model.ShowRepository,
	userActionService business.UserActionService,
	feedObjectService feedsvc.FeedService) NewEpisodeFanOut {

	return NewEpisodeFanOut{
		episodeRepository,
		showRepository,
		userActionService,
		feedObjectService,
	}
}
