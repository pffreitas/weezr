package feedfanout_test

import (
	"testing"

	"weezr.me/weezr/feedclnt"

	"weezr.me/weezr/feed"
	"weezr.me/weezr/feed/feedfanout"

	"github.com/icrowley/fake"

	"weezr.me/weezr/model"
	"weezr.me/weezr/test"

	"github.com/stretchr/testify/assert"

	"github.com/adjust/rmq/v2"
)

func TestRelatedEpisodeFanOut(t *testing.T) {
	assertions := assert.New(t)

	app := feed.NewApp()

	app.Invoke(func(
		episodeRepository model.EpisodeRepository,
		showRepository model.ShowRepository,
		relatedEpisodeFanOutConsumer feedfanout.RelatedEpisodeFanOutConsumer,
		feedObjectRepository model.FeedEpisodeRepository) int {

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Fatal(err)
		}

		episode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Fatal(err)
		}

		payload := feedclnt.RelatedEpisodeFanOutPayload{Username: fake.UserName(), EpisodeID: episode.ID, Factor: 1}
		delivery := rmq.NewTestDelivery(payload)

		relatedEpisodeFanOutConsumer.Consume(delivery)
		assertions.Equal(rmq.Acked, delivery.State)

		feedObjects, err := feedObjectRepository.Fetch(payload.Username, model.Portuguese, model.Page{Offset: 0, Limit: 10})
		if err != nil {
			t.Fatal(err)
		}

		assertions.Greater(len(feedObjects), 0)

		var episodeFeedObject *model.SlimEpisode
		for _, fo := range feedObjects {
			if episode.ID == fo.ID {
				episodeFeedObject = &fo
				break
			}
		}

		assertions.NotNil(episodeFeedObject)

		return 0
	})
}
