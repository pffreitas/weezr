package feedfanout_test

import (
	"strconv"
	"testing"

	"weezr.me/weezr/feed"
	"weezr.me/weezr/feed/feedfanout"

	"weezr.me/weezr/model"
	"weezr.me/weezr/test"

	"github.com/stretchr/testify/assert"

	"github.com/adjust/rmq/v2"
)

func TestNewEpisodeFanOut(t *testing.T) {
	assertions := assert.New(t)

	app := feed.NewApp()

	app.Invoke(func(
		episodeRepository model.EpisodeRepository,
		showRepository model.ShowRepository,
		newEpisodeFanOutConsumer feedfanout.NewEpisodeFanOutConsumer,
		feedObjectRepository model.FeedEpisodeRepository) int {

		show, err := test.InsertShow(showRepository, test.DefaultShow())
		if err != nil {
			t.Error(err)
		}

		episode, err := test.InsertDefaultEpisode(episodeRepository, show)
		if err != nil {
			t.Error(err)
		}

		delivery := rmq.NewTestDeliveryString(strconv.FormatInt(episode.ID, 10))

		newEpisodeFanOutConsumer.Consume(delivery)

		assertions.Equal(rmq.Acked, delivery.State)

		feedObjects, err := feedObjectRepository.Fetch("", model.Portuguese, model.Page{Offset: 0, Limit: 10})
		if err != nil {
			t.Error(err)
		}

		assertions.Greater(len(feedObjects), 0)

		var episodeFeedObject *model.SlimEpisode
		for _, fo := range feedObjects {
			if episode.ID == fo.ID {
				episodeFeedObject = &fo
				break
			}
		}

		assertions.NotNil(episodeFeedObject)

		return 0
	})
}
