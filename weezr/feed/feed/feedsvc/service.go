package feedsvc

import (
	"strconv"

	"github.com/sirupsen/logrus"

	"weezr.me/weezr/business"
	"weezr.me/weezr/model"
)

type FeedService struct {
	feedObjectRepository model.FeedEpisodeRepository
	showService          business.ShowService
	promotionRepository  model.PromotionRepository
	episodeRepository    model.EpisodeRepository
	playlistService      business.PlaylistService
	userActionService    business.UserActionService
}

func NewFeedService(
	feedObjectRepository model.FeedEpisodeRepository,
	showService business.ShowService,
	promotionRepository model.PromotionRepository,
	episodeRepository model.EpisodeRepository,
	playlistService business.PlaylistService,
	userActionService business.UserActionService) FeedService {

	return FeedService{
		feedObjectRepository,
		showService,
		promotionRepository,
		episodeRepository,
		playlistService,
		userActionService,
	}
}

func (s *FeedService) PutEpisode(username string, episode model.Episode, score int64) error {
	err := s.feedObjectRepository.Insert(model.NewEpisodeFeedObject(episode, username, score))
	if err != nil {
		return err
	}

	return nil
}

func (s *FeedService) AssembleFeed(username string, language model.Language, page model.Page) (model.FeedPage, error) {
	feedEpisodes, err := s.FetchFeedEpisodes(username, language, page)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": username}).Error("failed to fetch feed episodes")
		return model.FeedPage{}, err
	}

	promotions, err := s.FetchFeedPromotions(username, model.FeedPlacement, 2)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"username": username}).Error("failed to fetch feed promotions")
		return model.FeedPage{}, err
	}

	return model.FeedPage{Episodes: feedEpisodes, Promotions: promotions}, nil
}

func (s *FeedService) FetchFeedEpisodes(username string, language model.Language, page model.Page) ([]model.SlimEpisode, error) {
	feedEpisodes, err := s.feedObjectRepository.Fetch(username, language, page)
	feedEpisodes = s.userActionService.Enrich(username, feedEpisodes)
	return feedEpisodes, err
}

func (s *FeedService) FetchFeedPromotions(username string, placementType model.PlacementType, limit int64) ([]model.FeedPromotion, error) {
	promotions, err := s.promotionRepository.FindPromotions(placementType, limit)
	if err != nil {
		return nil, err
	}

	for i := range promotions {
		err, episodeList := s.getEpisodeList(username, promotions[i])
		if err == nil {
			promotions[i].EpisodeList = episodeList
		}
	}

	err = s.promotionRepository.IncrImpression(promotions)
	if err != nil {
		return nil, err
	}

	return promotions, nil
}

func (s *FeedService) getEpisodeList(username string, reader model.FeedObjectReferenceReader) (error, model.EpisodeList) {
	ref := reader.Get()

	switch ref.ObjectType {
	case model.ShowFeedObjectType:
		return s.getShowEpisodeList(username, ref.ObjectID)
	case model.PlaylistFeedObjectType:
		return s.getPlaylistEpisodeList(username, ref.ObjectID)
	default:
		return nil, model.EpisodeList{}
	}
}

func (s *FeedService) getShowEpisodeList(username string, showID int64) (error, model.EpisodeList) {
	show, err := s.showService.GetShowByID(showID)
	if err != nil {
		return err, model.EpisodeList{}
	}

	episodes, err := s.showService.GetShowEpisodes(username, show.Slug, model.Page{Offset: 0, Limit: 10})
	if err != nil {
		return err, model.EpisodeList{}
	}

	return nil, model.EpisodeList{
		ID:          show.ID,
		Slug:        show.Slug,
		Name:        show.Name,
		Description: show.Description,
		ImageKey:    show.ImageKey,
		Episodes:    episodes,
	}
}

func (s *FeedService) getPlaylistEpisodeList(username string, playlistID int64) (error, model.EpisodeList) {
	playlist, err := s.playlistService.GetPlaylist(username, strconv.FormatInt(playlistID, 10))
	if err != nil {
		return err, model.EpisodeList{}
	}

	return nil, model.EpisodeList{
		ID:       playlist.ID,
		Name:     playlist.Title,
		Episodes: playlist.Items,
	}
}
