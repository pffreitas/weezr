package main

import (
	"fmt"
	"log"
	"net/http"

	"weezr.me/weezr/feed"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.Info("Starting Feed App...")
	feedApp := feed.NewApp()

	feedApp.Invoke(func(router *mux.Router) int {
		logrus.Info("Running Feed App")
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", "8000"), router))
		return 0
	})
}
