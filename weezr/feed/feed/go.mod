module weezr.me/weezr/feed

go 1.13

require (
	github.com/adjust/rmq/v2 v2.0.0
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gorilla/mux v1.7.4
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/pffreitas/rmqprom v0.0.5
	github.com/pffreitas/weego v0.0.0-20200122023400-9fb39e754bde
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.27.0
	weezr.me/weezr/business v0.0.0
	weezr.me/weezr/db v0.0.0
	weezr.me/weezr/feedclnt v0.0.0
	weezr.me/weezr/model v0.0.0
	weezr.me/weezr/search v0.0.0
	weezr.me/weezr/test v0.0.0
)

replace (
	github.com/adjust/rmq/v2 => github.com/pffreitas/rmq/v2 v2.0.0
	weezr.me/weezr/business => ./../../business
	weezr.me/weezr/db => ./../../db
	weezr.me/weezr/feedclnt => ./../feedclnt
	weezr.me/weezr/model => ./../../model
	weezr.me/weezr/search => ./../../search
	weezr.me/weezr/test => ./../../test
)
