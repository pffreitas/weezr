package feed

import (
	"crypto/tls"
	"os"

	"github.com/icrowley/fake"

	"github.com/adjust/rmq/v2"

	"github.com/go-redis/redis/v7"
)

func NewRedisClient(redisConfig RmqConfig) *redis.Client {
	return redis.NewClient(&redis.Options{
		Network:  "tcp",
		Addr:     redisConfig.Address,
		Password: redisConfig.Password,
		DB:       0,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	})
}

func NewRmqConnection(redisClient *redis.Client) rmq.Connection {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = fake.Word()
	}

	return rmq.OpenConnectionWithRedisClient(hostname, redisClient)
}
