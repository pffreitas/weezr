package model

type User struct {
	ID         string `json:"id"`
	Username   string `json:"username"`
	Name       string `json:"name"`
	Nickname   string `json:"nickname"`
	GivenName  string `json:"givenName"`
	FamilyName string `json:"familyName"`
	Email      string `json:"email"`
	LastIP     string `json:"lastIP"`
	Picture    string `json:"picture"`
	Locale     string `json:"locale"`
}

type UserRepository interface {
	Save(user User) error
	FindByID(ID string) (User, error)
}
