package model

import (
	"time"
)

type ObjectId int64

type ActionType int

const (
	Liked ActionType = iota
	Played
	FinishedPlaying
	FollowedShow
	Shared
	AccessedEpisode
	StartedSession
)

type UserAction struct {
	Username   string `json:"username"`
	ObjectId   int64  `json:"objectId"`
	ActionType `json:"actionType"`
	CreatedAt  time.Time  `json:"createdAt"`
	DeletedAt  *time.Time `json:"deletedAt"`
}

type UserActionCount struct {
	ObjectId   `json:"objectId"`
	ActionType `json:"actionType"`
	Count      int64 `json:"count"`
}

type UserActionRepository interface {
	Insert(action UserAction) error
	Get(username string, objectId int64, actionType ActionType) (UserAction, error)
	FindAllByObjectIdAndType(objectId int64, actionType ActionType) ([]UserAction, error)
	FindAllByUsernameAndType(username string, actionType ActionType) ([]UserAction, error)
	FindAllByUsernameAndTypeAndObjectIDIn(username string, actionType ActionType, objectIDs []interface{}) ([]UserAction, error)
	FindByType(username string, actionType ActionType, page Page) ([]UserAction, error)
	Delete(username string, objectId int64, actionType ActionType) error
	Count(objectId int64, actionType ActionType) (int64, error)
	IncrCount(objectId int64, actionType ActionType) error
	DecrCount(objectId int64, actionType ActionType) error
}
