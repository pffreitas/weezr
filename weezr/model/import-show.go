package model

import "time"

type SourceType string

type ScavengeLinkStatus string

const (
	NewScavengeLink       ScavengeLinkStatus = "NEW"
	ProcessedScavengeLink ScavengeLinkStatus = "PROCESSED"
)

const (
	Apple SourceType = "APPLE"
)

type ScavengeLink struct {
	ID         int64
	SourceID   string
	SourceType SourceType
	Link       string
	Status     ScavengeLinkStatus
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

type ScavengeLinkRepository interface {
	Insert(scavengeLink ScavengeLink) (ScavengeLink, error)
	Update(scavengeLink ScavengeLink) error
	FindByID(id int64) (ScavengeLink, error)
}
