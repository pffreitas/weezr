package model

import (
	"strings"
)

type Language string

const (
	Portuguese Language = "pt"
	English    Language = "en"
)

var SupportedLanguages = []Language{Portuguese}

func GetLanguage(languageStr string) Language {
	var lang Language
	for _, supportedLang := range SupportedLanguages {
		if strings.HasPrefix(strings.ToLower(languageStr), string(supportedLang)) {
			lang = supportedLang
			break
		}
	}

	return lang
}
