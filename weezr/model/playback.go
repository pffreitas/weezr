package model

import "time"

type PlaybackProgress struct {
	ID        int64           `json:"id"`
	EpisodeID int64           `json:"episode_id"`
	Username  string          `json:"username"`
	Progress  int             `json:"progress"`
	Total     int             `json:"total"`
	Context   PlaybackContext `json:"context"`
	Finished  bool            `json:"finished"`
	CreatedAt time.Time       `json:"created_at"`
	UpdatedAt time.Time       `json:"updated_at"`
}

type PlaybackContext map[string]interface{}

type PlaybackProgressRepository interface {
	Insert(playbackProgress PlaybackProgress) error
	Update(playbackProgress PlaybackProgress) error
	FindLatest(episodeID int64, username string) (PlaybackProgress, error)
	FindLatestForUser(username string) (PlaybackProgress, error)
	FindLatestIn(episodeID []interface{}, username string) ([]PlaybackProgress, error)
	FindAll(episodeID int64, username string) ([]PlaybackProgress, error)
}
