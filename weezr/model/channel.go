package model

type Channel struct {
	ID    string `json:"id"`
	Slug  string
	Name  string
	Shows []Show
}

func (c Channel) String() string {
	return c.Name
}

type ChannelRepository interface {
	Insert(channel Channel) error
	FindAll() ([]Channel, error)
	FindBySlug(slug string) (*Channel, error)
}
