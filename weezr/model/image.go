package model

import "time"

type ImageStatus int

const (
	New ImageStatus = iota
	Processed
)

func (i ImageStatus) String() string {
	return [...]string{"New", "Processed"}[i]
}

type Image struct {
	Id            int64
	Key           string
	Source        string
	ETag          string
	ExpiresAt     *time.Time
	ContentLength int64
	ContentType   string
	Status        ImageStatus
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

type ImageRepository interface {
	Insert(image Image) (Image, error)
	Update(image Image) error
	FindByKey(key string) (Image, error)
}
