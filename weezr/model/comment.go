package model

import "time"

type Comment struct {
	ID            int64
	ParentID      int64
	EpisodeID     int64
	Text          string
	ResponseCount int64
	Username      string
	User          User
	CreatedAt     time.Time
}

type CommentRepository interface {
	Insert(comment Comment) (int64, error)
	Get(episodeId int64, page Page) ([]Comment, error)
	FindByID(commentId int64) (Comment, error)

	InsertCommentResponse(parent Comment, response Comment) (int64, error)
	GetCommentResponses(parentId int64, page Page) ([]Comment, error)
}
