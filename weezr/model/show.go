package model

import (
	"crypto/sha1"
	"fmt"
)

type Show struct {
	ID                    int64    `json:"id"`
	Slug                  string   `json:"slug"`
	Name                  string   `json:"name"`
	Description           string   `json:"description"`
	ImageSrc              string   `json:"imageSource"`
	ImageKey              string   `json:"image_key"`
	FeedURL               string   `json:"feed_url"`
	ChannelSlug           string   `json:"channel_slug"`
	Language              Language `json:"language"`
	FollowingCount        int64    `json:"following_count"`
	FollowedByCurrentUser bool     `json:"followedByCurrentUser"`
	ETag                  string   `json:"-"`
	ContentLength         int64    `json:"-"`
	Score                 int64    `json:"score"`
	Active                bool     `json:"-"`
}

func (s Show) Hashcode() string {
	str := fmt.Sprintf("%s", s.FeedURL)
	return fmt.Sprintf("%x", sha1.Sum([]byte(str)))
}

type ShowRepository interface {
	Insert(show Show) (Show, error)
	Update(show Show) error
	FindByID(id int64) (Show, error)
	FindBySlug(slug string) (Show, error)
	FindAll() ([]Show, error)
	FindByIdIn(showIds []interface{}) ([]Show, error)
	IncrScore(showID int64, scoreIncr int) error
}
