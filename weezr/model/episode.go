package model

import (
	"crypto/sha1"
	"fmt"
	"time"
)

type EpisodeSlug string
type Username string

type SlimEpisode struct {
	ID                 int64     `json:"id"`
	Slug               string    `json:"slug"`
	Title              string    `json:"title"`
	Description        string    `json:"description"`
	PublishedAt        time.Time `json:"published_at"`
	ShowSlug           string    `json:"show_slug"`
	ShowTitle          string    `json:"show_title"`
	ImageKey           string    `json:"image_key"`
	LikedByCurrentUser bool      `json:"likeByCurrentUser"`
	Progress           int       `json:"progress"`
	TotalLength        int       `json:"total_length"`
	Finished           bool      `json:"finished"`
	Score              float64   `json:"-"`
}

type ByScore []SlimEpisode

func (a ByScore) Len() int           { return len(a) }
func (a ByScore) Less(i, j int) bool { return a[i].Score < a[j].Score }
func (a ByScore) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type Episode struct {
	ID                    int64     `json:"id"`
	Slug                  string    `json:"slug"`
	Title                 string    `json:"title"`
	Description           string    `json:"description"`
	Content               string    `json:"content"`
	TidyContent           string    `json:"tidyContent"`
	SiteLink              string    `json:"-"`
	PublishedAt           time.Time `json:"published_at"`
	AudioFileURL          string    `json:"audio_file_url"`
	Categories            []string  `json:"-"`
	Keywords              string    `json:"-"`
	Language              Language  `json:"language"`
	Duration              string    `json:"-"`
	ImageSource           string    `json:"imageSource"`
	ImageKey              string    `json:"image_key"`
	ShowID                int64     `json:"show_id"`
	ShowTitle             string    `json:"showTitle"`
	ShowSlug              string    `json:"showSlug"`
	ShowCategories        []string  `json:"-"`
	CreatedAt             time.Time `json:"-"`
	LikeCount             int64     `json:"likeCount"`
	LikedByCurrentUser    bool      `json:"likeByCurrentUser"`
	PlaybackCount         int64     `json:"playbackCount"`
	FollowingCount        int64     `json:"followingCount"`
	FollowedByCurrentUser bool      `json:"followedByCurrentUser"`
	Score                 int64     `json:"-"`
}

func (e Episode) Hashcode() string {
	str := fmt.Sprintf("%s-%d", e.Title, e.PublishedAt.Unix())
	return fmt.Sprintf("%x", sha1.Sum([]byte(str)))
}

func (e Episode) Equals(other Episode) bool {
	return e.Title == other.Title &&
		e.AudioFileURL == other.AudioFileURL &&
		e.PublishedAt.Unix() == other.PublishedAt.Unix() &&
		e.ImageSource == other.ImageSource &&
		e.Content == other.Content &&
		e.Description == other.Description

}

type Page struct {
	Offset int64
	Limit  int64
}

type EpisodeRepository interface {
	Insert(episode Episode) (int64, error)
	Update(episode Episode) error
	FindByID(episodeID int64) (Episode, error)
	FindAll(page Page) ([]Episode, error)
	FindBySlug(episodeSlug EpisodeSlug) (Episode, error)
	FindByShow(showSlug string) ([]Episode, error)
	FindByShowSlug(showSlug string, page Page) ([]SlimEpisode, error)
	FindLatestByShowId(showIds []int64) ([]SlimEpisode, error)
	FindInProgressEpisodes(username string) ([]SlimEpisode, error)
	FindByIdIn(episodeIds []interface{}) ([]SlimEpisode, error)
	IncrPlayback(slug EpisodeSlug) error
	IncrScore(episodeID int64, scoreIncr int) error
}
