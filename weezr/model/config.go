package model

type WeezrConfig struct {
	ServerPort       string `envconfig:"PORT"`
	DatabaseURL      string `envconfig:"DATABASE_URL"`
	JWTSecret        string `envconfig:"JWT_SECRET_KEY"`
	SearchServiceURL string `envconfig:"SEARCH_SERVICE_URL"`
	ESAddress        string `envconfig:"ES_ADDRESS"`
}
