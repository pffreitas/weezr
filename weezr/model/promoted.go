package model

import "time"

type PlacementType string

const (
	HomeBannerPlacement PlacementType = "HomeBanner"
	FeedPlacement       PlacementType = "Feed"
)

type FeedPromotion struct {
	FeedObjectReference

	ID          int64         `json:"id"`
	Placement   PlacementType `json:"placement"`
	EpisodeList EpisodeList   `json:"episode_list"`
	Impressions int64         `json:"impressions"`
	ExpiresAt   time.Time     `json:"expires_at"`
	CreatedAt   time.Time     `json:"created_at"`
}

type EpisodeList struct {
	ID          int64         `json:"id"`
	Slug        string        `json:"slug"`
	Name        string        `json:"name"`
	Description string        `json:"description"`
	ImageKey    string        `json:"image_key"`
	Episodes    []SlimEpisode `json:"episodes"`
}

type PromotionRepository interface {
	Insert(promotion FeedPromotion) error
	FindPromotions(placementType PlacementType, limit int64) ([]FeedPromotion, error)
	IncrImpression(promo []FeedPromotion) error
}
