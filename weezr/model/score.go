package model

const (
	OpenEpisodeScoreIncr          = 1
	LikeEpisodeScoreIncr          = 10
	AddEpisodeToPlaylistScoreIncr = 20
	AddCommentToEpisodeScoreIncr  = 20
	ShareEpisodeScoreIncr         = 30
	PlayEpisodeScoreIncr          = 20
	FinishPlayingEpisodeScoreIncr = 30

	FollowShowScoreIncr = 20
)
