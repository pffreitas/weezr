package model

import (
	"errors"
	"time"
)

type PlaylistType string

const (
	UserDefined = "user-defined"
	History     = "history"
	ListenLater = "listen-later"
	Like        = "liked"
)

var (
	PlaylistItemAlreadyExistsError = errors.New("playlist_item_already_exists")
)

type Playlist struct {
	ID        int64         `json:"id"`
	Type      PlaylistType  `json:"type"`
	Code      string        `json:"code"`
	Title     string        `json:"title"`
	Items     []SlimEpisode `json:"items"`
	Username  string        `json:"username"`
	CreatedAt time.Time     `json:"createdAt"`
	DeletedAt *time.Time    `json:"deletedAt"`
}

type PlaylistRepository interface {
	Insert(playlist Playlist) (Playlist, error)
	FindAll(username string) ([]Playlist, error)
	FindByID(id int64) (Playlist, error)
	FindByUserAndType(username string, playlistType PlaylistType) (Playlist, error)
	Delete(id int64) error
	AddItem(playlist Playlist, episodeId int64) error
	RemoveItem(playlist Playlist, episodeId int64) error
	FindItems(id int64, page Page) ([]SlimEpisode, error)
}
