package model

type ShowSearchResult struct {
	Show
	Score float64 `json:"-"`
}

type ShowSearchResultByScore []ShowSearchResult

func (a ShowSearchResultByScore) Len() int           { return len(a) }
func (a ShowSearchResultByScore) Less(i, j int) bool { return a[i].Score < a[j].Score }
func (a ShowSearchResultByScore) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
