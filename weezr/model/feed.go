package model

import (
	"strings"
	"time"
)

type FeedObjectType string

var (
	EpisodeFeedObjectType  FeedObjectType = "episode"
	ShowFeedObjectType     FeedObjectType = "show"
	PlaylistFeedObjectType FeedObjectType = "playlist"
)

func NewEpisodeFeedObject(episode Episode, username string, score int64) FeedEpisode {
	return FeedEpisode{
		FeedObjectReference: FeedObjectReference{
			ObjectID:   episode.ID,
			ObjectType: EpisodeFeedObjectType,
		},
		Data: SlimEpisode{
			ID:          episode.ID,
			Slug:        episode.Slug,
			Title:       episode.Title,
			Description: chopWords(episode.TidyContent, 20),
			PublishedAt: episode.PublishedAt,
			ShowSlug:    episode.ShowSlug,
			ShowTitle:   episode.ShowTitle,
			ImageKey:    episode.ImageKey,
		},
		Score:     score,
		Username:  username,
		Language:  episode.Language,
		CreatedAt: time.Now().UTC(),
	}
}

type FeedObjectReference struct {
	ObjectID   int64          `json:"object_id"`
	ObjectType FeedObjectType `json:"object_type"`
}

type FeedObjectReferenceReader interface {
	Get() FeedObjectReference
}

func (r FeedObjectReference) Get() FeedObjectReference {
	return r
}

type FeedEpisode struct {
	FeedObjectReference

	ID        int64       `json:"id"`
	Data      SlimEpisode `json:"data"`
	Score     int64       `json:"score"`
	Username  string      `json:"username"`
	Language  Language    `json:"language"`
	CreatedAt time.Time   `json:"created_at"`
}

type FeedEpisodeRepository interface {
	Insert(feedObject FeedEpisode) error
	Fetch(username string, language Language, page Page) ([]SlimEpisode, error)
}

type FeedPage struct {
	Episodes   []SlimEpisode   `json:"episodes"`
	Promotions []FeedPromotion `json:"promotions"`
}

func chopWords(content string, nWords int) string {
	if len(content) > 0 {
		fields := strings.Fields(content)

		if len(fields) > nWords {
			return strings.Join(fields[0:20], " ")
		}

		return strings.Join(fields, " ")
	}

	return content
}
