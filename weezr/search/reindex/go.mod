module weezr.me/weezr/search/reindex

go 1.13

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20201104130540-2e1f801663c6
	github.com/pffreitas/weego v0.0.0-20200122023400-9fb39e754bde
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.30.0
	weezr.me/weezr/business v0.0.0
	weezr.me/weezr/db v0.0.0
	weezr.me/weezr/feed v0.0.0
	weezr.me/weezr/model v0.0.0
	weezr.me/weezr/scavenger v0.0.0
	weezr.me/weezr/search v0.0.0
	weezr.me/weezr/test v0.0.0
)

replace github.com/adjust/rmq/v2 => github.com/pffreitas/rmq/v2 v2.0.0

replace weezr.me/weezr/model => ../../model

replace weezr.me/weezr/db => ../../db

replace weezr.me/weezr/scavenger => ../../scavenger

replace weezr.me/weezr/search => ../../search

replace weezr.me/weezr/test => ../../test

replace weezr.me/weezr/business => ../../business

replace weezr.me/weezr/feed => ../../feed/feed

replace weezr.me/weezr/feedclnt => ../../feed/feedclnt
