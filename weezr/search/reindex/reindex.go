package main

import (
	"context"
	"encoding/json"
	"strings"

	"weezr.me/weezr/search/search"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esutil"
	log "github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

type ReindexConfig struct {
	Address string `envconfig:"ES_ADDRESS"`
	Type    string `envconfig:"TYPE"`
}
type Reindex struct {
	ReindexConfig     ReindexConfig
	ShowRepository    model.ShowRepository
	EpisodeRepository model.EpisodeRepository
}

func (r Reindex) Run() error {
	es, _ := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: []string{r.ReindexConfig.Address},
	})

	log.Println(elasticsearch.Version)
	log.Println(es.Info())

	if r.ReindexConfig.Type == "Show" {
		r.reindexShows(es)
	}

	if r.ReindexConfig.Type == "Episode" {
		r.reindexEpisodes(es)
	}

	return nil
}

func (r Reindex) reindexShows(es *elasticsearch.Client) error {
	_, err := es.Indices.Delete([]string{"shows"})
	if err != nil {
		log.WithError(err).Error("failed reindexing shows")
		return err
	}

	allShows, err := r.ShowRepository.FindAll()
	if err != nil {
		return err
	}

	log.WithField("show-count", len(allShows)).WithField("address", r.ReindexConfig.Address).Info("reindexing shows to ES")

	indexer, _ := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		Index:  "shows",
		Client: es,
	})

	success, failed := 0, 0
	for _, show := range allShows {
		body, err := json.Marshal(search.NewIndexedShow(show))
		if err != nil {
			failed = failed + 1
			log.WithError(err).WithField("show-slug", show.Slug).Error("failed to index show; failed to serialize json")
			continue
		}

		err = indexer.Add(
			context.Background(),
			esutil.BulkIndexerItem{
				Action: "index",
				Body:   strings.NewReader(string(body)),
			})

		if err != nil {
			failed = failed + 1
			log.WithError(err).WithField("show-slug", show.Slug).Error("failed to index show")
			continue
		}
		success = success + 1
	}

	err = indexer.Close(context.Background())
	if err != nil {
		log.WithError(err).Error("failed reindexing shows")
		return err
	}

	log.WithField("succeeded", success).WithField("failed", failed).Info("finished reindexing shows")
	return nil
}

func (r Reindex) reindexEpisodes(es *elasticsearch.Client) error {
	_, err := es.Indices.Delete([]string{"episodes"})
	if err != nil {
		log.WithError(err).Error("failed reindexing episodes")
		return err
	}

	success, failed := 0, 0
	offset := int64(0)
	pageSize := int64(50000)

	allEpisodes, err := r.EpisodeRepository.FindAll(model.Page{Offset: offset, Limit: pageSize})

	for len(allEpisodes) > 0 {
		offset = offset + pageSize
		allEpisodes, err = r.EpisodeRepository.FindAll(model.Page{Offset: offset, Limit: pageSize})
		if err != nil {
			return err
		}

		log.WithField("episode-count", len(allEpisodes)).Info("reindexing episodes to ES")

		indexer, _ := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
			Index:  "episodes",
			Client: es,
		})

		for _, episode := range allEpisodes {
			body, err := json.Marshal(search.NewIndexedEpisode(episode))
			if err != nil {
				failed = failed + 1
				log.WithError(err).WithField("episode-slug", episode.Slug).Error("failed to index episode; failed to serialize json")
				continue
			}

			err = indexer.Add(
				context.Background(),
				esutil.BulkIndexerItem{
					Action: "index",
					Body:   strings.NewReader(string(body)),
				})

			if err != nil {
				failed = failed + 1
				log.WithError(err).WithField("episode-slug", episode.Slug).Error("failed to index episode")
				continue
			}
			success = success + 1
		}

		err = indexer.Close(context.Background())
		if err != nil {
			log.WithError(err).Error("failed reindexing episodes")
			return err
		}
	}

	log.WithField("succeeded", success).WithField("failed", failed).Info("finished reindexing episodes")
	return nil
}
