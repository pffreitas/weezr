package main

import "github.com/sirupsen/logrus"

func main() {
	cronApp := NewScavengerCronApplication()

	cronApp.Invoke(func(reindex Reindex) int {
		err := reindex.Run()
		if err != nil {
			logrus.WithError(err).Error("Failed to run Scavenger")
			panic(err)
		}

		return 0
	})
}
