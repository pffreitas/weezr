package main

import (
	weego "github.com/pffreitas/weego/application"
	"weezr.me/weezr/db"
	"weezr.me/weezr/db/postgres"
)

type SearchReindexApplication struct {
	DatabaseConfig db.DatabaseConfig
	ReindexConfig  ReindexConfig
}

func NewScavengerCronApplication() weego.WeegoApplication {
	app := weego.New(SearchReindexApplication{})

	app.Provide(db.GetPgSession)
	app.Provide(db.GetDbxSession)

	app.Provide(postgres.NewEpisodePostgresRepository)
	app.Provide(postgres.NewShowPostgresRepository)
	app.Provide(Reindex{})

	return app
}
