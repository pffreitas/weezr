module weezr.me/weezr/search

go 1.12

require (
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20201104130540-2e1f801663c6
	github.com/mitchellh/mapstructure v1.3.3
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.2.2
	weezr.me/weezr/model v0.0.0
)

replace weezr.me/weezr/model => ./../model
