package search

import (
	"time"

	"weezr.me/weezr/model"
)

type IndexedDocument interface {
	getID() int64
}

type IndexedDocumentID struct {
	ID int64 `json:"id"`
}

func (i IndexedDocumentID) getID() int64 {
	return i.ID
}

type IndexedEpisode struct {
	IndexedDocumentID
	Slug        string    `json:"slug"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	TidyContent string    `json:"tidyContent"`
	PublishedAt time.Time `json:"published_at"`
	ShowSlug    string    `json:"show_slug"`
	ShowTitle   string    `json:"show_title"`
	ImageKey    string    `json:"image_key"`
}

func NewIndexedEpisode(episode model.Episode) IndexedEpisode {
	return IndexedEpisode{
		IndexedDocumentID: IndexedDocumentID{ID: episode.ID},
		Slug:              episode.Slug,
		Title:             episode.Title,
		Description:       episode.Description,
		TidyContent:       episode.TidyContent,
		PublishedAt:       episode.PublishedAt,
		ShowSlug:          episode.ShowSlug,
		ShowTitle:         episode.ShowTitle,
		ImageKey:          episode.ImageKey,
	}
}

type EpisodeResult struct {
	IndexedEpisode
	Score float64 `json:"score"`
}

type IndexedShow struct {
	IndexedDocumentID
	Slug        string `json:"slug"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ImageKey    string `json:"image_key"`
}

func NewIndexedShow(show model.Show) IndexedShow {
	return IndexedShow{
		IndexedDocumentID: IndexedDocumentID{ID: show.ID},
		Slug:              show.Slug,
		Name:              show.Name,
		Description:       show.Description,
		ImageKey:          show.ImageKey,
	}
}

type ShowResult struct {
	IndexedShow
	Score float64 `json:"score"`
}

type Result struct {
	Source map[string]interface{}
	Score  float64
}
