package search

import (
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

const (
	episodesIndexName = "episodes"
	showsIndexName    = "shows"
)

type SearchService struct {
	esClient *elasticsearch.Client
}

func NewSearchService(config Config) SearchService {
	esClient, _ := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: []string{config.ESAddress},
	})

	return SearchService{esClient: esClient}
}

func (s SearchService) IndexEpisode(episode IndexedEpisode) error {
	logrus.WithField("episode-slug", episode.Slug).Info("indexing episode")
	return indexDocument(episodesIndexName, episode, s.esClient)
}

func (s SearchService) IndexShow(show IndexedShow) error {
	logrus.WithField("show-slug", show.Slug).Info("indexing show")
	return indexDocument(showsIndexName, show, s.esClient)
}

func (s SearchService) SearchEpisodes(queryText string, page model.Page) ([]EpisodeResult, error) {
	results, err := runQuery(QueryContext{
		Index: episodesIndexName,
		Page:  page,
		Query: NewQueryString([]string{"title", "description", "tidyContent"}, queryText),
	}, s.esClient)

	if err != nil {
		return nil, err
	}

	var episodeResults []EpisodeResult
	for _, result := range results {
		var indexedEpisode IndexedEpisode
		err := mapstructure.Decode(result.Source, &indexedEpisode)
		if err != nil {
			logrus.WithError(err).WithField("map", result.Source).Error("failed to decode map into IndexedEpisode")
			continue
		}

		episodeResults = append(episodeResults, EpisodeResult{
			IndexedEpisode: indexedEpisode,
			Score:          result.Score,
		})
	}

	return episodeResults, err
}

func (s SearchService) SearchShows(queryText string, page model.Page) ([]ShowResult, error) {
	results, err := runQuery(QueryContext{
		Index: showsIndexName,
		Page:  page,
		Query: NewQueryString([]string{"name", "description"}, queryText),
	}, s.esClient)

	if err != nil {
		return nil, err
	}

	var showResults []ShowResult
	for _, result := range results {
		var indexedShow IndexedShow
		err := mapstructure.Decode(result.Source, &indexedShow)
		if err != nil {
			logrus.WithError(err).WithField("map", result.Source).Error("failed to decode map into IndexedShow")
			continue
		}

		showResults = append(showResults, ShowResult{
			IndexedShow: indexedShow,
			Score:       result.Score,
		})
	}

	return showResults, err
}
