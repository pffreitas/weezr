package search

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"weezr.me/weezr/model"
)

func TestShareEndpoints(t *testing.T) {
	assertions := assert.New(t)
	searchService := NewSearchService(Config{ESAddress: "http://localhost:9200"})

	err := searchService.IndexEpisode(IndexedEpisode{
		IndexedDocumentID: IndexedDocumentID{ID: 1},
		Slug:              "slug slug",
		Title:             "title",
		Description:       "description",
		TidyContent:       "tidy content",
		PublishedAt:       time.Now(),
		ShowSlug:          "show slug",
		ShowTitle:         "show title",
		ImageKey:          "image key",
	})

	if err != nil {
		t.Fail()
	}

	episodes, err := searchService.SearchEpisodes("title", model.Page{})
	if err != nil {
		t.Fail()
	}

	assertions.Equal(1, len(episodes))
	bla, _ := json.Marshal(episodes)
	fmt.Printf(">>>>>>> %s", bla)

	searchService.IndexShow(IndexedShow{
		IndexedDocumentID: IndexedDocumentID{ID: 1},
		Slug:              "slug",
		Name:              "name",
		Description:       "description",
	})

	shows, err := searchService.SearchShows("name", model.Page{})
	if err != nil {
		t.Fail()
	}
	assertions.Equal(1, len(shows))
	bla, _ = json.Marshal(shows)
	fmt.Printf(">>>>>>> %s", bla)

}
