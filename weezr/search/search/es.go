package search

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/elastic/go-elasticsearch/v8/esapi"

	"weezr.me/weezr/model"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/sirupsen/logrus"
)

type QueryContext struct {
	Index string
	Page  model.Page
	Query Query
}

type Query struct {
	QueryString QueryString `json:"query_string"`
}

type QueryString struct {
	Fields []string `json:"fields"`
	Query  string   `json:"query"`
}

func NewQueryString(fields []string, query string) Query {
	return Query{
		QueryString: QueryString{
			Fields: fields,
			Query:  query,
		},
	}
}

func indexDocument(index string, document IndexedDocument, esClient *elasticsearch.Client) error {
	serializedDocument, err := json.Marshal(document)
	if err != nil {
		logrus.WithError(err).WithField("index", index).WithField("document", document).Error("Failed to index document; failed to serialize")
		return err
	}
	req := esapi.IndexRequest{
		Index:      index,
		DocumentID: strconv.FormatInt(document.getID(), 10),
		Body:       bytes.NewReader(serializedDocument),
		Refresh:    "true",
	}

	res, err := req.Do(context.Background(), esClient)
	if err != nil {
		logrus.WithError(err).WithField("index", index).WithField("document", document).Error("Failed to index document")
		return err
	}

	_ = res.Body.Close()

	if res.IsError() {
		return fmt.Errorf("failed to index document; response is error: %+v; index: %s; document: %s", err, index, string(serializedDocument))
	}

	return nil
}

func runQuery(queryContext QueryContext, esClient *elasticsearch.Client) ([]Result, error) {
	query := queryContext.Query

	buf, err := serializeQuery(query)
	if err != nil {
		return nil, err
	}

	res, err := esClient.Search(
		esClient.Search.WithContext(context.Background()),
		esClient.Search.WithIndex(queryContext.Index),
		esClient.Search.WithBody(&buf),
	)
	if err != nil {
		logrus.WithError(err).WithField("queryContext", queryContext).Error("failed to run query")
		return nil, err
	}

	if res.IsError() {
		logrus.WithError(err).
			WithField("queryContext", queryContext).
			WithField("status", res.Status()).
			WithField("response", res.String()).
			Error("failed to run query; response is error")
		return nil, err
	}

	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		logrus.WithError(err).WithField("queryContext", queryContext).Error("failed to parse query response")
		return nil, err
	}

	_ = res.Body.Close()

	var results []Result
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		result := Result{
			Source: hit.(map[string]interface{})["_source"].(map[string]interface{}),
			Score:  hit.(map[string]interface{})["_score"].(float64),
		}

		results = append(results, result)
	}

	return results, nil
}

func serializeQuery(query Query) (bytes.Buffer, error) {
	var buf bytes.Buffer

	queryWrapper := map[string]interface{}{
		"query": query,
	}

	if err := json.NewEncoder(&buf).Encode(queryWrapper); err != nil {
		logrus.WithError(err).WithField("query", query).Error("failed to serialize query")
		return bytes.Buffer{}, err
	}

	return buf, nil
}
