package commons

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// WriteJSON .
func WriteJSON(w http.ResponseWriter, body interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if body != nil {
		bytes, err := json.Marshal(body)
		CheckError(err, fmt.Sprintf("erro ao serializar json: %#v", body))

		w.Write(bytes)
	}
}
