package commons

import (
	"fmt"
	"log"
	"net/http"
)

// Error x
func Error(err error, message string) error {
	LogError(err, message)
	return fmt.Errorf("%s; error: %s", message, err.Error())
}

// CheckError x
func CheckError(err error, message string) {
	if err != nil {
		LogError(err, message)
		panic(fmt.Errorf("%s; error: %s", message, err.Error()))
	}
}

// LogError  ...
func LogError(err error, message string) {
	if err != nil {
		log.Println("Error:", message, err)
	}
}

//HandleError x
func HandleError(w http.ResponseWriter) {
	r := recover()

	if r != nil {
		err := map[string]string{
			"error": fmt.Sprintf("%s", r),
		}

		WriteJSON(w, err, 500)
	}
}
