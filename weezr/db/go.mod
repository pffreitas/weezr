module weezr.me/weezr/db

go 1.12

require (
	github.com/go-ozzo/ozzo-dbx v1.5.0
	github.com/lib/pq v1.1.1
	github.com/sirupsen/logrus v1.4.2
	weezr.me/weezr/model v0.0.0
)

replace weezr.me/weezr/model => ../model
