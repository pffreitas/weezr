package postgres

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/lib/pq"

	dbx "github.com/go-ozzo/ozzo-dbx"

	"weezr.me/weezr/model"
)

var (
	DuplicatedEpisodeErr = errors.New("duplicated_episode")
)

type EpisodePostgresRepository struct {
	DbSession  *sql.DB
	DbxSession *dbx.DB
}

func NewEpisodePostgresRepository(database *sql.DB, dbxSession *dbx.DB) model.EpisodeRepository {
	return &EpisodePostgresRepository{database, dbxSession}
}

func (repo EpisodePostgresRepository) Insert(episode model.Episode) (int64, error) {
	for {
		episodeID, err := repo.insert(episode)
		if err == DuplicatedSlugErr {
			episode.Slug = fmt.Sprintf("%s-%s", episode.Slug, episode.Hashcode()[0:6])
		} else {
			return episodeID, err
		}
	}
}

func (repo EpisodePostgresRepository) insert(episode model.Episode) (int64, error) {
	var episodeID int64

	cat, _ := json.Marshal(episode.Categories)
	showCategories, _ := json.Marshal(episode.ShowCategories)

	err := repo.DbxSession.NewQuery(`
		insert into episode(
			id, slug, title, description, content, tidy_content, site_url, 
			published_at, audio_file_url, categories, show_id, show_slug, 
			duration, lang, created_at, keywords, show_categories, episode_image, image_key, hashcode
		) 
		values(
			nextval('episode_seq'), {:slug}, {:title}, {:description}, {:content}, {:tidy_content}, {:site_url},
			{:published_at}, {:audio_file_url},  {:categories}, {:show_id}, {:show_slug},
			{:duration}, {:lang}, {:created_at}, {:keywords}, {:show_categories}, {:episode_image}, {:image_key}, {:hashcode}
		) returning id`).
		Bind(dbx.Params{
			"slug":            episode.Slug,
			"title":           episode.Title,
			"description":     episode.Description,
			"content":         episode.Content,
			"tidy_content":    episode.TidyContent,
			"site_url":        episode.SiteLink,
			"published_at":    episode.PublishedAt,
			"audio_file_url":  episode.AudioFileURL,
			"categories":      cat,
			"show_id":         episode.ShowID,
			"show_slug":       episode.ShowSlug,
			"duration":        episode.Duration,
			"lang":            episode.Language,
			"created_at":      time.Now().UTC(),
			"keywords":        episode.Keywords,
			"episode_image":   episode.ImageSource,
			"show_categories": showCategories,
			"image_key":       episode.ImageKey,
			"hashcode":        episode.Hashcode(),
		}).Row(&episodeID)

	if err != nil {
		if pqError, ok := err.(*pq.Error); ok {
			if pqError.Constraint == "episode_hashcode_uk" {
				return episodeID, DuplicatedEpisodeErr
			}

			if pqError.Constraint == "episode_slug_uk" {
				return episodeID, DuplicatedSlugErr
			}

		}
		return episodeID, err
	}

	return episodeID, nil
}

func (repo EpisodePostgresRepository) Update(episode model.Episode) error {
	cat, _ := json.Marshal(episode.Categories)
	showCategories, _ := json.Marshal(episode.ShowCategories)

	_, err := repo.DbxSession.Update("episode",
		dbx.Params{
			"slug":            episode.Slug,
			"title":           episode.Title,
			"description":     episode.Description,
			"content":         episode.Content,
			"tidy_content":    episode.TidyContent,
			"site_url":        episode.SiteLink,
			"published_at":    episode.PublishedAt,
			"audio_file_url":  episode.AudioFileURL,
			"categories":      cat,
			"show_id":         episode.ShowID,
			"show_slug":       episode.ShowSlug,
			"duration":        episode.Duration,
			"lang":            episode.Language,
			"created_at":      time.Now().UTC(),
			"keywords":        episode.Keywords,
			"episode_image":   episode.ImageSource,
			"show_categories": showCategories,
			"image_key":       episode.ImageKey,
			"hashcode":        episode.Hashcode(),
		},
		dbx.HashExp{"id": episode.ID},
	).Execute()

	return err
}

func (repo EpisodePostgresRepository) FindBySlug(slug model.EpisodeSlug) (model.Episode, error) {
	var episode model.Episode

	err := repo.DbxSession.Select("e.id", "e.slug", "e.title", "e.description", "e.content", "e.site_url",
		"e.published_at", "e.audio_file_url", "e.show_slug", "s.name as show_title",
		"e.like_count", "e.playback_count", "e.image_key", "e.tidy_content", "e.score", "e.show_id", "e.lang as language").
		From("episode e").
		Join("JOIN", "show s", dbx.NewExp("s.slug = e.show_slug")).
		Where(dbx.HashExp{"e.slug": string(slug)}).
		One(&episode)

	return episode, err
}

func (repo EpisodePostgresRepository) FindAll(page model.Page) ([]model.Episode, error) {
	var res []model.Episode

	err := repo.DbxSession.Select("e.id", "e.slug", "e.title", "e.description",
		"e.published_at", "e.show_slug", "s.name as show_title",
		"e.image_key", "e.tidy_content", "e.show_id").
		From("episode e").
		Join("JOIN", "show s", dbx.NewExp("s.slug = e.show_slug")).
		Limit(page.Limit).
		Offset(page.Offset).
		All(&res)

	return res, err
}

func (repo EpisodePostgresRepository) FindByShow(showSlug string) ([]model.Episode, error) {
	var res []model.Episode

	err := repo.DbxSession.
		Select("id", "title", "published_at", "audio_file_url").
		From("episode").
		Where(dbx.HashExp{"show_slug": showSlug}).
		All(&res)

	return res, err
}

func (repo EpisodePostgresRepository) IncrPlayback(slug model.EpisodeSlug) error {
	_, err := repo.DbxSession.
		NewQuery("update episode set playback_count = playback_count + 1 where slug = {:episode_slug}").
		Bind(dbx.Params{"episode_slug": slug}).
		Execute()

	return err
}

func (repo EpisodePostgresRepository) FindByShowSlug(showSlug string, page model.Page) ([]model.SlimEpisode, error) {
	var episodes []model.SlimEpisode

	err := repo.DbxSession.Select(
		"e.id", "e.slug", "e.title", "e.published_at", "e.show_slug",
		"s.name as show_title", "e.image_key").
		From("episode e").
		Join("JOIN", "show s", dbx.NewExp("s.slug = e.show_slug")).
		Where(dbx.HashExp{"e.show_slug": showSlug}).
		Limit(page.Limit).
		Offset(page.Offset).
		OrderBy("e.published_at desc").
		All(&episodes)

	return episodes, err
}

func (repo EpisodePostgresRepository) FindByID(episodeID int64) (model.Episode, error) {
	var episode model.Episode

	err := repo.DbxSession.Select("e.id", "e.slug", "e.title", "e.description", "e.content", "e.site_url",
		"e.published_at", "e.audio_file_url", "e.show_slug", "s.name as show_title",
		"e.like_count", "e.playback_count", "e.image_key", "e.tidy_content", "e.score", "e.show_id", "e.lang as language").
		From("episode e").
		Join("JOIN", "show s", dbx.NewExp("s.slug = e.show_slug")).
		Where(dbx.HashExp{"e.id": episodeID}).
		One(&episode)

	return episode, err
}

func (repo EpisodePostgresRepository) IncrScore(episodeID int64, scoreIncr int) error {
	_, err := repo.DbxSession.
		NewQuery("update episode set score = score + {:score} where id = {:episode_id}").
		Bind(dbx.Params{"episode_id": episodeID, "score": scoreIncr}).
		Execute()

	return err
}

func (repo EpisodePostgresRepository) FindLatestByShowId(showIds []int64) ([]model.SlimEpisode, error) {
	var episodes []model.SlimEpisode

	var ids []string
	for _, id := range showIds {
		ids = append(ids, strconv.FormatInt(id, 10))
	}

	err := repo.DbxSession.
		Select("e.id", "e.slug", "e.title", "e.published_at", "e.show_slug",
			"s.name as show_title", "e.image_key").
		From("episode e").
		InnerJoin("show s", dbx.NewExp("e.show_id = s.id")).
		Where(dbx.And(
			dbx.NewExp(
				fmt.Sprintf(
					"e.id in (select e2.id from episode e2 where e2.show_id IN (%s) and e2.published_at > now() - '12 days'::interval)",
					strings.Join(ids, ", "),
				),
			),
		)).
		OrderBy("e.published_at desc").
		All(&episodes)

	return episodes, err
}

func (repo EpisodePostgresRepository) FindInProgressEpisodes(username string) ([]model.SlimEpisode, error) {
	var episodes []model.SlimEpisode

	err := repo.DbxSession.
		Select("e.id", "e.slug", "e.title", "e.published_at", "e.show_slug",
			"s.name as show_title", "e.image_key").
		From("playback_progress p").
		InnerJoin("episode e", dbx.NewExp("e.id = p.episode_id")).
		InnerJoin("show s", dbx.NewExp("s.id = e.show_id")).
		Where(dbx.And(
			dbx.NewExp(`p.id in (select id from playback_progress p2 where p2.episode_id = p.episode_id order by p2.updated_at desc limit 1)`),
			dbx.NewExp("p.updated_at > now() - '20 days'::interval"),
			dbx.HashExp{
				"p.finished": false,
				"p.username": username,
			},
		)).
		OrderBy("p.updated_at desc").
		All(&episodes)

	return episodes, err
}

func (repo EpisodePostgresRepository) FindByIdIn(episodeIds []interface{}) ([]model.SlimEpisode, error) {
	var episodes []model.SlimEpisode

	err := repo.DbxSession.
		Select("e.id", "e.slug", "e.title", "e.published_at", "e.show_slug", "substr(e.tidy_content, 0, 244) as description",
			"s.name as show_title", "e.image_key").
		From("episode e").
		InnerJoin("show s", dbx.NewExp("s.id = e.show_id")).
		Where(dbx.In("e.id", episodeIds...)).
		All(&episodes)

	return episodes, err
}
