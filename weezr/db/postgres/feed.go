package postgres

import (
	"encoding/json"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"weezr.me/weezr/model"
)

type FeedEpisodePostgresRepository struct {
	dbxSession *dbx.DB
}

func NewFeedObjectPostgresRepository(dbx *dbx.DB) model.FeedEpisodeRepository {
	return &FeedEpisodePostgresRepository{dbx}
}

func (r *FeedEpisodePostgresRepository) Insert(feedObject model.FeedEpisode) error {
	objectDataJson, err := json.Marshal(feedObject.Data)
	if err != nil {
		return err
	}

	var res model.FeedEpisode
	err = r.dbxSession.NewQuery(`
		insert into
			feed_object(id, object_id, object_type, object_data, score, language, username, created_at)
		values
			(nextval('feed_object_seq'), {:object_id}, {:object_type}, {:object_data}, {:score}, {:language}, {:username}, {:created_at})
		on conflict(object_id, object_type, username) do
		update
		set
			score = excluded.score,
			created_at = excluded.created_at
		where
			feed_object.object_id = excluded.object_id
			and feed_object.object_type = excluded.object_type
			and feed_object.username = excluded.username
		returning *`).
		Bind(dbx.Params{
			"object_id":   feedObject.ObjectID,
			"object_type": feedObject.ObjectType,
			"object_data": objectDataJson,
			"score":       feedObject.Score,
			"language":    feedObject.Language,
			"username":    feedObject.Username,
			"created_at":  time.Now().UTC(),
		}).
		One(&res)

	return err
}

func (r *FeedEpisodePostgresRepository) Fetch(paramUsername string, language model.Language, page model.Page) ([]model.SlimEpisode, error) {
	var data []model.SlimEpisode

	var where []dbx.Expression

	if len(paramUsername) > 0 {
		where = append(where, dbx.HashExp{"username": paramUsername})
	}

	if len(language) > 0 {
		where = append(where, dbx.And(dbx.HashExp{"username": ""}, dbx.HashExp{"language": language}))
	}

	rows, err := r.dbxSession.
		Select("object_data").
		From("feed_object").
		Where(dbx.Or(where...)).
		Limit(page.Limit).
		Offset(page.Offset).
		OrderBy("created_at desc", "score desc").
		Rows()

	if err != nil {
		return data, err
	}

	for rows.Next() {
		var objectData []byte
		err = rows.Scan(&objectData)
		if err != nil {
			return data, err
		}

		var feedEpisode model.SlimEpisode
		err = json.Unmarshal(objectData, &feedEpisode)
		if err != nil {
			return data, err
		}

		data = append(data, feedEpisode)
	}

	return data, err
}
