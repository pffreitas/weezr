package postgres

import (
	"database/sql"

	"weezr.me/weezr/model"
)

type ChannelPostgresRepository struct {
	DbSession *sql.DB
}

func NewChannelPostgresRepository(database *sql.DB) model.ChannelRepository {
	return &ChannelPostgresRepository{
		database,
	}
}

func (repo ChannelPostgresRepository) Insert(channel model.Channel) error {
	query := `insert into channel(id, slug, name) values (nextval('channel_seq'), $1, $2)`
	_, err := repo.DbSession.Exec(query, channel.Slug, channel.Name)
	return err
}

func (repo ChannelPostgresRepository) FindAll() ([]model.Channel, error) {
	query := `select id, slug, name from channel`
	rows, err := repo.DbSession.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var channelList []model.Channel
	for rows.Next() {
		var channel model.Channel
		err = rows.Scan(
			&channel.ID,
			&channel.Slug,
			&channel.Name,
		)

		if err != nil {
			return nil, err
		}
		channelList = append(channelList, channel)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return channelList, nil
}

// FindBySlug .
func (repo ChannelPostgresRepository) FindBySlug(slug string) (*model.Channel, error) {
	query := `select id, name, slug from channel where slug = $1`

	rows := repo.DbSession.QueryRow(query, slug)

	channel := model.Channel{}
	err := rows.Scan(
		&channel.ID,
		&channel.Name,
		&channel.Slug,
	)
	if err != nil {
		return nil, err
	}
	return &channel, nil
}
