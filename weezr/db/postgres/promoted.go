package postgres

import (
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"weezr.me/weezr/model"
)

type PromotionPostgresRepository struct {
	dbxSession *dbx.DB
}

func NewPromotionPostgresRepository(dbxSession *dbx.DB) model.PromotionRepository {
	return &PromotionPostgresRepository{dbxSession}
}

func (r PromotionPostgresRepository) Insert(promotion model.FeedPromotion) error {
	_, err := r.dbxSession.Insert("promotion", dbx.Params{
		"id":          dbx.NewExp("nextval('promotion_seq')"),
		"type":        promotion.ObjectType,
		"placement":   promotion.Placement,
		"object_id":   promotion.ObjectID,
		"impressions": promotion.Impressions,
		"expires_at":  promotion.ExpiresAt,
		"created_at":  time.Now().UTC(),
	}).Execute()
	return err
}

func (r PromotionPostgresRepository) FindPromotions(placementType model.PlacementType, limit int64) ([]model.FeedPromotion, error) {
	var rows []model.FeedPromotion

	err := r.dbxSession.Select("id", "type as object_type", "placement", "object_id", "impressions", "expires_at", "created_at").
		From("promotion").
		Where(dbx.And(
			dbx.NewExp("impressions > 0"),
			dbx.NewExp("expires_at > {:now}", dbx.Params{"now": time.Now().UTC()}),
			dbx.HashExp{"placement": placementType},
		)).
		OrderBy("impressions desc").
		Limit(limit).
		All(&rows)

	return rows, err
}

func (r PromotionPostgresRepository) IncrImpression(promo []model.FeedPromotion) error {
	if len(promo) == 0 {
		return nil
	}

	var ids []interface{}

	for _, p := range promo {
		ids = append(ids, p.ID)
	}

	_, err := r.dbxSession.Update(
		"promotion",
		dbx.Params{"impressions": dbx.NewExp("impressions - 1")},
		dbx.In("id", ids...),
	).Execute()

	return err
}
