package postgres

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"weezr.me/weezr/model"
)

type ScavengeLinkPostgresRepository struct {
	DbxSession *dbx.DB
}

func NewScavengeLinkPostgresRepository(dbxSession *dbx.DB) model.ScavengeLinkRepository {
	return ScavengeLinkPostgresRepository{dbxSession}
}

func (r ScavengeLinkPostgresRepository) Insert(link model.ScavengeLink) (model.ScavengeLink, error) {
	var ret model.ScavengeLink

	err := r.DbxSession.NewQuery(`
		insert into
			scavenge_link(id, source_id, source_type, link, status, created_at, updated_at)
		values
			(nextval('scavenge_link_seq'), {:source_id}, {:source_type}, {:link}, {:status}, {:created_at}, {:updated_at})
		on conflict(source_id, source_type, link) do nothing
		returning *`).
		Bind(dbx.Params{
			"source_id":   link.SourceID,
			"source_type": string(link.SourceType),
			"link":        link.Link,
			"status":      string(link.Status),
			"created_at":  link.CreatedAt,
			"updated_at":  link.UpdatedAt,
		}).One(&ret)

	return ret, err
}

func (r ScavengeLinkPostgresRepository) Update(link model.ScavengeLink) error {
	_, err := r.DbxSession.Update("scavenge_link",
		dbx.Params{
			"status":     link.Status,
			"created_at": link.CreatedAt,
			"updated_at": link.UpdatedAt,
		},
		dbx.HashExp{"id": link.ID},
	).Execute()

	return err
}

func (r ScavengeLinkPostgresRepository) FindByID(id int64) (model.ScavengeLink, error) {
	var link model.ScavengeLink

	err := r.DbxSession.Select("id", "source_id", "source_type", "link", "status", "created_at", "updated_at").
		From("scavenge_link").
		Where(dbx.HashExp{"id": id}).
		One(&link)

	return link, err
}
