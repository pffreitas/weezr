package postgres

import (
	"database/sql"
	"encoding/json"

	dbx "github.com/go-ozzo/ozzo-dbx"

	log "github.com/sirupsen/logrus"

	"weezr.me/weezr/model"
)

type PlaybackProgressPostgresRepository struct {
	dbSession  *sql.DB
	dbxSession *dbx.DB
}

func NewPlaybackProgressPostgresRepository(database *sql.DB, dbxSession *dbx.DB) model.PlaybackProgressRepository {
	return PlaybackProgressPostgresRepository{database, dbxSession}
}

func (r PlaybackProgressPostgresRepository) Insert(playback model.PlaybackProgress) error {
	context, err := json.Marshal(playback.Context)
	if err != nil {
		log.WithError(err).Errorf("failed to serialize playback context to json")
	}

	_, err = r.dbxSession.Insert("playback_progress", dbx.Params{
		"id":         dbx.NewExp("nextval('playback_progress_seq')"),
		"episode_id": playback.EpisodeID,
		"username":   playback.Username,
		"progress":   playback.Progress,
		"total":      playback.Total,
		"finished":   playback.Finished,
		"context":    context,
		"created_at": playback.CreatedAt,
		"updated_at": playback.UpdatedAt,
	}).Execute()

	return err
}

func (r PlaybackProgressPostgresRepository) Update(playback model.PlaybackProgress) error {
	query := `
	UPDATE
    	playback_progress p1
	SET
		progress = $1,
	    total = $2,
		context = $3,
	    finished = $4,
		updated_at = now()
	WHERE
		episode_id = $5
		AND username = $6
		AND updated_at = (
			SELECT
				max(p2.updated_at)
			FROM
				playback_progress p2
			WHERE
				p2.episode_id = p1.episode_id
				AND p2.username = p1.username)

	`

	context, err := json.Marshal(playback.Context)
	if err != nil {
		log.WithError(err).Errorf("failed to serialize playback context to json")
	}

	_, err = r.dbSession.Exec(query, playback.Progress, playback.Total,
		context, playback.Finished, playback.EpisodeID, playback.Username)

	return err
}

func (r PlaybackProgressPostgresRepository) FindLatest(episodeID int64, username string) (model.PlaybackProgress, error) {
	query := `
	select id, episode_id, username, progress, total, context, finished, created_at, updated_at from 
	playback_progress where episode_id = $1 and username = $2
	order by updated_at desc limit 1
	`

	row := r.dbSession.QueryRow(query, episodeID, username)

	var contextBytes []byte
	var playbackProgress model.PlaybackProgress

	err := row.Scan(
		&playbackProgress.ID,
		&playbackProgress.EpisodeID,
		&playbackProgress.Username,
		&playbackProgress.Progress,
		&playbackProgress.Total,
		&contextBytes,
		&playbackProgress.Finished,
		&playbackProgress.CreatedAt,
		&playbackProgress.UpdatedAt,
	)

	json.Unmarshal(contextBytes, &playbackProgress.Context)

	if err != nil {
		return model.PlaybackProgress{}, err
	}

	return playbackProgress, nil
}

func (r PlaybackProgressPostgresRepository) FindAll(episodeID int64, username string) ([]model.PlaybackProgress, error) {
	query := `
	select id, episode_id, username, progress, total, finished, created_at, updated_at from 
	playback_progress where episode_id = $1 and username = $2
	order by updated_at desc
	`

	rows, err := r.dbSession.Query(query, episodeID, username)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var list []model.PlaybackProgress
	for rows.Next() {
		var playbackProgress model.PlaybackProgress
		err := rows.Scan(
			&playbackProgress.ID,
			&playbackProgress.EpisodeID,
			&playbackProgress.Username,
			&playbackProgress.Progress,
			&playbackProgress.Total,
			&playbackProgress.Finished,
			&playbackProgress.CreatedAt,
			&playbackProgress.UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		list = append(list, playbackProgress)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (r PlaybackProgressPostgresRepository) FindLatestIn(episodeID []interface{}, username string) ([]model.PlaybackProgress, error) {
	var res []model.PlaybackProgress

	err := r.dbxSession.Select("episode_id", "progress", "total", "finished").
		From("playback_progress p1").
		Where(dbx.And(
			dbx.In("episode_id", episodeID...),
			dbx.HashExp{"username": username},
			dbx.NewExp(`id in (select id from playback_progress p2
									where p2.episode_id = p1.episode_id
									and p2.username = p1.username
									order by updated_at desc
									limit 1)`),
		)).
		All(&res)

	return res, err
}

func (r PlaybackProgressPostgresRepository) FindLatestForUser(username string) (model.PlaybackProgress, error) {
	var res model.PlaybackProgress

	err := r.dbxSession.Select("episode_id", "progress", "total", "finished").
		From("playback_progress p").
		Where(dbx.HashExp{"username": username}).
		OrderBy("updated_at desc").
		Limit(1).
		One(&res)

	return res, err
}
