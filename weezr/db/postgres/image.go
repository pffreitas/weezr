package postgres

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/sirupsen/logrus"

	"weezr.me/weezr/model"
)

type ImagePostgresRepository struct {
	DbxSession *dbx.DB
}

func NewImagePostgresRepository(dbxSession *dbx.DB) model.ImageRepository {
	return &ImagePostgresRepository{dbxSession}
}

func (repo ImagePostgresRepository) Insert(image model.Image) (model.Image, error) {
	var persistedImage model.Image
	err := repo.DbxSession.NewQuery(`
		insert into
			image(id, source, key, etag, expires_at, content_type, content_length, status, created_at, updated_at)
		values
			(nextval('img_seq'), {:source}, {:key}, {:etag}, {:expires_at}, {:content_type}, {:content_length}, {:status}, {:created_at}, {:updated_at})
		on conflict(source) do
		update
		set
			updated_at = now()
		where
			image.source = excluded.source 
		returning *`).
		Bind(dbx.Params{
			"source":         image.Source,
			"key":            image.Key,
			"etag":           image.ETag,
			"expires_at":     image.ExpiresAt,
			"content_type":   image.ContentType,
			"content_length": image.ContentLength,
			"status":         image.Status,
			"created_at":     image.CreatedAt,
			"updated_at":     image.UpdatedAt,
		}).One(&persistedImage)

	return persistedImage, err
}

func (repo ImagePostgresRepository) Update(image model.Image) error {
	logrus.WithField("id", image.Id).Info("updating image")

	_, err := repo.DbxSession.Update("image",
		dbx.Params{
			"etag":           image.ETag,
			"expires_at":     image.ExpiresAt,
			"content_type":   image.ContentType,
			"content_length": image.ContentLength,
			"status":         image.Status,
			"updated_at":     image.UpdatedAt,
		},
		dbx.HashExp{"id": image.Id}).Execute()

	return err
}

func (repo ImagePostgresRepository) FindByKey(key string) (model.Image, error) {
	var image model.Image

	err := repo.DbxSession.
		Select("id", "source", "key", "etag", "expires_at", "content_type", "content_length", "status", "created_at", "updated_at").
		From("image").
		Where(dbx.HashExp{"key": key}).
		One(&image)

	return image, err
}
