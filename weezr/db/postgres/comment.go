package postgres

import (
	"database/sql"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"

	"weezr.me/weezr/model"
)

type CommentPostgresRepository struct {
	DbSession  *sql.DB
	DbxSession *dbx.DB
}

func NewCommentPostgresRepository(dbSession *sql.DB, dbxSession *dbx.DB) model.CommentRepository {
	return CommentPostgresRepository{dbSession, dbxSession}
}

func (r CommentPostgresRepository) Insert(comment model.Comment) (int64, error) {

	var commentID int64
	err := r.DbxSession.NewQuery("insert into comment(id, parent_id, episode_id, text, username, created_at) values(nextval('comment_seq'), {:parent_id}, {:episode_id}, {:text}, {:username}, {:created_at}) returning id").
		Bind(dbx.Params{
			"id":         dbx.NewExp("nextval('comment_seq')"),
			"parent_id":  comment.ParentID,
			"episode_id": comment.EpisodeID,
			"text":       comment.Text,
			"username":   comment.Username,
			"created_at": time.Now().UTC(),
		}).Row(&commentID)

	return commentID, err
}

func (r CommentPostgresRepository) Get(episodeId int64, page model.Page) ([]model.Comment, error) {
	var commentList []model.Comment

	err := r.DbxSession.Select("c.id", "parent_id", "episode_id", "text", "created_at", "response_count", "c.username",
		"u.id as user.id", "u.username as user.username", "u.name as user.name", "u.nickname as user.nickname",
		"u.given_name as user.given_name", "u.family_name as user.family_name", "u.email as user.email",
		"u.last_ip as user.last_ip", "u.picture as user.picture").
		From("comment c").
		Join("JOIN", "weezr_user u", dbx.NewExp("c.username = u.id")).
		Where(dbx.HashExp{"episode_id": episodeId, "parent_id": 0}).
		OrderBy("created_at DESC").
		Limit(page.Limit).
		Offset(page.Offset).
		All(&commentList)

	if err != nil {
		return commentList, err
	}

	return commentList, nil
}

func (r CommentPostgresRepository) FindByID(commentId int64) (model.Comment, error) {
	var comment model.Comment

	err := r.DbxSession.Select("c.id", "parent_id", "episode_id", "text", "created_at", "response_count", "c.username",
		"u.id as user.id", "u.username as user.username", "u.name as user.name", "u.nickname as user.nickname",
		"u.given_name as user.given_name", "u.family_name as user.family_name", "u.email as user.email",
		"u.last_ip as user.last_ip", "u.picture as user.picture").
		From("comment c").
		Join("JOIN", "weezr_user u", dbx.NewExp("c.username = u.id")).
		Where(dbx.HashExp{"c.id": commentId}).
		One(&comment)

	if err != nil {
		return comment, err
	}

	return comment, nil
}

func (r CommentPostgresRepository) InsertCommentResponse(parent model.Comment, response model.Comment) (int64, error) {
	var responseID int64
	err := r.DbxSession.NewQuery("insert into comment(id, parent_id, episode_id, text, username, created_at) values(nextval('comment_seq'), {:parent_id}, {:episode_id}, {:text}, {:username}, {:created_at}) returning id").
		Bind(dbx.Params{
			"parent_id":  parent.ID,
			"episode_id": parent.EpisodeID,
			"text":       response.Text,
			"username":   response.Username,
			"created_at": time.Now().UTC(),
		}).Row(&responseID)

	if err != nil {
		return responseID, err
	}

	_, err = r.DbxSession.Update("comment", dbx.Params{"response_count": dbx.NewExp("response_count + 1")}, dbx.HashExp{"id": parent.ID}).Execute()

	if err != nil {
		return responseID, err
	}

	return responseID, nil
}

func (r CommentPostgresRepository) GetCommentResponses(parentId int64, page model.Page) ([]model.Comment, error) {
	var commentList []model.Comment

	err := r.DbxSession.Select("c.id", "parent_id", "episode_id", "text", "created_at", "response_count", "c.username",
		"u.id as user.id", "u.username as user.username", "u.name as user.name", "u.nickname as user.nickname",
		"u.given_name as user.given_name", "u.family_name as user.family_name", "u.email as user.email",
		"u.last_ip as user.last_ip", "u.picture as user.picture").
		From("comment c").
		Join("JOIN", "weezr_user u", dbx.NewExp("c.username = u.id")).
		Where(dbx.HashExp{"parent_id": parentId}).
		OrderBy("created_at DESC").
		Limit(page.Limit).
		Offset(page.Offset).
		All(&commentList)

	if err != nil {
		return commentList, err
	}

	return commentList, nil
}
