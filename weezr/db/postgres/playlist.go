package postgres

import (
	"database/sql"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"

	"github.com/lib/pq"

	"weezr.me/weezr/model"
)

type PlaylistPostgresRepository struct {
	db         *sql.DB
	dbxSession *dbx.DB
}

func NewPlaylistPostgresRepository(database *sql.DB, dbxSession *dbx.DB) model.PlaylistRepository {
	return &PlaylistPostgresRepository{database, dbxSession}
}

func (repo PlaylistPostgresRepository) Insert(playlist model.Playlist) (model.Playlist, error) {
	var persisted model.Playlist

	err := repo.dbxSession.NewQuery(`
		insert into playlist (id, type, code, title, username, created_at)
		values (nextval('playlist_seq'), {:type}, {:code}, {:title}, {:username}, {:created_at}) returning *`).
		Bind(dbx.Params{
			"type":       playlist.Type,
			"code":       playlist.Code,
			"title":      playlist.Title,
			"username":   playlist.Username,
			"created_at": time.Now().UTC(),
		}).
		One(&persisted)

	return persisted, err
}

func (repo PlaylistPostgresRepository) FindAll(username string) ([]model.Playlist, error) {
	var list []model.Playlist

	err := repo.dbxSession.
		Select("id", "type", "code", "title", "username", "created_at").
		From("playlist").
		Where(dbx.HashExp{
			"username":   username,
			"deleted_at": dbx.NewExp("deleted_at is null"),
		}).
		All(&list)

	return list, err
}

func (repo PlaylistPostgresRepository) FindByID(id int64) (model.Playlist, error) {
	var playlist model.Playlist

	err := repo.dbxSession.
		Select("id", "type", "code", "title", "username", "created_at").
		From("playlist").
		Where(dbx.HashExp{
			"id":         id,
			"deleted_at": dbx.NewExp("deleted_at is null"),
		}).
		One(&playlist)

	return playlist, err
}

func (repo PlaylistPostgresRepository) Delete(id int64) error {
	query := `update playlist set deleted_at = $1 where id = $2`
	_, err := repo.db.Exec(query, time.Now().UTC(), id)
	return err
}

func (repo PlaylistPostgresRepository) AddItem(playlist model.Playlist, episodeId int64) error {
	sqlString := `insert into playlist_item(episode_id,  playlist_id) values ($1, $2)`
	_, err := repo.db.Exec(sqlString, episodeId, playlist.ID)

	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok && pqErr.Constraint == "playlist_item_episode_uk" {
			return model.PlaylistItemAlreadyExistsError
		}

		return err
	}

	return nil
}

func (repo PlaylistPostgresRepository) FindItems(playlistId int64, page model.Page) ([]model.SlimEpisode, error) {
	query := `
			SELECT
			    e.id, e.slug, e.title, e.published_at, e.image_key, substr(e.tidy_content, 0, 244) as description,
				sh.slug as show_slug, sh.name as show_title
			FROM
				playlist_item pi
					JOIN episode e ON e.id = pi.episode_id
					JOIN show sh ON sh.id = e.show_id
					JOIN playlist p on p.id = pi.playlist_id
					LEFT JOIN playback_progress pp on pp.id = (select id from playback_progress p2
														  where p2.episode_id = pi.episode_id
														  and p2.username = pp.username
														  order by updated_at desc
														  limit 1)
			WHERE
              pi.playlist_id = {:playlist_id}
			  AND p.deleted_at is null
			  AND pi.deleted_at is null
			ORDER BY  coalesce(pp.finished, false) asc, pi.created_at desc
			LIMIT {:limit}
			OFFSET {:offset}
	`

	var list []model.SlimEpisode

	err := repo.dbxSession.NewQuery(query).
		Bind(dbx.Params{"playlist_id": playlistId, "limit": page.Limit, "offset": page.Offset}).
		All(&list)

	return list, err
}

func (repo PlaylistPostgresRepository) FindByUserAndType(username string, playlistType model.PlaylistType) (model.Playlist, error) {
	var playlists model.Playlist

	err := repo.dbxSession.
		Select("id", "type", "code", "title", "username", "created_at").
		From("playlist").
		Where(dbx.HashExp{"username": username, "type": playlistType, "deleted_at": dbx.NewExp("deleted_at is null")}).
		One(&playlists)

	return playlists, err
}

func (repo PlaylistPostgresRepository) RemoveItem(playlist model.Playlist, episodeId int64) error {
	query := `update playlist_item set deleted_at = $1 where playlist_id = $2 and episode_id = $3`
	_, err := repo.db.Exec(query, time.Now().UTC(), playlist.ID, episodeId)
	return err
}
