package postgres

import (
	"errors"
	"fmt"

	"github.com/lib/pq"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"weezr.me/weezr/model"
)

type ShowPostgresRepository struct {
	dbxSession *dbx.DB
}

func NewShowPostgresRepository(dbxSession *dbx.DB) model.ShowRepository {
	return &ShowPostgresRepository{dbxSession}
}

var (
	DuplicatedSlugErr = errors.New("duplicated_slug")
	DuplicatedShowErr = errors.New("duplicated_show")
)

func (repo ShowPostgresRepository) Insert(show model.Show) (model.Show, error) {
	for {
		persistedShow, err := repo.insert(show)
		if err == DuplicatedSlugErr {
			show.Slug = fmt.Sprintf("%s-%s", show.Slug, show.Hashcode()[0:6])
		} else {
			return persistedShow, err
		}
	}
}

func (repo ShowPostgresRepository) insert(show model.Show) (model.Show, error) {
	var persistedShow model.Show

	err := repo.dbxSession.NewQuery(`
		insert into show (
			id, slug, name, description, image, image_key,  
			feed_url, channel_slug, lang, etag, content_length, hashcode, active
		)
		values (
			nextval('show_seq'), {:slug}, {:name}, {:description}, {:image}, {:image_key},
			{:feed_url}, {:channel_slug}, {:lang}, {:etag}, {:content_length}, {:hashcode}, {:active}
		) returning *`).
		Bind(dbx.Params{
			"slug":           show.Slug,
			"name":           show.Name,
			"description":    show.Description,
			"image":          show.ImageSrc,
			"image_key":      show.ImageKey,
			"feed_url":       show.FeedURL,
			"channel_slug":   show.ChannelSlug,
			"lang":           show.Language,
			"etag":           show.ETag,
			"content_length": show.ContentLength,
			"hashcode":       show.Hashcode(),
			"active":         show.Active,
		}).
		One(&persistedShow)

	if err != nil {
		if pqError, ok := err.(*pq.Error); ok {
			if pqError.Constraint == "show_hashcode_uk" {
				return persistedShow, DuplicatedShowErr
			}

			if pqError.Constraint == "show_slug_uk" {
				return persistedShow, DuplicatedSlugErr
			}

		}
		return persistedShow, err
	}

	return persistedShow, nil
}

func (repo ShowPostgresRepository) Update(show model.Show) error {
	_, err := repo.dbxSession.Update("show",
		dbx.Params{
			"name":           show.Name,
			"description":    show.Description,
			"image":          show.ImageSrc,
			"image_key":      show.ImageKey,
			"etag":           show.ETag,
			"content_length": show.ContentLength,
			"lang":           show.Language,
			"active":         show.Active,
		},
		dbx.HashExp{"slug": show.Slug},
	).Execute()

	return err
}

func (repo ShowPostgresRepository) FindAll() ([]model.Show, error) {
	var shows []model.Show

	err := repo.dbxSession.
		Select("id", "slug", "name", "description", "lang", "image_key").
		From("show").
		Where(dbx.HashExp{"active": true}).
		All(&shows)

	return shows, err
}

func (repo ShowPostgresRepository) FindBySlug(slug string) (model.Show, error) {
	var show model.Show

	err := repo.dbxSession.
		Select("id", "slug", "name", "description", "image as image_src", "image_key",
			"feed_url", "channel_slug", "lang", "etag", "content_length", "active", "score").
		From("show").
		Where(dbx.HashExp{"slug": slug}).
		One(&show)

	return show, err
}

func (repo ShowPostgresRepository) FindByID(id int64) (model.Show, error) {
	var show model.Show

	err := repo.dbxSession.
		Select("id", "slug", "name", "description", "image as image_src", "image_key",
			"feed_url", "channel_slug", "lang", "etag", "content_length", "active").
		From("show").
		Where(dbx.HashExp{"id": id}).
		One(&show)

	return show, err
}

func (repo ShowPostgresRepository) IncrScore(showID int64, scoreIncr int) error {
	_, err := repo.dbxSession.
		NewQuery("update show set score = score + {:score} where id = {:show_id}").
		Bind(dbx.Params{"show_id": showID, "score": scoreIncr}).
		Execute()

	return err
}

func (repo ShowPostgresRepository) FindByIdIn(showIds []interface{}) ([]model.Show, error) {
	var shows []model.Show

	err := repo.dbxSession.
		Select("id", "slug", "name", "description", "image as image_src", "image_key",
			"feed_url", "channel_slug", "lang", "etag", "content_length", "active").
		From("show s").
		Where(dbx.In("s.id", showIds...)).
		All(&shows)

	return shows, err
}
