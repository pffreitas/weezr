package postgres

import (
	"database/sql"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

type UserPostgresRepository struct {
	dbxSession *dbx.DB
}

func NewUserPostgresRepository(dbxSession *dbx.DB) model.UserRepository {
	return UserPostgresRepository{dbxSession}
}

func (e UserPostgresRepository) Save(user model.User) error {

	var existingUser model.User
	err := e.dbxSession.Select("id").From("weezr_user").
		Where(dbx.HashExp{"id": user.ID}).
		One(&existingUser)

	saveParams := dbx.Params{
		"id":          user.ID,
		"username":    user.Username,
		"name":        user.Name,
		"nickname":    user.Nickname,
		"given_name":  user.GivenName,
		"family_name": user.FamilyName,
		"email":       user.Email,
		"last_ip":     user.LastIP,
		"picture":     user.Picture,
		"locale":      user.Locale,
	}

	if err == sql.ErrNoRows {
		logrus.WithError(err).Error("Failed to find user by id")

		_, err := e.dbxSession.Insert("weezr_user", saveParams).Execute()
		return err
	} else {
		_, err := e.dbxSession.Update("weezr_user", saveParams, dbx.HashExp{"id": user.ID}).Execute()
		return err
	}
}

func (e UserPostgresRepository) FindByID(ID string) (model.User, error) {
	var user model.User

	err := e.dbxSession.
		Select("id", "username", "name", "nickname", "given_name", "family_name", "email", "last_ip", "picture", "locale").
		From("weezr_user").
		Where(dbx.HashExp{"id": ID}).
		One(&user)

	return user, err
}
