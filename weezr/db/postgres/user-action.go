package postgres

import (
	"database/sql"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"

	"weezr.me/weezr/model"
)

type UserActionPostgresRepository struct {
	db         *sql.DB
	dbxSession *dbx.DB
}

func NewUserActionPostgresRepository(db *sql.DB, dbxSession *dbx.DB) model.UserActionRepository {
	return UserActionPostgresRepository{db, dbxSession}
}

func (r UserActionPostgresRepository) Insert(action model.UserAction) error {
	_, err := r.db.Exec(
		"insert into user_action(username, object_id, action_type, created_at) values($1, $2, $3, $4)",
		action.Username,
		action.ObjectId,
		action.ActionType,
		time.Now().UTC(),
	)

	return err
}

func (r UserActionPostgresRepository) Get(username string, objectId int64, actionType model.ActionType) (model.UserAction, error) {
	var userAction model.UserAction

	err := r.db.
		QueryRow(`
				SELECT
					username,
					object_id,
					action_type,
					created_at,
					deleted_at
				FROM
					user_action
				WHERE
					username = $1
					AND object_id = $2
					AND action_type = $3 
					AND deleted_at is null`,
			username,
			objectId,
			actionType).
		Scan(
			&userAction.Username,
			&userAction.ObjectId,
			&userAction.ActionType,
			&userAction.CreatedAt,
			&userAction.DeletedAt,
		)

	return userAction, err
}

func (r UserActionPostgresRepository) Delete(username string, objectId int64, actionType model.ActionType) error {
	_, err := r.db.Exec(
		"update user_action set deleted_at = $1 where username = $2 and object_id = $3 and action_type = $4",
		time.Now().UTC(),
		username,
		objectId,
		actionType,
	)

	return err
}

func (r UserActionPostgresRepository) IncrCount(objectId int64, actionType model.ActionType) error {
	_, err := r.db.Exec(
		"insert into user_action_count(object_id, action_type, cnt) values ($1, $2, 1) on conflict(object_id, action_type) do update set cnt = user_action_count.cnt + 1",
		objectId,
		actionType,
	)

	return err
}

func (r UserActionPostgresRepository) Count(objectId int64, actionType model.ActionType) (int64, error) {
	var cnt int64

	err := r.db.
		QueryRow(
			"select cnt from user_action_count where object_id = $1 and action_type = $2",
			objectId,
			actionType,
		).
		Scan(&cnt)

	return cnt, err
}

func (r UserActionPostgresRepository) DecrCount(objectId int64, actionType model.ActionType) error {
	_, err := r.db.Exec(
		"update user_action_count set cnt = cnt - 1 where object_id = $1 and action_type = $2",
		objectId,
		actionType,
	)

	return err
}

func (r UserActionPostgresRepository) FindAllByUsernameAndType(username string, actionType model.ActionType) ([]model.UserAction, error) {
	rows, err := r.db.Query(`
				SELECT
					username,
					object_id,
					action_type,
					created_at,
					deleted_at
				FROM
					user_action
				WHERE
					username = $1
					AND action_type = $2 
					AND deleted_at is null`,
		username,
		actionType,
	)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var userActions []model.UserAction
	for rows.Next() {
		var userAction model.UserAction

		err := rows.Scan(
			&userAction.Username,
			&userAction.ObjectId,
			&userAction.ActionType,
			&userAction.CreatedAt,
			&userAction.DeletedAt,
		)

		if err != nil {
			return nil, err
		}

		userActions = append(userActions, userAction)
	}

	err = rows.Err()

	return userActions, nil
}

func (r UserActionPostgresRepository) FindByType(username string, actionType model.ActionType, page model.Page) ([]model.UserAction, error) {

	var all []model.UserAction

	err := r.dbxSession.
		Select("username", "object_id", "action_type", "created_at", "deleted_at").
		From("user_action").
		Where(
			dbx.NewExp(
				"username={:username} and action_type={:action_type} and deleted_at is null",
				dbx.Params{"username": username, "action_type": actionType},
			),
		).
		Limit(page.Limit).
		Offset(page.Offset).
		OrderBy("created_at desc").
		All(&all)

	return all, err
}

func (r UserActionPostgresRepository) FindAllByObjectIdAndType(objectId int64, actionType model.ActionType) ([]model.UserAction, error) {

	var all []model.UserAction

	err := r.dbxSession.
		Select("username", "object_id", "action_type", "created_at", "deleted_at").
		From("user_action").
		Where(
			dbx.NewExp(
				"object_id={:object_id} and action_type={:action_type} and deleted_at is null",
				dbx.Params{"object_id": objectId, "action_type": actionType},
			),
		).
		OrderBy("created_at desc").
		All(&all)

	return all, err
}

func (r UserActionPostgresRepository) FindAllByUsernameAndTypeAndObjectIDIn(username string, actionType model.ActionType, objectIDs []interface{}) ([]model.UserAction, error) {
	var all []model.UserAction

	err := r.dbxSession.
		Select("username", "object_id", "action_type", "created_at", "deleted_at").
		From("user_action").
		Where(
			dbx.And(
				dbx.NewExp(
					"username={:username} and action_type={:action_type} and deleted_at is null ",
					dbx.Params{"username": username, "action_type": actionType},
				),
				dbx.In("object_id", objectIDs...),
			),
		).
		OrderBy("created_at desc").
		All(&all)

	return all, err
}
