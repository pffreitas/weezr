package db

import (
	"database/sql"
	"log"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	_ "github.com/lib/pq"
)

type DatabaseConfig struct {
	DatabaseURL string `envconfig:"DATABASE_URL"`
}

func GetPgSession(config DatabaseConfig) *sql.DB {
	conn, err := sql.Open("postgres", config.DatabaseURL)
	if err != nil {
		log.Fatal(err)
	}

	conn.SetMaxOpenConns(15)
	conn.SetMaxIdleConns(5)
	conn.SetConnMaxLifetime(time.Hour)

	return conn
}

func GetDbxSession(conn *sql.DB) *dbx.DB {
	db := dbx.NewFromDB(conn, "postgres")
	//db.QueryLogFunc = func(ctx context.Context, t time.Duration, sql string, rows *sql.Rows, err error) {
	//	log.Printf("[%.2fms] Query SQL: %v", float64(t.Milliseconds()), sql)
	//}
	//db.ExecLogFunc = func(ctx context.Context, t time.Duration, sql string, result sql.Result, err error) {
	//	log.Printf("[%.2fms] Execute SQL: %v", float64(t.Milliseconds()), sql)
	//}
	return db
}
