create table user_action (
    username varchar(255) not null,
    object_id bigint not null,
    action_type int not null,
    created_at timestamp not null,
    deleted_at timestamp
);

create table user_action_count
(
    object_id   bigint not null,
    action_type int    not null,
    cnt         bigint
);

CREATE UNIQUE INDEX user_action_count_uk ON user_action_count (object_id, action_type);