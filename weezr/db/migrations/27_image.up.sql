create table image (
    id bigint primary key not null,
    key uuid not null,
    source text not null,
    size int not null
);

CREATE SEQUENCE IF NOT EXISTS img_seq START 1;