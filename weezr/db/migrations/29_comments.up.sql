create table comment (
    id bigint primary key not null,
    parent_id bigint,
    episode_id bigint references episode(id),
    text text not null,
    response_count bigint not null default 0,
    created_at timestamp not null
);

CREATE SEQUENCE IF NOT EXISTS comment_seq START 1;