
create table weezr_user (
    id varchar(500) primary key not null,
    username varchar(500) not null ,
    name varchar(255) not null,
    nickname varchar(255),
    given_name varchar(255),
    family_name varchar(255),
    email varchar(500) not null,
    last_ip varchar(50),
    picture varchar(750)
);

