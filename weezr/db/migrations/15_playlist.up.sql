create table playlist(
    id bigint not null primary key,
    type int not null,
    code varchar(255) not null,
    title varchar(255) not null,
    username varchar(255) not null,
    created_at timestamp,
    deleted_at timestamp
);

create table playlist_item(
    item_id varchar not null,
    playlist_id bigint not null
);

create sequence playlist_seq start 1;