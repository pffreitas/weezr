create table feed_object
(
    id          bigint primary key not null,
    object_id   bigint             not null,
    object_type varchar(20)        not null,
    object_data jsonb              not null,
    score       bigint             not null,
    username    varchar(255),
    created_at  timestamp          not null
);

CREATE SEQUENCE IF NOT EXISTS feed_object_seq START 1;

create index feed_object_q1 on feed_object (username, score, created_at);