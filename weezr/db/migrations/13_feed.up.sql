CREATE TABLE feed_section (
    id bigint PRIMARY KEY,
    title varchar(255),
    items jsonb,
    object_type int,
    object_id bigint,
    section_type int,
    username varchar(255),
    created_at timestamp,
    deleted_at timestamp
);

CREATE SEQUENCE IF NOT EXISTS feed_section_seq START 1;

