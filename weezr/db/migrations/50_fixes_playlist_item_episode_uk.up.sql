 drop index playlist_item_episode_uk;

create unique index playlist_item_episode_uk on playlist_item (playlist_id, episode_id) where deleted_at is null;