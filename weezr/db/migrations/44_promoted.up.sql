alter table playlist
    drop column featured;
alter table show
    drop column featured;

create table promotion
(
    id          bigint primary key not null,
    type        varchar(20)        not null,
    placement   varchar(20)        not null,
    object_id   bigint             not null,
    impressions bigint             not null,
    expires_at  timestamp          not null,
    created_at  timestamp          not null
);

create sequence promotion_seq start 1;
