CREATE TABLE category (
    id bigint PRIMARY KEY,
    code varchar(255)
);

CREATE TABLE category_mapping (
    category_id bigint,
    original_text varchar(500)
);

CREATE TABLE episode_category (
    category_id bigint,
    episode_id bigint
);

INSERT INTO category (id, code)
    VALUES (0, 'general');

INSERT INTO category (id, code)
    VALUES (1, 'business');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (1, 'Business');

INSERT INTO category (id, code)
    VALUES (2, 'natural-sciences');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (2, 'Natural Sciences');

INSERT INTO category (id, code)
    VALUES (3, 'tech-news');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (3, 'Tech News');

INSERT INTO category (id, code)
    VALUES (4, 'careers');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (4, 'Careers');

INSERT INTO category (id, code)
    VALUES (5, 'software-how-to');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (5, 'Software How-To');

INSERT INTO category (id, code)
    VALUES (6, 'society-and-culture');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (6, 'Society & Culture');

INSERT INTO category (id, code)
    VALUES (7, 'arts');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (7, 'Arts');

INSERT INTO category (id, code)
    VALUES (8, 'gadgets');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (8, 'Gadgets');

INSERT INTO category (id, code)
    VALUES (9, 'design');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (9, 'Design');

INSERT INTO category (id, code)
    VALUES (10, 'education-technology');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (10, 'Education Technology');

INSERT INTO category (id, code)
    VALUES (11, 'management');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (11, 'Management');

INSERT INTO category (id, code)
    VALUES (12, 'science');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (12, 'Science');

INSERT INTO category (id, code)
    VALUES (13, 'science-and-medicine');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (13, 'Science & Medicine');

INSERT INTO category (id, code)
    VALUES (14, 'podcasting');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (14, 'Podcasting');

INSERT INTO category (id, code)
    VALUES (15, 'news');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (15, 'News');

INSERT INTO category (id, code)
    VALUES (16, 'comedy');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (16, 'Comedy');

INSERT INTO category (id, code)
    VALUES (17, 'tv-and-film');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (17, 'TV & Film');

INSERT INTO category (id, code)
    VALUES (18, 'technology');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (18, 'Technology');

INSERT INTO category (id, code)
    VALUES (19, 'education');

INSERT INTO category_mapping (category_id, original_text)
    VALUES (19, 'Education');

