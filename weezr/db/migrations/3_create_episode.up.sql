CREATE TABLE episode (
    id bigint PRIMARY KEY,
    slug VARCHAR(350) NOT NULL,
    title VARCHAR(300) NOT NULL,
    description VARCHAR(3000),
    content VARCHAR(3000),
    site_url VARCHAR(400),
    published_at timestamp,
    audio_file_url VARCHAR(400) NOT NULL,
    banner_url VARCHAR(300),
    categories jsonb,
    show_slug VARCHAR(400)
);

CREATE SEQUENCE IF NOT EXISTS episode_seq START 1;

