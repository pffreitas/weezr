CREATE TABLE "show" (
    id bigint PRIMARY KEY,
    name VARCHAR(300) NOT NULL,
    slug VARCHAR(350) NOT NULL,
    banner_url VARCHAR(500),
    feed_url VARCHAR(500),
    channel_slug VARCHAR(500)
);

CREATE SEQUENCE IF NOT EXISTS show_seq START 1;

