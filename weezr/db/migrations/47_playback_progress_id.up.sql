ALTER TABLE playback_progress ADD COLUMN id bigint;
ALTER TABLE playback_progress ADD PRIMARY KEY (id);
alter table playback_progress drop column episode_slug;

alter table playback_progress add column episode_id bigint references episode(id);

create sequence playback_progress_seq;