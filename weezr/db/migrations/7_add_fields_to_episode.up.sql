ALTER TABLE episode
    ADD COLUMN duration varchar(45);

ALTER TABLE episode
    ADD COLUMN created_at timestamp;

ALTER TABLE episode
    ADD COLUMN episode_image varchar(400);

ALTER TABLE episode
    ADD COLUMN keywords varchar(2000);

ALTER TABLE episode
    ADD COLUMN lang varchar(50);

ALTER TABLE episode
    ADD COLUMN show_categories jsonb;

