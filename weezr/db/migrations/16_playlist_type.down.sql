ALTER TABLE playlist
    DROP COLUMN "type";

ALTER TABLE playlist
    ADD COLUMN "type" int NOT NULL;

