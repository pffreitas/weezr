ALTER TABLE episode
    DROP COLUMN duration;

ALTER TABLE episode
    DROP COLUMN created_at;

ALTER TABLE episode
    DROP COLUMN episode_image;

ALTER TABLE episode
    DROP COLUMN keywords;

ALTER TABLE episode
    DROP COLUMN lang;

ALTER TABLE episode
    DROP COLUMN show_categories;

