CREATE TABLE playback_progress (
    episode_slug VARCHAR(350) NOT NULL,
    username VARCHAR(255) NOT NULL,
    progress integer NOT NULL,
    context jsonb,
    created_at timestamp,
    updated_at timestamp
);

