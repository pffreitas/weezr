create table scavenge_link (
    id bigint PRIMARY KEY,
    source_id varchar not null,
    source_type varchar not null,
    link varchar not null,
    status varchar not null,
    created_at timestamp not null,
    updated_at timestamp not null
);

create unique index scavenge_link_uk on scavenge_link(source_id, source_type, link);

CREATE SEQUENCE IF NOT EXISTS scavenge_link_seq START 1;