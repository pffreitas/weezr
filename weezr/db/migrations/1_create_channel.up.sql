CREATE TABLE channel (
    id bigint PRIMARY KEY,
    name VARCHAR(300) NOT NULL,
    slug VARCHAR(350) NOT NULL,
    banner_url VARCHAR(300)
);

CREATE SEQUENCE IF NOT EXISTS channel_seq START 1;

