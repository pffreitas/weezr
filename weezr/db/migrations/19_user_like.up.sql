create table user_like
(
    slug       VARCHAR(350) NOT NULL,
    username   varchar(255) not null,
    liked boolean,
    updated_at timestamp
);