alter table playlist_item drop column item_id;

alter table playlist_item
    add column episode_id bigint not null references episode (id);

ALTER TABLE playlist_item
    ADD CONSTRAINT playlist_fk FOREIGN KEY (playlist_id) REFERENCES playlist (id);

create unique index playlist_item_episode_uk on playlist_item (playlist_id, episode_id);


