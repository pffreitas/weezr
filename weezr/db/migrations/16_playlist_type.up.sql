ALTER TABLE playlist
    DROP COLUMN "type";

ALTER TABLE playlist
    ADD COLUMN "type" varchar(155) NOT NULL;

