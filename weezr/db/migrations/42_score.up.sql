alter table episode add column show_id bigint references show(id);
alter table episode add column score bigint not null default 0;
alter table show add column score bigint not null default 0;

drop table category;
drop table category_mapping;
drop table episode_category;
drop table feed_section;
drop table new_kid;