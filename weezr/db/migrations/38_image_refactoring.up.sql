alter table image drop column size;


alter table image add column etag varchar not null default '';
alter table image add column expires_at timestamp;
alter table image add column content_length bigint not null default 0;
alter table image add column content_type varchar not null default '';
alter table image add column status bigint not null default 0;
alter table image add column updated_at timestamp not null default CURRENT_TIMESTAMP;
alter table image add column created_at timestamp not null default CURRENT_TIMESTAMP;

create unique index image_uk on image(source);

alter table show add column etag varchar not null default '';
alter table show add column content_length bigint not null default 0;