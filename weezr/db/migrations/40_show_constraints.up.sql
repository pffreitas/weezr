alter table show add column hashcode char(42);
create unique index show_hashcode_uk on show(hashcode);
create unique index show_slug_uk on show(slug);

alter table episode add column hashcode char(42);
create unique index episode_hashcode_uk on episode(hashcode);
create unique index episode_slug_uk on episode(slug);