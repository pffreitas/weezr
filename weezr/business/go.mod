module weezr.me/weezr/business

go 1.12

require (
	github.com/sirupsen/logrus v1.4.2
	weezr.me/weezr/feedclnt v0.0.0
	weezr.me/weezr/model v0.0.0
)

replace (
	weezr.me/weezr/feedclnt => ../feed/feedclnt
	weezr.me/weezr/model => ../model
)
