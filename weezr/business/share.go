package business

import (
	"weezr.me/weezr/feedclnt"
	"weezr.me/weezr/model"
)

type ShareService struct {
	userActionService         UserActionService
	episodeRepository         model.EpisodeRepository
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue
	scoreService              ScoreService
}

func NewShareService(
	userActionService UserActionService,
	episodeRepository model.EpisodeRepository,
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue,
	scoreService ScoreService) ShareService {

	return ShareService{
		userActionService,
		episodeRepository,
		relatedEpisodeFanOutQueue,
		scoreService,
	}
}
func (s ShareService) Share(username string, slug model.EpisodeSlug) error {

	episode, err := s.episodeRepository.FindBySlug(slug)
	if err != nil {
		return err
	}

	err = s.userActionService.Insert(model.UserAction{
		Username:   username,
		ObjectId:   episode.ID,
		ActionType: model.Shared,
	})
	if err != nil {
		return err
	}

	err = s.scoreService.IncrEpisodeScore(episode.ID, model.ShareEpisodeScoreIncr)
	if err != nil {
		return err
	}

	err = s.relatedEpisodeFanOutQueue.Post(username, episode, 1.5)
	if err != nil {
		return err
	}

	return nil
}
