package business

import (
	"database/sql"
	"strconv"

	"weezr.me/weezr/feedclnt"

	"github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

type PlaylistService struct {
	repository                model.PlaylistRepository
	episodeRepository         model.EpisodeRepository
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue
	scoreService              ScoreService
	userActionService         UserActionService
}

func NewPlaylistService(
	repository model.PlaylistRepository,
	episodeRepository model.EpisodeRepository,
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue,
	scoreService ScoreService,
	userActionService UserActionService) PlaylistService {
	return PlaylistService{
		repository,
		episodeRepository,
		relatedEpisodeFanOutQueue,
		scoreService,
		userActionService,
	}
}

func (s PlaylistService) GetPlaylist(username string, playlistIdOrType string) (model.Playlist, error) {
	playlist, err := s.findPlayListByIdOrType(playlistIdOrType, username)
	if err != nil {
		return playlist, err
	}

	return playlist, nil
}

func (s PlaylistService) GetItems(username string, playlistID string, page model.Page) ([]model.SlimEpisode, error) {
	playlist, err := s.findPlayListByIdOrType(playlistID, username)
	if err != nil {
		return nil, err
	}

	playlistItems, err := s.repository.FindItems(playlist.ID, page)
	if err == sql.ErrNoRows {
		return playlistItems, nil
	}

	if err != nil {
		logrus.WithError(err).WithField("playlistId", playlistID).Error("failed to find playlist items")
		return playlistItems, err
	}

	playlistItems = s.userActionService.Enrich(username, playlistItems)
	return playlistItems, nil
}

func (s PlaylistService) AddItemToHistory(username string, episodeId int64) error {
	return s.addItemTo(username, model.History, episodeId)
}

func (s PlaylistService) addItemTo(username string, playlistType model.PlaylistType, episodeId int64) error {
	logrus.WithFields(logrus.Fields{"playlistType": playlistType, "episodeId": episodeId}).Info("adding item to list")
	playlist, err := s.repository.FindByUserAndType(username, playlistType)

	if err == sql.ErrNoRows {
		playlist, err = s.repository.Insert(model.Playlist{
			Type:     playlistType,
			Code:     string(playlistType),
			Username: username,
		})
		if err != nil {
			return err
		}
	}

	if err != nil {
		return err
	}

	err = s.addItem(username, playlist, episodeId)
	if err != nil {
		return err
	}

	return nil
}

func (s PlaylistService) AddItem(username string, playlistIdOrType string, episodeId int64) error {
	playlist, err := s.findPlayListByIdOrType(playlistIdOrType, username)
	if err != nil {
		if model.ListenLater == playlistIdOrType {
			playlist, err = s.repository.Insert(model.Playlist{
				Type:     model.ListenLater,
				Code:     "listen-later",
				Username: username,
			})
			if err != nil {
				logrus.
					WithError(err).
					WithField("username", username).
					WithField("playlistId", playlistIdOrType).
					Error("failed to create listen later playlist")
				return err
			}
		} else {
			logrus.WithError(err).WithField("playlistId", playlistIdOrType).Error("failed to find playlist")
			return err
		}
	}

	err = s.addItem(username, playlist, episodeId)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist item")
		return err
	}

	return nil
}

func (s PlaylistService) addItem(username string, playlist model.Playlist, episodeId int64) error {
	episode, err := s.episodeRepository.FindByID(episodeId)
	if err != nil {
		logrus.WithError(err).Error("failed to insert playlist item; episode does not exist")
		return err
	}

	err = s.repository.AddItem(playlist, episodeId)
	if err != nil {
		if err == model.PlaylistItemAlreadyExistsError {
			return nil
		}

		logrus.WithError(err).Error("failed to insert playlist item")
		return err
	}

	if playlist.Type == model.UserDefined || playlist.Type == model.ListenLater {
		err = s.scoreService.IncrEpisodeScore(episode.ID, model.AddEpisodeToPlaylistScoreIncr)
		if err != nil {
			logrus.WithError(err).Error("failed to insert playlist item; failed to incr score")
			return err
		}

		err = s.relatedEpisodeFanOutQueue.Post(username, episode, 1.4)
		if err != nil {
			logrus.WithError(err).
				WithFields(logrus.Fields{"username": username, "episode-id": episodeId}).
				Error("failed to post to related episode fan out queue")
			return err
		}
	}

	return nil
}

func (s PlaylistService) findPlayListByIdOrType(playlistIdOrType string, username string) (model.Playlist, error) {
	var playlist model.Playlist

	playlistId, err := strconv.ParseInt(playlistIdOrType, 10, 64)
	if err != nil {
		playlist, err = s.repository.FindByUserAndType(username, model.PlaylistType(playlistIdOrType))
		if err != nil {
			logrus.WithError(err).WithField("playlistId", model.PlaylistType(playlistIdOrType)).Error("failed to find playlist")
			return playlist, err
		}
	} else {
		playlist, err = s.repository.FindByID(playlistId)
		if err == sql.ErrNoRows {
			return playlist, nil
		}

		if err != nil {
			logrus.WithError(err).WithField("playlistId", playlistId).Error("failed to find playlist")
			return playlist, err
		}
	}

	return playlist, nil
}

func (s PlaylistService) RemoveItem(username string, playlistIdOrType string, episodeId int64) error {
	playlist, err := s.findPlayListByIdOrType(playlistIdOrType, username)
	if err != nil {
		logrus.WithError(err).Error("failed to remove item from playlist; playlist not found")
		return err
	}

	episode, err := s.episodeRepository.FindByID(episodeId)
	if err != nil {
		logrus.WithError(err).Error("failed to remove item from playlist; episode does not exist")
		return err
	}

	if playlist.Type == model.UserDefined || playlist.Type == model.ListenLater {
		err = s.scoreService.IncrEpisodeScore(episode.ID, -1*model.AddEpisodeToPlaylistScoreIncr)
		if err != nil {
			logrus.WithError(err).Error("failed to remove item from playlist; failed to incr score")
			return err
		}
	}

	err = s.repository.RemoveItem(playlist, episodeId)
	if err != nil {
		logrus.WithError(err).Error("failed to remove item from playlist")
		return err
	}

	return nil
}
