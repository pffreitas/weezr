package business

import "weezr.me/weezr/model"

type ShowService struct {
	model.ShowRepository
	model.EpisodeRepository
	userActionService UserActionService
}

func NewShowService(repository model.ShowRepository, episodeRepository model.EpisodeRepository, userActionService UserActionService) ShowService {
	return ShowService{repository, episodeRepository, userActionService}
}

func (s ShowService) GetShow(username string, slug string) (model.Show, error) {
	show, err := s.ShowRepository.FindBySlug(slug)
	if err != nil {
		return show, err
	}

	count, _ := s.userActionService.Count(show.ID, model.FollowedShow)
	show.FollowingCount = count

	_, err = s.userActionService.GetAction(username, show.ID, model.FollowedShow)
	show.FollowedByCurrentUser = err == nil

	return show, nil
}

func (s ShowService) GetShowByID(showId int64) (model.Show, error) {
	return s.ShowRepository.FindByID(showId)
}

func (s ShowService) GetShowEpisodes(username string, showSlug string, page model.Page) ([]model.SlimEpisode, error) {
	episodes, err := s.EpisodeRepository.FindByShowSlug(showSlug, page)
	episodes = s.userActionService.Enrich(username, episodes)
	return episodes, err
}
