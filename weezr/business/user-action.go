package business

import (
	"github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

type UserActionService struct {
	repository                 model.UserActionRepository
	playbackPostgresRepository model.PlaybackProgressRepository
}

func NewUserActionService(
	repository model.UserActionRepository,
	playbackPostgresRepository model.PlaybackProgressRepository) UserActionService {
	return UserActionService{repository, playbackPostgresRepository}
}

func (s UserActionService) Insert(action model.UserAction) error {
	logrus.
		WithFields(logrus.Fields{"action-type": action.ActionType, "object-id": action.ObjectId}).
		Info("inserting user action")

	err := s.repository.IncrCount(action.ObjectId, action.ActionType)
	if err != nil {
		return err
	}

	return s.repository.Insert(action)
}

func (s UserActionService) GetAction(username string, objectId int64, actionType model.ActionType) (model.UserAction, error) {
	return s.repository.Get(username, objectId, actionType)
}

func (s UserActionService) Delete(username string, objectId int64, actionType model.ActionType) error {
	logrus.
		WithFields(logrus.Fields{"action-type": actionType, "object-id": objectId}).
		Info("deleting user action")

	err := s.repository.DecrCount(objectId, actionType)
	if err != nil {
		return err
	}

	return s.repository.Delete(username, objectId, actionType)
}

func (s UserActionService) Count(objectId int64, actionType model.ActionType) (int64, error) {
	return s.repository.Count(objectId, actionType)
}

func (s UserActionService) FindAllByUsernameAndType(username string, actionType model.ActionType) ([]model.UserAction, error) {
	return s.repository.FindAllByUsernameAndType(username, actionType)
}

func (s UserActionService) FindAllByUsernameAndTypeAndObjectIDIn(username string, actionType model.ActionType, objectIDs []interface{}) ([]model.UserAction, error) {
	return s.repository.FindAllByUsernameAndTypeAndObjectIDIn(username, actionType, objectIDs)
}

func (s UserActionService) FindAllByObjectIdAndType(objectId int64, actionType model.ActionType) ([]model.UserAction, error) {
	return s.repository.FindAllByObjectIdAndType(objectId, actionType)
}

func (s UserActionService) Enrich(username string, episodes []model.SlimEpisode) []model.SlimEpisode {
	if len(username) == 0 {
		return episodes
	}

	var objectIDs []interface{}
	for _, i := range episodes {
		objectIDs = append(objectIDs, i.ID)
	}

	liked, err := s.FindAllByUsernameAndTypeAndObjectIDIn(username, model.Liked, objectIDs)
	if err != nil {
		logrus.WithError(err).Error("failed to enrich episodes; failed to find liked actions")
		return episodes
	}

	playbackProgresses, err := s.playbackPostgresRepository.FindLatestIn(objectIDs, username)
	if err != nil {
		logrus.WithError(err).Error("failed to enrich episodes; failed to find episode progress")
		return episodes
	}

	for i := range episodes {
		for _, l := range liked {
			if l.ObjectId == episodes[i].ID {
				episodes[i].LikedByCurrentUser = true
				break
			}
		}

		for _, p := range playbackProgresses {
			if p.EpisodeID == episodes[i].ID {
				episodes[i].TotalLength = p.Total
				episodes[i].Progress = p.Progress
				episodes[i].Finished = p.Finished
				break
			}
		}
	}

	return episodes
}
