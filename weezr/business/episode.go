package business

import (
	"weezr.me/weezr/feedclnt"

	"weezr.me/weezr/model"
)

type EpisodeService struct {
	model.EpisodeRepository
	PlaylistService
	UserActionService
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue
	scoreService              ScoreService
}

func NewEpisodeService(
	repository model.EpisodeRepository,
	playlistService PlaylistService,
	userActionService UserActionService,
	relatedEpisodeFanOutQueue feedclnt.RelatedEpisodeFanOutQueue,
	scoreService ScoreService) EpisodeService {

	return EpisodeService{
		repository,
		playlistService,
		userActionService,
		relatedEpisodeFanOutQueue,
		scoreService,
	}
}

func (s *EpisodeService) Like(username string, slug model.EpisodeSlug) (model.Episode, error) {
	var episode model.Episode

	episode, err := s.EpisodeRepository.FindBySlug(slug)
	if err != nil {
		return episode, err
	}

	err = s.UserActionService.Insert(model.UserAction{
		Username:   username,
		ObjectId:   episode.ID,
		ActionType: model.Liked,
	})
	if err != nil {
		return episode, err
	}

	err = s.PlaylistService.addItemTo(username, model.Like, episode.ID)
	if err != nil {
		return episode, err
	}

	count, _ := s.UserActionService.Count(episode.ID, model.Liked)
	episode.LikeCount = count

	_, err = s.UserActionService.GetAction(username, episode.ID, model.Liked)
	episode.LikedByCurrentUser = err == nil

	err = s.scoreService.IncrEpisodeScore(episode.ID, model.LikeEpisodeScoreIncr)
	if err != nil {
		return episode, err
	}

	err = s.relatedEpisodeFanOutQueue.Post(username, episode, 1.2)
	if err != nil {
		return episode, err
	}

	return episode, nil
}

func (s *EpisodeService) UndoLike(username string, slug model.EpisodeSlug) (model.Episode, error) {
	var episode model.Episode

	episode, err := s.EpisodeRepository.FindBySlug(slug)
	if err != nil {
		return episode, err
	}

	err = s.UserActionService.Delete(username, episode.ID, model.Liked)
	if err != nil {
		return episode, err
	}

	err = s.PlaylistService.RemoveItem(username, model.Like, episode.ID)
	if err != nil {
		return episode, err
	}

	count, _ := s.UserActionService.Count(episode.ID, model.Liked)
	episode.LikeCount = count

	_, err = s.UserActionService.GetAction(username, episode.ID, model.Liked)
	episode.LikedByCurrentUser = err == nil

	err = s.scoreService.IncrEpisodeScore(episode.ID, -1*model.LikeEpisodeScoreIncr)
	if err != nil {
		return episode, err
	}

	return episode, nil
}
