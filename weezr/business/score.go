package business

import "weezr.me/weezr/model"

type ScoreService struct {
	episodeRepository model.EpisodeRepository
	showRepository    model.ShowRepository
}

func CreateScoreService(episodeRepository model.EpisodeRepository, showRepository model.ShowRepository) ScoreService {
	return ScoreService{
		episodeRepository,
		showRepository,
	}
}

func (s *ScoreService) IncrEpisodeScore(episodeId int64, scoreIncr int) error {
	episode, err := s.episodeRepository.FindByID(episodeId)
	if err != nil {
		return err
	}

	err = s.episodeRepository.IncrScore(episodeId, scoreIncr)
	if err != nil {
		return err
	}

	err = s.IncrShowScore(episode.ShowID, scoreIncr)
	if err != nil {
		return err
	}

	return nil
}

func (s *ScoreService) IncrShowScore(showID int64, scoreIncr int) error {
	err := s.showRepository.IncrScore(showID, scoreIncr)
	if err != nil {
		return err
	}

	return nil
}
