package winj

import (
	"reflect"
)

type provider struct {
	producedType reflect.Type
}

// Container .
type Container struct {
	providers map[reflect.Type]provider
	instances map[reflect.Type][]reflect.Value
}

// Param .
type Param struct {
	Type    reflect.Type
	IsSlice bool
}

// New .
func New() *Container {
	container := &Container{
		map[reflect.Type]provider{},
		map[reflect.Type][]reflect.Value{},
	}

	v := reflect.ValueOf(container)
	container.instances[v.Type()] = []reflect.Value{v}

	return container
}

// Provide .
func (c *Container) Provide(constructor interface{}) {
	fnVal := reflect.ValueOf(constructor)
	fnType := fnVal.Type()

	inParams := getInParams(fnType)
	inArgs := getInArgs(c, inParams)

	producedType := fnType.Out(0)
	c.providers[producedType] = provider{
		producedType,
	}

	instance := fnVal.Call(inArgs)
	c.instances[producedType] = instance
}

// Invoke .
func (c *Container) Invoke(fn interface{}) interface{} {
	fnVal := reflect.ValueOf(fn)
	fnType := fnVal.Type()

	inParams := getInParams(fnType)
	inArgs := getInArgs(c, inParams)

	ret := fnVal.Call(inArgs)
	return ret[0].Interface()
}

func getInParams(fnType reflect.Type) []Param {
	numIn := fnType.NumIn()
	var inParams []Param

	for i := 0; i < numIn; i++ {
		inParamType := fnType.In(i)
		inParams = append(inParams, Param{
			inParamType,
			inParamType.Kind() == reflect.Slice,
		})
	}

	return inParams
}

func getInArgs(c *Container, inParams []Param) []reflect.Value {
	var inArgs []reflect.Value

	for _, param := range inParams {
		if param.IsSlice {
			ins := reflect.MakeSlice(param.Type, 0, 0)
			for k, v := range c.instances {
				if k.Implements(param.Type.Elem()) {
					ins = reflect.Append(ins, v[0])
				}
			}
			inArgs = append(inArgs, ins)
		} else {
			inArgs = append(inArgs, c.instances[param.Type][0])
		}
	}

	return inArgs
}
