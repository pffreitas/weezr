package main

import (
	weego "github.com/pffreitas/weego/application"
	"weezr.me/weezr/scavenger/config"
)

type ReprocessConfig struct {
	QueueName string `envconfig:"QUEUE_NAME"`
}

type ScavengerReprocessApplication struct {
	RmqConfig       config.RmqConfig
	ReprocessConfig ReprocessConfig
}

func NewScavengerReprocessApplication() weego.WeegoApplication {
	app := weego.New(ScavengerReprocessApplication{})

	app.Provide(config.NewRedisClient)
	app.Provide(config.NewRmqConnection)
	app.Provide(Reprocess{})

	return app
}
