package main

import (
	"github.com/adjust/rmq/v2"
	log "github.com/sirupsen/logrus"
)

type Reprocess struct {
	RmqConnection   rmq.Connection
	ReprocessConfig ReprocessConfig
}

func (r Reprocess) Run() error {
	log.
		WithField("queue-name", r.ReprocessConfig.QueueName).
		Info("returning all from rejected")

	queue := r.RmqConnection.OpenQueue(r.ReprocessConfig.QueueName)
	returned := queue.ReturnAllRejected()

	log.
		WithField("returned", returned).
		WithField("queue-name", r.ReprocessConfig.QueueName).
		Info("returned all from rejected")

	return nil
}
