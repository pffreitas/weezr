package main

import "github.com/sirupsen/logrus"

func main() {
	cronApp := NewScavengerReprocessApplication()

	cronApp.Invoke(func(reprocess Reprocess) int {
		err := reprocess.Run()
		if err != nil {
			logrus.WithError(err).Error("Failed to run Scavenger")
			panic(err)
		}

		return 0
	})
}
