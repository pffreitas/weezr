package link

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"weezr.me/weezr/scavenger/show"

	"github.com/sirupsen/logrus"

	"weezr.me/weezr/model"
)

type Scavenger struct {
	scavengeLinkRepository model.ScavengeLinkRepository
	showRepository         model.ShowRepository
	channelRepository      model.ChannelRepository
	showQueue              show.Queue
}

func NewLinkScavenger(
	scavengeLinkRepository model.ScavengeLinkRepository,
	showRepository model.ShowRepository,
	channelRepository model.ChannelRepository,
	showQueue show.Queue) Scavenger {
	return Scavenger{
		scavengeLinkRepository,
		showRepository,
		channelRepository,
		showQueue,
	}
}

func (s Scavenger) Scavenge(id string) error {
	linkId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		logrus.WithError(err).WithField("id", id).Error("failed to scavenge link; failed to parse id")
		return InvalidIdError
	}

	link, err := s.scavengeLinkRepository.FindByID(linkId)
	if err != nil {
		logrus.WithError(err).WithField("id", id).Error("failed to scavenge link; failed to find scavenger link")
		return LinkNotFound
	}

	resp, err := http.Get(link.Link)
	if err != nil || resp.StatusCode != http.StatusOK {
		logrus.
			WithError(err).
			WithField("status", resp.StatusCode).
			WithField("id", id).
			Error("failed to scavenge link; failed get resource")
		return FailedToGetLink
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.WithError(err).WithField("id", id).Error("failed to scavenge link; failed read resource")
		return FailedToReadLinkContents
	}

	var rawData map[string]interface{}
	err = json.Unmarshal(body, &rawData)
	if err != nil {
		logrus.WithError(err).WithField("id", id).WithField("data", string(body)).Error("failed to scavenge link; failed parse resource")
		return FailedToParseLinkContents
	}

	results, ok := rawData["results"].([]interface{})
	if !ok {
		logrus.WithField("rawData", rawData).Error("failed to parse lookup response")
		return FailedToReadResults
	}

	if len(results) == 0 {
		logrus.WithField("rawData", rawData).Error("failed to scavenge link; empty results lookup response")
		return nil
	}

	data, ok := results[0].(map[string]interface{})
	if !ok {
		logrus.WithField("rawData", rawData).Error("failed to parse lookup response")
		return FailedToParseResult
	}

	feedUrl := data["feedUrl"]
	kind := data["kind"]

	if "podcast" == kind && feedUrl != nil {
		feedURL, ok := feedUrl.(string)
		if !ok {
			logrus.WithField("data", rawData).Error("failed cast feed url")
			return FailedToCastFeedUrl
		}

		feedResp, err := http.Get(feedURL)
		if err != nil {
			logrus.WithError(err).Error("failed to scavenge link; failed to get feed")
			return FailedToGetFeed
		}

		feed, err := show.ParseFeed(feedResp.Body)
		if err != nil {
			logrus.WithError(err).Error("failed to scavenge link; failed to parse feed")
			return FailedToParseFeed
		}

		parsedShow := show.ParseShow(feed)
		parsedShow.FeedURL = feedURL

		channel := model.Channel{
			Slug: parsedShow.Slug,
			Name: parsedShow.Name,
		}
		err = s.channelRepository.Insert(channel)
		if err != nil {
			logrus.WithError(err).Error("failed to scavenge link; failed to insert channel")
			return FailedToInsertChannel
		}

		parsedShow.ChannelSlug = channel.Slug
		persistedShow, err := s.showRepository.Insert(parsedShow)
		if err != nil {
			logrus.WithError(err).Error("failed to scavenge link; failed to insert show")
			return FailedToInsertShow
		}

		link.Status = model.ProcessedScavengeLink
		err = s.scavengeLinkRepository.Update(link)
		if err != nil {
			logrus.WithError(err).Error("failed to scavenge link; failed to update link status")
			return FailedToMarkLinkAsProcessed
		}

		if ok := s.showQueue.Publish(strconv.FormatInt(persistedShow.ID, 10)); !ok {
			logrus.WithField("show-id", 0).Error("Failed to publish to scavenge-show queue")
		}
	}

	return nil
}
