package link

import "errors"

var (
	LinkNotFound                = errors.New("link_not_found")
	FailedToGetLink             = errors.New("failed_to_get_link")
	FailedToReadLinkContents    = errors.New("failed_to_read_link_contents")
	FailedToParseLinkContents   = errors.New("failed_to_parse_link_contents")
	FailedToReadResults         = errors.New("failed_to_read_results")
	EmptyResultsError           = errors.New("empty_results_error")
	FailedToParseResult         = errors.New("failed_to_parse_result")
	FailedToCastFeedUrl         = errors.New("failed_to_cast_feed_url")
	FailedToGetFeed             = errors.New("failed_to_get_feed")
	FailedToParseFeed           = errors.New("failed_to_parse_feed")
	FailedToInsertChannel       = errors.New("failed_to_insert_channel")
	FailedToInsertShow          = errors.New("failed_to_insert_show")
	FailedToMarkLinkAsProcessed = errors.New("failed_to_mark_link_as_processed")
	InvalidIdError              = errors.New("invalid_id_error")
)
