package link

import "github.com/prometheus/client_golang/prometheus"

type ScavengeSuccessCounter struct {
	prometheus.Counter
}

func NewScavengeSuccessCounter() ScavengeSuccessCounter {
	counter := prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "scavenger",
		Name:      "link_success",
		Help:      "Successfully scavenged links",
	})

	prometheus.MustRegister(counter)
	return ScavengeSuccessCounter{counter}
}

type ScavengeErrorCounter struct {
	*prometheus.CounterVec
}

func NewScavengeErrorCounter() ScavengeErrorCounter {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "scavenger",
		Name:      "link_error",
		Help:      "Number of errors while scavenging links",
	}, []string{"error"})

	prometheus.MustRegister(counter)
	return ScavengeErrorCounter{counter}
}
