package link

import (
	"time"

	"github.com/sirupsen/logrus"

	"github.com/adjust/rmq/v2"
)

type Queue struct {
	rmq.Queue
}

func NewScavengeLinkQueue(connection rmq.Connection) Queue {
	queue := connection.OpenQueue("link-scavenger")
	queue.StartConsuming(5, 5*time.Second)
	return Queue{queue}
}

func NewScavengeLinkConsumer(
	scavengeLinkQueue Queue,
	linkScavenger Scavenger,
	successCounter ScavengeSuccessCounter,
	errorCounter ScavengeErrorCounter) Consumer {
	scavengeLinkConsumer := Consumer{linkScavenger, successCounter, errorCounter}
	for i := 0; i < 1; i++ {
		scavengeLinkQueue.AddConsumer("link-scavenger", scavengeLinkConsumer)
	}
	return scavengeLinkConsumer
}

type Consumer struct {
	linkScavenger  Scavenger
	successCounter ScavengeSuccessCounter
	errorCounter   ScavengeErrorCounter
}

func (c Consumer) Consume(delivery rmq.Delivery) {
	err := c.linkScavenger.Scavenge(delivery.Payload())
	if err != nil {
		c.errorCounter.WithLabelValues(err.Error()).Inc()
		logrus.WithError(err).WithField("link-id", delivery.Payload()).Error("failed to scavenge link")
		delivery.Reject()
		return
	}

	c.successCounter.Inc()
	delivery.Ack()
}
