package link_test

import (
	"database/sql"
	"strconv"
	"testing"
	"time"

	"weezr.me/weezr/scavenger/app"

	"github.com/adjust/rmq/v2"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
	"weezr.me/weezr/scavenger/link"
)

func TestScavengeLink(t *testing.T) {
	assertions := assert.New(t)

	scavengerApp := app.NewScavengerApp()

	scavengerApp.Invoke(func(scavengeLinkRepository model.ScavengeLinkRepository, channelRepository model.ChannelRepository, showRepository model.ShowRepository, consumer link.Consumer) int {
		persistedLink, err := scavengeLinkRepository.Insert(model.ScavengeLink{
			SourceID:   "1115313672",
			SourceType: model.Apple,
			Link:       "https://itunes.apple.com/us/lookup?id=1115313672",
			Status:     model.NewScavengeLink,
			CreatedAt:  time.Now().UTC(),
			UpdatedAt:  time.Now().UTC(),
		})
		if err != nil && err != sql.ErrNoRows {
			t.Fatal(err)
		}

		delivery := rmq.NewTestDeliveryString(strconv.FormatInt(persistedLink.ID, 10))
		consumer.Consume(delivery)
		assertions.Equal(rmq.Acked, delivery.State)

		processedLink, err := scavengeLinkRepository.FindByID(persistedLink.ID)
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(model.ProcessedScavengeLink, processedLink.Status)

		c, err := channelRepository.FindBySlug("lambda3-podcast")
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(c.Name, "Lambda3 Podcast")

		s, err := showRepository.FindBySlug("lambda3-podcast")
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(s.Name, "Lambda3 Podcast")

		return 0
	})
}
