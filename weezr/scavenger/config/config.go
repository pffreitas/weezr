package config

type RmqConfig struct {
	Address  string `envconfig:"RMQ_ADDRESS"`
	Password string `envconfig:"RMQ_PASSWORD"`
}

type SpacesConfig struct {
	Endpoint     string `envconfig:"SPACES_ENDPOINT"`
	ClientID     string `envconfig:"SPACES_KEY"`
	ClientSecret string `envconfig:"SPACES_SECRET"`
}
