package main

import (
	"strconv"

	"github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
	"weezr.me/weezr/scavenger/show"
)

type Scavenger struct {
	ShowRepository model.ShowRepository
	ShowQueue      show.Queue
}

func (s Scavenger) Scavenge() error {
	logrus.Info("running scavenger")

	shows, err := s.ShowRepository.FindAll()
	if err != nil {
		return err
	}

	logrus.WithField("show-count", len(shows)).Info("scavenging shows")

	for _, sh := range shows {
		logrus.WithField("show-slug", sh.Slug).Info("posting to show scavenge queue")
		s.ShowQueue.Publish(strconv.FormatInt(sh.ID, 10))
	}

	return nil
}
