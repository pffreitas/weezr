package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"weezr.me/weezr/model"
	"weezr.me/weezr/test"
)

func TestScavengeCron(t *testing.T) {
	assertions := assert.New(t)

	scavengerApp := NewScavengerCronApplication()

	scavengerApp.Invoke(func(showRepository model.ShowRepository, scavenger Scavenger) int {
		s := test.DefaultShow()
		s.FeedURL = "http://softwareengineeringdaily.com/feed/podcast/"

		s, err := test.InsertShow(showRepository, s)
		if err != nil {
			t.Fatal(err)
		}

		err = scavenger.Scavenge()
		assertions.NoError(err)

		return 0
	})
}
