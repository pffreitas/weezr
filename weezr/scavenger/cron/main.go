package main

import "github.com/sirupsen/logrus"

func main() {
	cronApp := NewScavengerCronApplication()

	cronApp.Invoke(func(scavenger Scavenger) int {
		err := scavenger.Scavenge()
		if err != nil {
			logrus.WithError(err).Error("Failed to run Scavenger")
			panic(err)
		}

		return 0
	})
}
