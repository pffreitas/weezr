package main

import (
	weego "github.com/pffreitas/weego/application"
	"weezr.me/weezr/db"
	"weezr.me/weezr/db/postgres"
	"weezr.me/weezr/scavenger/config"
	"weezr.me/weezr/scavenger/show"
)

type ScavengerCronApplication struct {
	DatabaseConfig db.DatabaseConfig
	RmqConfig      config.RmqConfig
}

func NewScavengerCronApplication() weego.WeegoApplication {
	app := weego.New(ScavengerCronApplication{})

	app.Provide(db.GetPgSession)
	app.Provide(db.GetDbxSession)
	app.Provide(config.NewRedisClient)
	app.Provide(config.NewRmqConnection)

	app.Provide(show.NewShowScavengerQueue)
	app.Provide(postgres.NewShowPostgresRepository)

	app.Provide(Scavenger{})

	return app
}
