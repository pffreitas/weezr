package image_test

import (
	"testing"

	"weezr.me/weezr/scavenger/app"

	"weezr.me/weezr/model"

	"github.com/stretchr/testify/assert"

	"weezr.me/weezr/scavenger/image"
)

func TestInsertImage(t *testing.T) {
	assertions := assert.New(t)

	scavengerApp := app.NewScavengerApp()

	scavengerApp.Invoke(func(imageRepository model.ImageRepository, imageProcessor image.Processor) int {

		img, err := imageProcessor.Insert("http://softwaredaily.wpengine.com/wp-content/uploads/powerpress/SED_square_solid_bg.png")
		if err != nil {
			t.Fatal(err)
		}

		assertions.NotEmpty(img.Key)

		img2, err := imageProcessor.Insert("http://softwaredaily.wpengine.com/wp-content/uploads/powerpress/SED_square_solid_bg.png")
		if err != nil {
			t.Fatal(err)
		}

		assertions.NotEmpty(img2.Key)
		assertions.Equal(img.Key, img2.Key)

		sedImage, err := imageRepository.FindByKey(img.Key)
		if err != nil {
			t.Fatal(err)
		}

		err = imageProcessor.GenerateThumbnail(sedImage.Key)
		if err != nil {
			t.Fatal(err)
		}

		img3, err := imageProcessor.Insert("http://softwaredaily.wpengine.com/wp-content/uploads/powerpress/SED_square_solid_bg.png")
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(img.Key, img3.Key)
		assertions.Equal(model.Processed, img3.Status)

		sedImage, err = imageRepository.FindByKey(img.Key)
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(model.Processed, sedImage.Status)
		assertions.Equal("\"58fc7b2c-1b235\"", sedImage.ETag)
		assertions.Equal(int64(111157), sedImage.ContentLength)
		assertions.Equal("image/png", sedImage.ContentType)

		err = imageProcessor.GenerateThumbnail(sedImage.Key)
		if err != nil {
			t.Fatal(err)
		}

		sedImage2, err := imageRepository.FindByKey(img.Key)
		if err != nil {
			t.Fatal(err)
		}

		assertions.Equal(sedImage.UpdatedAt, sedImage2.UpdatedAt)

		return 0
	})
}
