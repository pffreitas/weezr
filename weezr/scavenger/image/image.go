package image

import (
	"bytes"
	"fmt"
	"image"
	_ "image/jpeg"
	"image/png"
	"io"
	"net/http"
	"time"

	"weezr.me/weezr/scavenger/config"

	"weezr.me/weezr/model"

	"github.com/sirupsen/logrus"

	"github.com/google/uuid"
	minio "github.com/minio/minio-go/v6"
	"github.com/nfnt/resize"
)

type Processor struct {
	spacesConfig    config.SpacesConfig
	imageRepository model.ImageRepository
	imageQueue      Queue
}

type DownloadedImage struct {
	ContentLength int64
	ContentType   string
	Etag          string
}

func NewImageProcessor(config config.SpacesConfig, repository model.ImageRepository, imageQueue Queue) Processor {
	return Processor{config, repository, imageQueue}
}

func (i Processor) Insert(imageSrc string) (model.Image, error) {
	var img model.Image

	key, err := i.generateKey()
	if err != nil {
		return img, err
	}

	createdAt := time.Now().UTC()
	img, err = i.imageRepository.Insert(model.Image{
		Key:       key,
		Source:    imageSrc,
		Status:    model.New,
		CreatedAt: createdAt,
		UpdatedAt: time.Now().UTC(),
	})
	if err != nil {
		logrus.WithError(err).Error("failed to insert image")
		return img, err
	}

	fmt.Printf("IMG --- %+v --- %+v --- eq: %+v; ---  %+v ---  %+v \n", createdAt, img.CreatedAt, img.CreatedAt.Equal(createdAt), img.CreatedAt.Round(time.Millisecond).Unix(), createdAt.Round(time.Millisecond).Unix())

	if img.CreatedAt.Round(time.Millisecond).Unix() == createdAt.Round(time.Millisecond).Unix() {
		logrus.Info("publishing to image queue")
		i.imageQueue.Publish(img.Key)
	}

	return img, nil
}

func (i Processor) FindByKey(imageKey string) (model.Image, error) {
	return i.imageRepository.FindByKey(imageKey)
}

func (i Processor) GenerateThumbnail(imageKey string) error {
	img, err := i.imageRepository.FindByKey(imageKey)
	if err != nil {
		logrus.WithError(err).WithField("key", imageKey).Error("image corresponding to key not found")
		return ImageNotFound
	}

	if img.Status == model.Processed {
		logrus.WithField("key", imageKey).Info("Skipping image processing as it's already in Processed state")
		return nil
	}

	sizes := []uint{60, 240}

	downloadedImage, err := i.head(img.Source)
	if err != nil {
		return FailedToHeadImage
	}

	if downloadedImage.Etag == img.ETag && downloadedImage.ContentLength == img.ContentLength {
		logrus.
			WithFields(logrus.Fields{"key": img.Key, "source": img.Source, "etag": img.ETag}).
			Info("Skipping image processing as image hasn't changed.")
		return nil
	}

	file, err := i.download(img.Source)
	if err != nil {
		return FailedToGetImage
	}

	decodedImage, err := i.decode(file)
	if err != nil {
		return FailedToDecodeImage
	}

	for _, size := range sizes {
		resizedImage := resize.Resize(size, size, decodedImage, resize.Lanczos3)

		err = i.upload(img.Key, size, resizedImage)
		if err != nil {
			return FailedToUploadImage
		}
	}

	err = i.imageRepository.Update(model.Image{
		Id:            img.Id,
		ETag:          downloadedImage.Etag,
		ExpiresAt:     nil,
		ContentLength: downloadedImage.ContentLength,
		ContentType:   downloadedImage.ContentType,
		Status:        model.Processed,
		UpdatedAt:     time.Now().UTC(),
	})
	if err != nil {
		return FailedToUpdateImage
	}

	err = file.Close()
	if err != nil {
		logrus.WithError(err).Error("failed to close image file")
	}

	return nil
}

func (i Processor) generateKey() (string, error) {
	keyUUID, err := uuid.NewUUID()
	if err != nil {
		return "", err
	}

	key := keyUUID.String()
	return key, nil
}

func (i Processor) head(imageUrl string) (DownloadedImage, error) {
	resp, err := http.Head(imageUrl)
	if err != nil {
		return DownloadedImage{}, err
	}

	return DownloadedImage{
		ContentLength: resp.ContentLength,
		ContentType:   resp.Header.Get("Content-Type"),
		Etag:          resp.Header.Get("ETag"),
	}, nil
}

func (i Processor) download(imageUrl string) (io.ReadCloser, error) {
	resp, err := http.Get(imageUrl)
	if err != nil {
		return nil, err
	}

	return resp.Body, err
}

func (i Processor) decode(file io.ReadCloser) (image.Image, error) {
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}

	return img, nil
}

func (i Processor) upload(key string, size uint, image image.Image) error {
	minioClient, err := minio.New(i.spacesConfig.Endpoint, i.spacesConfig.ClientID, i.spacesConfig.ClientSecret, true)
	if err != nil {
		return err
	}

	var buf = bytes.NewBuffer([]byte{})
	err = png.Encode(buf, image)
	if err != nil {
		return err
	}

	_, err = minioClient.PutObject("weezr-static-3", fmt.Sprintf("%s-%d.png", key, size), buf, int64(buf.Len()), minio.PutObjectOptions{
		ContentType:  "image/jpg",
		UserMetadata: map[string]string{"x-amz-acl": "public-read"},
	})

	if err != nil {
		logrus.WithError(err).Errorf("failed to upload image")
		return err
	}

	return nil
}
