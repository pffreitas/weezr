package image

import (
	"time"

	"github.com/sirupsen/logrus"

	"github.com/adjust/rmq/v2"
)

type Queue struct {
	rmq.Queue
}

func NewImageProcessorQueue(connection rmq.Connection) Queue {
	queue := connection.OpenQueue("image-processor")
	queue.StartConsuming(10, time.Second)

	return Queue{queue}
}

func NewImageProcessorConsumer(
	imageProcessorQueue Queue,
	imageProcessor Processor,
	successCounter ScavengeSuccessCounter,
	errorCounter ScavengeErrorCounter) Consumer {

	imageProcessorConsumer := Consumer{
		imageProcessor,
		successCounter,
		errorCounter,
	}
	imageProcessorQueue.AddConsumer("image-processor", imageProcessorConsumer)

	return imageProcessorConsumer
}

type Consumer struct {
	imageProcessor Processor
	successCounter ScavengeSuccessCounter
	errorCounter   ScavengeErrorCounter
}

func (c Consumer) Consume(delivery rmq.Delivery) {
	logrus.WithField("payload", delivery.Payload()).Info("Consuming message on ImageProcessorQueue")

	err := c.imageProcessor.GenerateThumbnail(delivery.Payload())
	if err != nil {
		c.errorCounter.WithLabelValues(err.Error()).Inc()
		logrus.WithError(err).WithField("key", delivery.Payload()).Error("failed to generate image thumbnail")
		delivery.Reject()
		return
	}

	c.successCounter.Inc()
	delivery.Ack()
}
