package image

import "errors"

var (
	ImageNotFound       = errors.New("episode_not_found")
	FailedToHeadImage   = errors.New("failed_to_head_image")
	FailedToGetImage    = errors.New("failed_to_get_image")
	FailedToDecodeImage = errors.New("failed_to_decode_image")
	FailedToUploadImage = errors.New("failed_to_upload_image")
	FailedToUpdateImage = errors.New("failed_to_update_image")
)
