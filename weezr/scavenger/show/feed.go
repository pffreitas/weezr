package show

import (
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gosimple/slug"
	"github.com/microcosm-cc/bluemonday"
	"github.com/mmcdole/gofeed"
	log "github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
)

var (
	ugc    = bluemonday.UGCPolicy().AddTargetBlankToFullyQualifiedLinks(true)
	strict = bluemonday.StrictPolicy()
)

type DownloadedFeed struct {
	Body          io.ReadCloser
	ETag          string
	ContentLength int64
}

func getFeed(show model.Show) (DownloadedFeed, error) {
	headResp, err := http.Head(show.FeedURL)
	if err != nil {
		log.WithField("feed-url", show.FeedURL).WithError(err).Error("Failed to request head for feed")
		return DownloadedFeed{}, FailedToHeadFeed
	}

	downloadedFeed := DownloadedFeed{
		Body:          nil,
		ETag:          headResp.Header.Get("ETag"),
		ContentLength: headResp.ContentLength,
	}

	if downloadedFeed.ETag == show.ETag && downloadedFeed.ContentLength == show.ContentLength {
		return downloadedFeed, FeedDidntChange
	}

	resp, err := http.Get(show.FeedURL)
	if err != nil {
		log.WithError(err).Error("Failed to get feed")
		return downloadedFeed, FailedToGetFeed
	}

	downloadedFeed.Body = resp.Body

	return downloadedFeed, nil
}

func ParseFeed(body io.ReadCloser) (*gofeed.Feed, error) {
	feedParser := gofeed.NewParser()

	bodyBytes, err := ioutil.ReadAll(body)
	if err != nil {
		log.WithError(err).Error("Failed to read feed")
		return nil, err
	}

	feed, err := feedParser.ParseString(string(bodyBytes))
	if err != nil {
		log.WithError(err).Error("Failed to parse feed")
		return nil, err
	}

	return feed, nil
}

func ParseShow(feed *gofeed.Feed) model.Show {
	showTitle := strict.Sanitize(feed.Title)

	showImageSource := ""
	if feed.Image != nil && len(feed.Image.URL) > 0 {
		showImageSource = feed.Image.URL
	}

	if feed.ITunesExt != nil && len(feed.ITunesExt.Image) > 0 {
		showImageSource = feed.ITunesExt.Image
	}

	var lang = model.GetLanguage(slug.Make(feed.Language))

	return model.Show{
		Slug:        slug.Make(showTitle),
		Name:        strict.Sanitize(feed.Title),
		Description: feed.Description,
		ImageSrc:    showImageSource,
		Language:    lang,
		FeedURL:     feed.FeedLink,
		Active:      len(lang) > 0,
	}
}

func parseEpisode(show model.Show, feed *gofeed.Feed, item *gofeed.Item) model.Episode {
	episodeSlug := slug.Make(strict.Sanitize(item.Title))

	if len(item.Enclosures) == 0 {
		log.WithField("episode-slug", episodeSlug).Error("episode has zero 'enclosure'")
	}

	if len(item.Enclosures) > 1 {
		log.WithField("episode-slug", episodeSlug).Warn("episode has more than one 'enclosure'")
	}

	audioFileURL := ""
	if len(item.Enclosures) == 1 {
		audioFileURL = item.Enclosures[0].URL
	}

	episodeImageSource := show.ImageSrc
	if item.Image != nil && len(item.Image.URL) > 0 {
		episodeImageSource = item.Image.URL
	}

	if item.ITunesExt != nil && len(item.ITunesExt.Image) > 0 {
		episodeImageSource = item.ITunesExt.Image
	}

	keywords := ""
	if feed.ITunesExt != nil {
		keywords = keywords + feed.ITunesExt.Keywords
	}

	if item.ITunesExt != nil {
		keywords = keywords + item.ITunesExt.Keywords
	}

	duration := ""
	if item.ITunesExt != nil {
		duration = item.ITunesExt.Duration
	}

	var publishedAt time.Time
	if item.PublishedParsed != nil {
		publishedAt = *item.PublishedParsed
	}

	tidyContent := strict.Sanitize(item.Description)
	if len(item.Content) > 0 {
		tidyContent = strict.Sanitize(item.Content)
	}

	return model.Episode{
		Slug:         episodeSlug,
		Title:        strict.Sanitize(item.Title),
		Description:  strict.Sanitize(item.Description),
		Content:      ugc.Sanitize(item.Content),
		TidyContent:  tidyContent,
		SiteLink:     item.Link,
		PublishedAt:  publishedAt,
		CreatedAt:    time.Now().UTC(),
		Categories:   append(append([]string{}, item.Categories...), feed.Categories...),
		Keywords:     keywords,
		AudioFileURL: audioFileURL,
		ShowID:       show.ID,
		ShowTitle:    show.Name,
		ShowSlug:     show.Slug,
		ImageSource:  episodeImageSource,
		Language:     show.Language,
		Duration:     duration,
	}
}
