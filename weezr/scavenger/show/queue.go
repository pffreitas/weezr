package show

import (
	"time"

	"github.com/adjust/rmq/v2"

	"github.com/sirupsen/logrus"
)

type Queue struct {
	rmq.Queue
}

func NewShowScavengerQueue(connection rmq.Connection) Queue {
	queue := connection.OpenQueue("show-scavenger")
	queue.StartConsuming(10, time.Second)
	return Queue{queue}
}

func NewShowScavengerConsumer(
	showQueue Queue,
	showScavenger Scavenger,
	successCounter ScavengeSuccessCounter,
	errorCounter ScavengeErrorCounter) Consumer {

	showConsumer := Consumer{showScavenger, successCounter, errorCounter}
	for i := 0; i < 30; i++ {
		showQueue.AddConsumer("show-scavenger", showConsumer)
	}
	return showConsumer
}

type Consumer struct {
	showScavenger  Scavenger
	successCounter ScavengeSuccessCounter
	errorCounter   ScavengeErrorCounter
}

func (c Consumer) Consume(delivery rmq.Delivery) {
	showId := delivery.Payload()

	err := c.showScavenger.Scavenge(showId)
	if err != nil {
		c.errorCounter.WithLabelValues(err.Error()).Inc()
		logrus.WithError(err).WithField("show-id", showId).Error("failed to scavenge show")
		delivery.Reject()
		return
	}

	c.successCounter.Inc()
	delivery.Ack()
}
