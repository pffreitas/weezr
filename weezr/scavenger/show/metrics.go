package show

import "github.com/prometheus/client_golang/prometheus"

type ScavengeSuccessCounter struct {
	prometheus.Counter
}

func NewScavengeSuccessCounter() ScavengeSuccessCounter {
	counter := prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "scavenger",
		Name:      "show_success_count",
		Help:      "Successfully scavenged shows",
	})

	prometheus.MustRegister(counter)
	return ScavengeSuccessCounter{counter}
}

type ScavengeErrorCounter struct {
	*prometheus.CounterVec
}

func NewScavengeErrorCounter() ScavengeErrorCounter {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "scavenger",
		Name:      "show_error",
		Help:      "Number of errors while scavenging shows",
	}, []string{"error"})

	prometheus.MustRegister(counter)
	return ScavengeErrorCounter{counter}
}
