package show_test

import (
	"testing"

	"weezr.me/weezr/model"

	"github.com/mmcdole/gofeed"

	"weezr.me/weezr/scavenger/show"

	"github.com/stretchr/testify/assert"
)

func TestParseShowUnsupportedLang(t *testing.T) {
	assertions := assert.New(t)

	parsedShow := show.ParseShow(&gofeed.Feed{
		Language: "",
	})

	assertions.False(parsedShow.Active)
}

func TestParseShowSupportedLang(t *testing.T) {
	assertions := assert.New(t)

	for _, l := range []string{"pt", "PT", "pT", "pT_BR", "ptpt", "pt-pt"} {
		parsedShow := show.ParseShow(&gofeed.Feed{
			Language: l,
		})

		assertions.True(parsedShow.Active)
		assertions.Equal(model.Portuguese, parsedShow.Language)
		assertions.True(len(parsedShow.Language) > 0)
	}

	for _, l := range []string{"en", "EN", "eN", "En", "enen", "english", "engl", "en-NZ"} {
		parsedShow := show.ParseShow(&gofeed.Feed{
			Language: l,
		})

		assertions.True(parsedShow.Active)
		assertions.Equal(model.English, parsedShow.Language)
		assertions.True(len(parsedShow.Language) > 0)
	}

	esShow := show.ParseShow(&gofeed.Feed{Language: "es"})
	assertions.True(len(esShow.Language) == 0)
}
