package show

import (
	"strconv"

	"weezr.me/weezr/search/search"

	"weezr.me/weezr/db/postgres"

	"weezr.me/weezr/scavenger/image"

	"weezr.me/weezr/scavenger/episode"

	"weezr.me/weezr/model"

	log "github.com/sirupsen/logrus"
)

type Scavenger struct {
	showRepository    model.ShowRepository
	episodeRepository model.EpisodeRepository
	imageProcessor    image.Processor
	episodeQueue      episode.Queue
	searchService     search.SearchService
}

func NewShowScavenger(
	showRepository model.ShowRepository,
	episodeRepository model.EpisodeRepository,
	imageProcessor image.Processor,
	episodeQueue episode.Queue,
	searchService search.SearchService) Scavenger {
	return Scavenger{
		showRepository,
		episodeRepository,
		imageProcessor,
		episodeQueue,
		searchService,
	}
}

func (c Scavenger) Scavenge(id string) error {
	log.WithField("show-id", id).Info("Scavenging show")

	showId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to scavenge show; failed to parse id")
		return InvalidIdError
	}

	show, err := c.showRepository.FindByID(showId)
	if err != nil {
		log.WithField("show-id", showId).WithError(err).Error("Failed to find show")
		return ShowNotFound
	}

	if !show.Active {
		log.WithField("show-id", showId).WithError(err).Error("show is not active")
		return ShowNotActive
	}

	downloadedFeed, err := getFeed(show)
	if err != nil {
		if err == FeedDidntChange {
			log.WithFields(log.Fields{"show-slug": show.Slug, "etag": show.ETag, "content-length": show.ContentLength}).
				Info("Skipping show scavenging as feed etag and content-length hasn't changed.")
			return nil
		}

		log.WithError(err).WithField("show-slug", show.Slug).Error("Failed to get feed file")
		return err
	}

	feed, err := ParseFeed(downloadedFeed.Body)
	if err != nil {
		log.WithError(err).Error("Failed to parse feed")
		return FailedToParseFeed
	}

	parsedShow := ParseShow(feed)

	show.Name = parsedShow.Name
	show.Description = parsedShow.Description
	show.ETag = downloadedFeed.ETag
	show.ContentLength = downloadedFeed.ContentLength
	show.ImageSrc = parsedShow.ImageSrc
	show.Language = parsedShow.Language

	img, err := c.imageProcessor.Insert(parsedShow.ImageSrc)
	if err != nil {
		log.WithError(err).WithField("show-slug", show.Slug).Error("Failed to process image")
		return FailedToInsertShowImage
	}
	show.ImageKey = img.Key

	err = c.showRepository.Update(show)
	if err != nil {
		log.WithError(err).Errorf("failed to update show: %s", show.Slug)
		return FailedToUpdateShow
	}

	err = c.searchService.IndexShow(search.NewIndexedShow(show))
	if err != nil {
		log.WithError(err).WithField("id", show.ID).Error("failed to scavenge show; failed to index show")
	}

	showEpisodes, err := c.episodeRepository.FindByShow(show.Slug)
	if err != nil {
		log.WithError(err).Error("Failed retrieve show's episodes")
		return FailedToFetchShowsEpisodes
	}

	log.WithFields(log.Fields{"existing": len(showEpisodes), "parsed": len(feed.Items)}).Info("Existing episodes x parsed")

	for _, item := range feed.Items {
		parsedEpisode := parseEpisode(show, feed, item)

		if !contains(showEpisodes, parsedEpisode) {
			img, err := c.imageProcessor.Insert(parsedEpisode.ImageSource)
			if err != nil {
				log.WithError(err).
					WithField("episode-slug", parsedEpisode.Slug).
					Error("Failed to post to EpisodeScavengeQueue; Failed insert episode image")
				return FailedToInsertEpisodeImage
			}

			parsedEpisode.ImageKey = img.Key
			episodeId, err := c.episodeRepository.Insert(parsedEpisode)
			if err != nil {
				if err == postgres.DuplicatedEpisodeErr {
					continue
				}

				log.WithError(err).
					WithField("episode-slug", parsedEpisode.Slug).
					Error("Failed to post to EpisodeScavengeQueue; Failed to insert episode")
				return FailedToInsertEpisode
			}

			log.WithField("episode-id", episodeId).Info("Posting episode to EpisodeScavengeQueue")
			if ok := c.episodeQueue.Publish(strconv.FormatInt(episodeId, 10)); !ok {
				log.WithField("episode-slug", parsedEpisode.Slug).
					WithField("episode-id", episodeId).
					Error("failed to post to episode-scavenger queue")
			}
		} else {
			if updated(showEpisodes, parsedEpisode) {
				img, err := c.imageProcessor.Insert(parsedEpisode.ImageSource)
				if err != nil {
					log.WithError(err).
						WithField("episode-slug", parsedEpisode.Slug).
						Error("Failed to post to EpisodeScavengeQueue; Failed insert episode image")
					return FailedToInsertEpisodeImage
				}

				parsedEpisode.ImageKey = img.Key
				err = c.episodeRepository.Update(parsedEpisode)
				if err != nil {
					if err == postgres.DuplicatedEpisodeErr {
						continue
					}

					log.WithError(err).
						WithField("episode-slug", parsedEpisode.Slug).
						Error("Failed to post to EpisodeScavengeQueue; Failed to update episode")
					return FailedToUpdateEpisode
				}

				err = c.searchService.IndexEpisode(search.NewIndexedEpisode(parsedEpisode))
				if err != nil {
					log.WithError(err).WithField("episode", parsedEpisode).Error("failed to reindex episode on update")
				}
			}
		}
	}

	return nil
}

func contains(episodeList []model.Episode, episode model.Episode) bool {
	for _, e := range episodeList {
		if e.Hashcode() == episode.Hashcode() {
			return true
		}
	}
	return false
}

func updated(episodeList []model.Episode, episode model.Episode) bool {
	for _, e := range episodeList {
		if e.Equals(episode) {
			return true
		}
	}
	return false
}
