package show

import "errors"

var (
	ShowNotFound               = errors.New("show_not_found")
	ShowNotActive              = errors.New("show_not_active")
	InvalidIdError             = errors.New("invalid_id_error")
	FailedToHeadFeed           = errors.New("failed_to_head_feed")
	FailedToGetFeed            = errors.New("failed_to_get_feed")
	FailedToParseFeed          = errors.New("failed_to_parse_feed")
	FailedToInsertShowImage    = errors.New("failed_to_insert_show_image")
	FailedToInsertEpisodeImage = errors.New("failed_to_insert_episode_image")
	FailedToUpdateShow         = errors.New("failed_to_update_show")
	FailedToFetchShowsEpisodes = errors.New("failed_to_fetch_shows_episodes")
	FeedDidntChange            = errors.New("feed_didnt_change")
	FailedToInsertEpisode      = errors.New("failed_to_insert_episode")
	FailedToUpdateEpisode      = errors.New("failed_to_update_episode")
)
