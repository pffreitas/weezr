package show_test

import (
	"strconv"
	"testing"

	"weezr.me/weezr/scavenger/app"

	"github.com/adjust/rmq/v2"

	"weezr.me/weezr/model"

	"weezr.me/weezr/test"

	"weezr.me/weezr/scavenger/show"

	"github.com/stretchr/testify/assert"
)

func TestScavengeShow(t *testing.T) {
	assertions := assert.New(t)

	scavengerApp := app.NewScavengerApp()

	scavengerApp.Invoke(func(showRepository model.ShowRepository, consumer show.Consumer) int {
		s := test.DefaultShow()
		s.FeedURL = "http://softwareengineeringdaily.com/feed/podcast/"
		s.Active = true

		s, err := test.InsertShow(showRepository, s)
		if err != nil {
			t.Fatal(err)
		}

		delivery := rmq.NewTestDeliveryString(strconv.FormatInt(s.ID, 10))

		consumer.Consume(delivery)

		assertions.Equal(rmq.Acked, delivery.State)

		s, _ = showRepository.FindBySlug(s.Slug)
		assertions.NotEmpty(s.ETag)
		assertions.Greater(s.ContentLength, int64(0))
		assertions.NotEmpty(s.ImageSrc)

		return 0
	})
}
