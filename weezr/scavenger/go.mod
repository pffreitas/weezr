module weezr.me/weezr/scavenger

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/adjust/rmq/v2 v2.0.0
	github.com/expectedsh/go-sonic v0.0.0-20200417165347-1cfe7c425fff
	github.com/go-redis/redis/v7 v7.2.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/gosimple/slug v1.9.0
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/minio/minio-go/v6 v6.0.55
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pffreitas/rmqprom v0.0.5
	github.com/pffreitas/weego v0.0.0-20200122023400-9fb39e754bde
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.30.0
	gopkg.in/yaml.v2 v2.2.7 // indirect
	weezr.me/weezr/db v0.0.0
	weezr.me/weezr/feedclnt v0.0.0
	weezr.me/weezr/model v0.0.0
	weezr.me/weezr/search v0.0.0
	weezr.me/weezr/test v0.0.0
)

replace (
	github.com/adjust/rmq/v2 => github.com/pffreitas/rmq/v2 v2.0.0
	weezr.me/weezr/business => ../business
	weezr.me/weezr/db => ../db
	weezr.me/weezr/feed => ../feed/feed
	weezr.me/weezr/feedclnt => ../feed/feedclnt
	weezr.me/weezr/model => ../model
	weezr.me/weezr/search => ../search
	weezr.me/weezr/test => ../test
)
