package main

import (
	"fmt"
	"log"
	"net/http"

	"weezr.me/weezr/scavenger/app"

	"github.com/gorilla/mux"

	"github.com/sirupsen/logrus"
)

func main() {
	scavengerApp := app.NewScavengerApp()

	scavengerApp.Invoke(func(router *mux.Router) int {
		logrus.Info("Running scavenger")
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", "8000"), router))
		return 0
	})
}
