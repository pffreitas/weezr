package episode

import (
	"math"
	"strconv"
	"time"

	"weezr.me/weezr/search/search"

	"weezr.me/weezr/feedclnt"

	log "github.com/sirupsen/logrus"
	"weezr.me/weezr/model"
	"weezr.me/weezr/scavenger/image"
)

type Scavenger struct {
	episodeRepository     model.EpisodeRepository
	imageProcessor        image.Processor
	newEpisodeFanOutQueue feedclnt.NewEpisodeFanOutQueue
	searchService         search.SearchService
}

func NewEpisodeScavenger(
	episodeRepository model.EpisodeRepository,
	imageProcessor image.Processor,
	newEpisodeFanOutQueue feedclnt.NewEpisodeFanOutQueue,
	searchService search.SearchService) Scavenger {

	return Scavenger{
		episodeRepository,
		imageProcessor,
		newEpisodeFanOutQueue,
		searchService,
	}
}

func (s Scavenger) Scavenge(id string) error {
	log.WithField("payload", id).Info("Scavenging episode")

	episodeId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to scavenge episode; failed to parse id")
		return InvalidIdError
	}

	e, err := s.episodeRepository.FindByID(episodeId)
	if err != nil {
		log.WithError(err).WithField("id", episodeId).Error("failed to scavenge episode; episode not found")
		return EpisodeNotFound
	}

	err = s.searchService.IndexEpisode(search.NewIndexedEpisode(e))
	if err != nil {
		log.WithError(err).WithField("id", episodeId).Error("failed to scavenge episode; failed to index episode")
		return FailedToIndexEpisode
	}

	if len(e.TidyContent) >= 144 && math.Abs(time.Now().UTC().Sub(e.PublishedAt).Hours()) < 48 {
		err = s.newEpisodeFanOutQueue.Post(e)
		if err != nil {
			log.WithError(err).WithField("id", episodeId).Error("failed to scavenge episode; failed to fan out episode to feed")
			return FailedToFanOutEpisodeToFeed
		}
	}

	return nil
}
