package episode

import "errors"

var (
	EpisodeNotFound             = errors.New("episode_not_found")
	FailedToIndexEpisode        = errors.New("failed_to_index_episode")
	InvalidIdError              = errors.New("invalid_id_error")
	FailedToFanOutEpisodeToFeed = errors.New("failed_to_fan_out_episode_to_feed")
)
