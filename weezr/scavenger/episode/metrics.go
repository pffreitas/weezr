package episode

import "github.com/prometheus/client_golang/prometheus"

type ScavengeSuccessCounter struct {
	prometheus.Counter
}

func NewScavengeSuccessCounter() ScavengeSuccessCounter {
	counter := prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "scavenger",
		Name:      "episode_success",
		Help:      "Successfully scavenged episodes",
	})

	prometheus.MustRegister(counter)
	return ScavengeSuccessCounter{counter}
}

type ScavengeErrorCounter struct {
	*prometheus.CounterVec
}

func NewScavengeErrorCounter() ScavengeErrorCounter {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "scavenger",
		Name:      "episode_error",
		Help:      "Number of errors while scavenging episodes",
	}, []string{"error"})

	prometheus.MustRegister(counter)
	return ScavengeErrorCounter{counter}
}
