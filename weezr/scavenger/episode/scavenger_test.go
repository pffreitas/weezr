package episode_test

import (
	"strconv"
	"testing"

	"weezr.me/weezr/scavenger/episode"

	"weezr.me/weezr/scavenger/app"

	"github.com/adjust/rmq/v2"

	"weezr.me/weezr/model"

	"weezr.me/weezr/test"

	"github.com/stretchr/testify/assert"
)

func TestScavengeEpisode(t *testing.T) {
	assertions := assert.New(t)

	scavengerApp := app.NewScavengerApp()

	scavengerApp.Invoke(func(showRepository model.ShowRepository, episodeRepository model.EpisodeRepository, consumer episode.Consumer) int {
		s := test.DefaultShow()

		s, err := test.InsertShow(showRepository, s)
		if err != nil {
			t.Fatal(err)
		}

		e, err := test.InsertDefaultEpisode(episodeRepository, s)
		if err != nil {
			t.Fatal(err)
		}

		delivery := rmq.NewTestDeliveryString(strconv.FormatInt(e.ID, 10))

		consumer.Consume(delivery)

		assertions.Equal(rmq.Acked, delivery.State)

		return 0
	})
}
