package episode

import (
	"time"

	"github.com/sirupsen/logrus"

	"github.com/adjust/rmq/v2"
)

type Queue struct {
	rmq.Queue
}

func NewEpisodeScavengerQueue(connection rmq.Connection) Queue {
	queue := connection.OpenQueue("episode-scavenger")
	queue.StartConsuming(2, 5*time.Second)
	return Queue{queue}
}

func NewEpisodeScavengerConsumer(
	episodeQueue Queue,
	episodeScavenger Scavenger,
	successCounter ScavengeSuccessCounter,
	errorCounter ScavengeErrorCounter) Consumer {
	episodeConsumer := Consumer{episodeScavenger, successCounter, errorCounter}

	for i := 0; i < 10; i++ {
		episodeQueue.AddConsumer("episode-scavenger", episodeConsumer)
	}

	return episodeConsumer
}

type Consumer struct {
	episodeScavenger Scavenger
	successCounter   ScavengeSuccessCounter
	errorCounter     ScavengeErrorCounter
}

func (c Consumer) Consume(delivery rmq.Delivery) {
	err := c.episodeScavenger.Scavenge(delivery.Payload())
	if err != nil {
		c.errorCounter.WithLabelValues(err.Error()).Inc()
		logrus.WithError(err).WithField("episode-id", delivery.Payload()).Error("failed to scavenge episode")
		delivery.Reject()
		return
	}

	c.successCounter.Inc()
	delivery.Ack()
}
