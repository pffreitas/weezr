package app

import (
	"github.com/adjust/rmq/v2"
	"github.com/gorilla/mux"
	"github.com/pffreitas/rmqprom"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"weezr.me/weezr/db"
	"weezr.me/weezr/db/postgres"
	"weezr.me/weezr/feedclnt"
	"weezr.me/weezr/scavenger/config"
	"weezr.me/weezr/scavenger/episode"
	"weezr.me/weezr/scavenger/image"
	"weezr.me/weezr/scavenger/link"
	"weezr.me/weezr/scavenger/show"
	"weezr.me/weezr/search/search"

	weego "github.com/pffreitas/weego/application"
	whttp "github.com/pffreitas/weego/application/http"
)

type ScavengerApplication struct {
	DatabaseConfig db.DatabaseConfig
	RmqConfig      config.RmqConfig
	SpacesConfig   config.SpacesConfig
	SearchConfig   search.Config
}

func NewScavengerApp() weego.WeegoApplication {
	app := weego.New(ScavengerApplication{})

	app.Provide(db.GetPgSession)
	app.Provide(db.GetDbxSession)
	app.Provide(config.NewRedisClient)
	app.Provide(config.NewRmqConnection)

	app.Provide(postgres.NewChannelPostgresRepository)
	app.Provide(postgres.NewShowPostgresRepository)
	app.Provide(postgres.NewEpisodePostgresRepository)
	app.Provide(postgres.NewImagePostgresRepository)
	app.Provide(postgres.NewScavengeLinkPostgresRepository)

	app.Provide(feedclnt.CreateNewEpisodeFanOutQueue)

	app.Provide(image.NewScavengeSuccessCounter)
	app.Provide(image.NewScavengeErrorCounter)
	app.Provide(image.NewImageProcessorQueue)
	app.Provide(image.NewImageProcessor)
	app.Provide(image.NewImageProcessorConsumer)

	app.Provide(search.NewSearchService)

	app.Provide(episode.NewScavengeSuccessCounter)
	app.Provide(episode.NewScavengeErrorCounter)
	app.Provide(episode.NewEpisodeScavenger)
	app.Provide(episode.NewEpisodeScavengerQueue)
	app.Provide(episode.NewEpisodeScavengerConsumer)

	app.Provide(show.NewScavengeSuccessCounter)
	app.Provide(show.NewScavengeErrorCounter)
	app.Provide(show.NewShowScavenger)
	app.Provide(show.NewShowScavengerQueue)
	app.Provide(show.NewShowScavengerConsumer)

	app.Provide(link.NewScavengeSuccessCounter)
	app.Provide(link.NewScavengeErrorCounter)
	app.Provide(link.NewLinkScavenger)
	app.Provide(link.NewScavengeLinkQueue)
	app.Provide(link.NewScavengeLinkConsumer)

	app.Provide(whttp.NewRouter)

	app.Provide(func(router *mux.Router, connection rmq.Connection) int {
		rmqprom.RecordRmqMetrics(connection)

		router.Handle("/metrics", promhttp.Handler())
		return 0
	})

	return app
}
