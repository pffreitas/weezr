package test

import (
	"github.com/gosimple/slug"
	"github.com/icrowley/fake"
	"weezr.me/weezr/model"
)

func DefaultShow() model.Show {
	chanelSlug := slug.Make(fake.WordsN(3))

	showName := fake.WordsN(3)
	showSlug := slug.Make(showName)

	return model.Show{
		Slug:        showSlug,
		Name:        showName,
		FeedURL:     fake.DomainName(),
		ChannelSlug: chanelSlug,
		ImageKey:    fake.Word(),
	}
}

func InsertShow(repository model.ShowRepository, show model.Show) (model.Show, error) {
	persistedShow, err := repository.Insert(show)
	if err != nil {
		return persistedShow, err
	}

	return persistedShow, nil
}
