package test

import (
	"time"

	"github.com/gosimple/slug"
	"github.com/icrowley/fake"

	"weezr.me/weezr/model"
)

func DefaultEpisode(show model.Show) model.Episode {
	episodeSlug := slug.Make(fake.WordsN(3))

	return model.Episode{
		Slug:        episodeSlug,
		Title:       episodeSlug,
		PublishedAt: time.Now().UTC(),
		ShowID:      show.ID,
		ShowSlug:    show.Slug,
		Language:    model.Portuguese,
		ImageKey:    fake.CharactersN(10),
		Description: fake.WordsN(40),
		TidyContent: fake.WordsN(40),
	}
}

func InsertDefaultEpisode(repository model.EpisodeRepository, show model.Show) (model.Episode, error) {
	episode := DefaultEpisode(show)

	_, err := repository.Insert(episode)
	if err != nil {
		return model.Episode{}, err
	}

	return repository.FindBySlug(model.EpisodeSlug(episode.Slug))
}

func InsertEpisode(repository model.EpisodeRepository, episode model.Episode) (model.Episode, error) {
	_, err := repository.Insert(episode)
	if err != nil {
		return model.Episode{}, err
	}

	return repository.FindBySlug(model.EpisodeSlug(episode.Slug))
}
