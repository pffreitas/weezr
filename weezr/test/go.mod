module weezr.me/weezr/test

go 1.13

require (
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/gosimple/slug v1.9.0
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	weezr.me/weezr/model v0.0.0
)

replace weezr.me/weezr/model => ../model
