REVISION=$(shell git rev-list --count HEAD)
HASH=$(shell git rev-parse --short HEAD)
WEEZR_VERSION=${REVISION}.${HASH}
IMAGE_BASE_TAG=registry.gitlab.com/pffreitas/weezr

down: 
	docker-compose down
	docker volume prune -f

infra:
	docker-compose run migrate

webapp-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C webapp build-image

webapp-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C webapp push-image

webapp-deploy: 
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C webapp deploy
	

api-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/api build-image

api-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/api push-image

api-deploy: 
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/api deploy

feed-build-image: 
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/feed/feed/app build-image

feed-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/feed/feed/app push-image

feed-deploy: 
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/feed/feed/app deploy

scavenger-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger build-image

scavenger-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger push-image

scavenger-deploy:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger deploy

scavenger-cron-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger/cron build-image

scavenger-cron-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger/cron push-image

scavenger-cron-deploy:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger/cron deploy

reindex-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/search/reindex build-image

reindex-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/search/reindex push-image

reindex-deploy:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/search/reindex deploy

reprocess-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger/reprocess build-image

reprocess-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger/reprocess push-image

reprocess-deploy:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/scavenger/reprocess deploy

migrations-build-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/db build-image

migrations-push-image:
	$(MAKE) IMAGE_BASE_TAG=$(IMAGE_BASE_TAG) WEEZR_VERSION=$(WEEZR_VERSION) -C weezr/db push-image


db-drop: 
	docker run --rm -v ${CURDIR}/weezr/db/migrations:/migrations migrate/migrate -path /migrations -database postgres://weezr:weezr@db.local.weezr.me:5432/weezr?sslmode=disable drop